<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePedidoDomicilioTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('pedido_domicilio', function (Blueprint $table) {
            $table->id();
            $table->LongText('codigo_pedido');
            $table->LongText('nombre');
            $table->LongText('telefono');
            $table->LongText('direccion');
            $table->LongText('barrio');
            $table->LongText('indicacion_adicional');
            $table->LongText('comentarios');
            $table->LongText('productos');
            $table->LongText('adicionales');
            $table->LongText('domicilio');
            $table->LongText('valor_adicional');
            $table->LongText('valor_productos');
            $table->LongText('total_pedido');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('pedido_domicilio');
    }

}
