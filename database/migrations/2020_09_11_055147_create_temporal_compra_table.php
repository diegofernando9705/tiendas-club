<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemporalCompraTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temporal_compra', function (Blueprint $table) {
            $table->id();
            $table->string('id_compra');
            $table->string('id_producto');
            $table->string('nombre_producto');
            $table->string('imagen_producto');
            $table->string('cantidad_producto');
            $table->string('valor_compra_cant_prod');
            $table->string('valor_total_compra');
            $table->string('nombre_comprador')->nullable();
            $table->string('telefono_comprador')->nullable();
            $table->string('estado_compra');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temporal_compra');
    }
}
