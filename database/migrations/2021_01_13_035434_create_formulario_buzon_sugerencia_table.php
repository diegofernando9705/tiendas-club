<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFormularioBuzonSugerenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulario_buzon_sugerencia', function (Blueprint $table) {
            $table->id();
            $table->longText('nombre_persona');
            $table->longText('telefono_persona');
            $table->longText('calificacion_persona');
            $table->longText('mensaje_sugerencia')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulario_buzon_sugerencia');
    }
}
