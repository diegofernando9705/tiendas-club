<?php

use Caffeinated\Shinobi\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /**
         *
         * Creación de permisos para los usuarios del sistema
         *
         */

        Permission::create([
            'name'        => 'Usuarios | Principal',
            'slug'        => 'users.index',
            'description' => 'Vista principal de los usuarios del sistema',
        ]);

        Permission::create([
            'name'        => 'Usuarios |  Crear un usuario',
            'slug'        => 'users.registerUser',
            'description' => 'Crear un usuario del sistema',
        ]);

        Permission::create([
            'name'        => 'Usuarios | Detalle unico usuario',
            'slug'        => 'users.show',
            'description' => 'Vista para ver el detalle de un usuario del sistema',
        ]);

        Permission::create([
            'name'        => 'Usuarios |  Edición de usuario',
            'slug'        => 'users.edit',
            'description' => 'Vista para editar un usuario del sistema',
        ]);

        Permission::create([
            'name'        => 'Usuarios |  Eliminar de usuario',
            'slug'        => 'users.destroy',
            'description' => 'Eliminar un usuario del sistema',
        ]);

        /**
         *
         * Creación de permisos para los Roles del sistema
         *
         */

        Permission::create([
            'name'        => 'Roles | Principal',
            'slug'        => 'roles.index',
            'description' => 'Vista principal de los roles del sistema',
        ]);

        Permission::create([
            'name'        => 'Roles | Creacion rol',
            'slug'        => 'roles.create',
            'description' => 'Vista para ver el formulario de creacion de un rol del sistema',
        ]);

        Permission::create([
            'name'        => 'Roles | Detalle unico rol',
            'slug'        => 'roles.show',
            'description' => 'Vista para ver el detalle de un rol del sistema',
        ]);

        Permission::create([
            'name'        => 'Roles |  Edición de rol',
            'slug'        => 'roles.edit',
            'description' => 'Vista para editar un rol del sistema',
        ]);

        Permission::create([
            'name'        => 'Roles |  Eliminar de rol',
            'slug'        => 'roles.destroy',
            'description' => 'Eliminar un rol del sistema',
        ]);

        /**
         *
         * Creación de permisos para los Productos del sistema
         *
         */

        Permission::create([
            'name'        => 'Productos | Principal',
            'slug'        => 'productos.index',
            'description' => 'Vista principal de los productos del sistema',
        ]);

        Permission::create([
            'name'        => 'Productos | Creacion producto',
            'slug'        => 'productos.create',
            'description' => 'Vista para ver el formulario de creacion de un producto del sistema',
        ]);

        Permission::create([
            'name'        => 'Productos | Registrar producto',
            'slug'        => 'productos.store',
            'description' => 'Registro de un producto del sistema',
        ]);

        Permission::create([
            'name'        => 'Productos | Buscador producto',
            'slug'        => 'productos.ajaxBuscador',
            'description' => 'Vista para ver el formulario de consulta productos del sistema',
        ]);

        Permission::create([
            'name'        => 'Productos | Detalle unico producto',
            'slug'        => 'productos.show',
            'description' => 'Vista para ver el detalle de un producto del sistema',
        ]);

        Permission::create([
            'name'        => 'Productos |  Edición de producto',
            'slug'        => 'productos.edit',
            'description' => 'Vista para editar un producto del sistema',
        ]);

        Permission::create([
            'name'        => 'Productos |  Eliminar de producto',
            'slug'        => 'productos.destroy',
            'description' => 'Eliminar un producto del sistema',
        ]);
        /**
         *
         * Creación de permisos para las categorias de los Productos del sistema
         *
         */

        Permission::create([
            'name'        => 'Categorias | Principal',
            'slug'        => 'categoria.index',
            'description' => 'Vista principal de categoria del sistema',
        ]);

        Permission::create([
            'name'        => 'Categorias | Creacion categoria',
            'slug'        => 'productos.categoria.create',
            'description' => 'Vista para ver el formulario de creacion de categoria del sistema',
        ]);

        Permission::create([
            'name'        => 'Categorias | Buscador categoria',
            'slug'        => 'productos.categoria.ajaxBuscadorCategoria',
            'description' => 'Vista para ver el formulario de consulta productos del sistema',
        ]);

        Permission::create([
            'name'        => 'Categorias | Detalle unico Categoria',
            'slug'        => 'productos.show.categoria',
            'description' => 'Vista para ver el detalle de un producto del sistema',
        ]);

        Permission::create([
            'name'        => 'Categorias |  Edición de Categoria',
            'slug'        => 'productos.edit.categoria',
            'description' => 'Vista para editar un producto del sistema',
        ]);

        Permission::create([
            'name'        => 'Productos |  Eliminar de producto',
            'slug'        => 'productos.categoria.destroy',
            'description' => 'Eliminar un producto del sistema',
        ]);

    }
}
