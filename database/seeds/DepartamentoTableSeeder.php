<?php

use Illuminate\Database\Seeder;

class DepartamentoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
/*
DB::table('departamentos')->insert([
'nombre_departamento' => 'Amazonas',
'sigla_departamento'  => 'AMZ',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Antioquia',
'sigla_departamento'  => 'ANT',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Arauca',
'sigla_departamento'  => 'ARU',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Atlántico',
'sigla_departamento'  => 'ATL',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Bolívar',
'sigla_departamento'  => 'BOL',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Boyacá',
'sigla_departamento'  => 'BOY',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Caldas',
'sigla_departamento'  => 'CAL',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Caquetá',
'sigla_departamento'  => 'CAQ',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Casanare',
'sigla_departamento'  => 'CAS',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Cauca',
'sigla_departamento'  => 'CAU',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Cesar',
'sigla_departamento'  => 'CES',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Chocó',
'sigla_departamento'  => 'CHOC',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Córdoba',
'sigla_departamento'  => 'COR',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Cundinamarca',
'sigla_departamento'  => 'CUN',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Guainía',
'sigla_departamento'  => 'GUA',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Guaviare',
'sigla_departamento'  => 'GUV',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Huila',
'sigla_departamento'  => 'HUI',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'La Guajira',
'sigla_departamento'  => 'GUJ',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Magdalena',
'sigla_departamento'  => 'MAG',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Meta',
'sigla_departamento'  => 'MET',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Nariño',
'sigla_departamento'  => 'NAR',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Norte de Santander',
'sigla_departamento'  => 'NOR',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Putumayo',
'sigla_departamento'  => 'PUT',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Quindío',
'sigla_departamento'  => 'QUI',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Risaralda',
'sigla_departamento'  => 'RIS',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'San Andrés y Providencia',
'sigla_departamento'  => 'SAP',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Santander',
'sigla_departamento'  => 'SAN',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Sucre',
'sigla_departamento'  => 'SUC',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Tolima',
'sigla_departamento'  => 'TOL',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Valle del Cauca',
'sigla_departamento'  => 'VAC',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Vaupés',
'sigla_departamento'  => 'VAU',
'estado_departamento' => '1',
]);

DB::table('departamentos')->insert([
'nombre_departamento' => 'Vichada',
'sigla_departamento'  => 'VIC',
'estado_departamento' => '1',
]);
 */
    }
}
