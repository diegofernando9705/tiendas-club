<?php

use Caffeinated\Shinobi\Models\Role;
use Illuminate\Database\Seeder;

class UsuariosTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        factory(App\User::class, 10)->create();

        Role::create([
            'name'    => 'Admin',
            'slug'    => 'admin',
            'special' => 'all-access',
        ]);
    }
}
