<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Cart;
use GuzzleHttp\Client;


class PublicoController extends Controller
{   

    public function client(Request $request){

        $informacion_tienda = DB::table('tiendas')->where('id_tienda', $request->codigo_tienda)->get();

        $key_public = $informacion_tienda[0]->llave_publica;

        if($key_public == "0"){
           return view('wompi.no_habilitado');
        }else{

          $url = "https://production.wompi.co/v1/merchants/pub_prod_wP7BJfrfiM3aA5ZRUhqcI6LSp4ndPbTd";

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            //for debug only!
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $resp = curl_exec($curl);
            
            curl_close($curl);

            $data = json_decode($resp, true);


            $accepted_payment_methods = $data['data']['accepted_payment_methods'];
            $acceptance_token = $data['data']['presigned_acceptance']['acceptance_token'];
            $permalink = $data['data']['presigned_acceptance']['permalink'];
            

            if(isset($request->opcion) && $request->opcion == 'whatsapp'){

            	$nombre = $request->nombre;
				$telefono = $request->telefono;
				$direccion = $request->direccion;
				$barrio = $request->barrio;
				$indicacion_adicional = $request->indicacion_adicional;
				$codigo_referencia_wompi = $request->codigo_referencia_wompi;
				$comentarios = $request->comentarios;
				$codigo_tienda = $request->codigo_tienda;


            	return view('wompi.form-domicilio', compact('permalink', 'accepted_payment_methods', 'nombre', 'telefono', 'direccion', 'barrio', 'indicacion_adicional', 'codigo_referencia_wompi', 'comentarios', 'codigo_tienda'));
            
            }else{

            	$codigo_tienda = $request->codigo_tienda;
            	$nombre_cliente = $request->nombre;
            	$telefono_cliente = $request->telefono;
            	$hora_cliente = $request->hora_llegada;
            	$comentarios_cliente = $request->comentarios;


            return view('wompi.form', compact('permalink', 'accepted_payment_methods', 'nombre_cliente', 'telefono_cliente', 'hora_cliente', 'codigo_tienda', 'comentarios_cliente'));
            }

        }
    }

    public function method_pay(Request $request){

        
        if($request->acceptance_token == true){



                $codigo_tienda = $request->codigo_tienda;    
                
                $informacion_tienda = DB::table('tiendas')->where('id_tienda', $codigo_tienda)->get();

                foreach ($informacion_tienda as $informacion) {
                	$domicilio = $informacion->domicilio;
                }

                $i = 0;
                $carrito = [];
                $carrito_total = [];
                $producto_unico = [];
                $total_adicion = [];

                $array = explode('"', Cart::subtotal());
                $array_1 = explode(",", $array[0]);
                $total_valor = array_sum($array_1);

                if ($total_valor == "0") {
                        $producto_unico[1] = "0";
                        $producto_unico[2] = "0";
                        $producto_unico[3] = "0";
                } else {

                    foreach (Cart::content() as $pedido_carrito_validate) {
                        $carrito[] = $pedido_carrito_validate->id;
                        $carrito_total[] = $pedido_carrito_validate;
                        $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
                    }

                    $total_adicionales = array_sum($valor_adicion);

                    foreach ($array as $value) {
                        $total_prueba = explode(",", $value); 
                    }


                    $tamano_array_total = sizeof($total_prueba);


                    if($tamano_array_total == 3){
                    
                        $solo_miles = explode(".", $total_prueba[2]);
                        $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];
                        $producto_unico[1] = $subtotal+$domicilio+$total_adicionales;
                        $producto_unico[2] = $subtotal;
                        $producto_unico[3] = $total_adicionales;
                    
                    }else if($tamano_array_total <= 2){
                    
                        $solo_miles = explode(".", $total_prueba[1]);
                        $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];
                        $total = $producto_unico[1] = $subtotal+$total_adicionales;
                        
                    
                    }else{

                    }
                } //  FIN IF TOTAL VALOR == 0
                

                if($request->opcion == "local"){
        			$resultado_total = $total;
        		}else{
        			$resultado_total = $total+$domicilio;
        		}


                $nombre = $request->nombre_wompi;
                $celular = $request->telefono_wompi;
                $correo =  $request->email_wompi;

            if($request->method == "CARD"){
                return view("wompi.form_pago_tarjeta", compact("nombre","celular", "codigo_tienda", "resultado_total"));
            }else if($request->method == "NEQUI"){
                return view("wompi.form_pago_nequi", compact("nombre","celular", "codigo_tienda", "resultado_total"));
            }else if($request->method == "PSE"){

                $tienda = DB::table('tiendas')->where('id_tienda', $codigo_tienda)->get();

                foreach ($tienda as $info) {
                    $llave_publica = $info->llave_publica;
                }


                /* SE GENERA EL SELECTOR PARA BANCOS */

                    $ch_pay = curl_init('https://production.wompi.co/v1/pse/financial_institutions');               
                    curl_setopt($ch_pay, CURLOPT_CUSTOMREQUEST, "GET");                                                                            
                    curl_setopt($ch_pay, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch_pay , CURLOPT_HTTPHEADER, array(                                     
                                'Content-Type: application/json',
                                'Authorization: Bearer '.$llave_publica)                                                                       
                    ); 

                    $result_pay = curl_exec($ch_pay);
                    curl_close($ch_pay);

                    $data_pay = json_decode($result_pay, true);

                return view("wompi.form_pago_pse", compact("nombre","celular", "codigo_tienda", "total", "data_pay"));

                /* FIN GENERA EL SELECTOR PARA BANCOS */
            }else if($request->method == "BANCOLOMBIA_TRANSFER"){ 
                return "BANCOLOMBIA";
            }else{

            }
            
        }
    }

    public function pay_token(Request $request){
        
        $nombre_wompi = $request->nombre_wompi;
        $email_wompi = $request->email_wompi;
        $telefono_wompi = $request->telefono_wompi;

        $acceptance_token = $request->acceptance_token;
        $method = $request->method;

        $i = 0;
        $carrito = [];
        $carrito_total = [];
        $producto_unico = [];
        $total_adicion = [];


        $array = explode('"', Cart::subtotal());
        $array_1 = explode(",", $array[0]);
        $total_valor = array_sum($array_1);


        if ($total_valor == "0") {

            $producto_unico[1] = "0";
            $producto_unico[2] = "0";
            $producto_unico[3] = "0";

        } else {

            foreach (Cart::content() as $pedido_carrito_validate) {
                $carrito[] = $pedido_carrito_validate->id;
                $carrito_total[] = $pedido_carrito_validate;
                $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
            }

            $total_adicionales = array_sum($valor_adicion);

            foreach ($array as $value) {
                $total_prueba = explode(",", $value); 
            }


            $tamano_array_total = sizeof($total_prueba);


            if($tamano_array_total == 3){
            
                $solo_miles = explode(".", $total_prueba[2]);
                $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];
                $producto_unico[1] = $subtotal+$total_adicionales;
                $producto_unico[2] = $subtotal;
                $producto_unico[3] = $total_adicionales;
            
            }else if($tamano_array_total <= 2){
            
                $solo_miles = explode(".", $total_prueba[1]);
                $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];
                $total = $subtotal+$total_adicionales;
                
            
            }else{
            }

        }

        if($request->method == "CARD"){
            
            $nombre_tarjeta = $request->nombre_tarjeta;
            $cvv_tarjeta = $request->cvv_tarjeta;
            $numero_tarjeta = $request->numero_tarjeta;
            $mes_tarjeta = $request->mes_tarjeta;
            $ano_tarjeta = $request->ano_tarjeta;

            return view("wompi.form_tarjeta_credito", compact("nombre_wompi", "email_wompi", "telefono_wompi", "acceptance_token", "method", "nombre_tarjeta", "cvv_tarjeta", "numero_tarjeta", "mes_tarjeta", "ano_tarjeta", "total"));
        }
    }    

    public function pay_wompi(Request $request){

        $informacion_tienda = DB::table('tiendas')->where('id_tienda', $request->codigo_tienda)->get();
        
        foreach ($informacion_tienda as $value) {
            $llave_publica = $value->llave_publica;
            $llave_privada = $value->llave_privada;
        }

        // se efectuara el pago a wompi
        $numero_tarjeta = str_replace(' ', '', $request->numero_tarjeta);
        

        /* VER VALOR DE LA COMPRA*/

                $i = 0;
                $carrito = [];
                $carrito_total = [];
                $producto_unico = [];
                $total_adicion = [];


                $array = explode('"', Cart::subtotal());
                $array_1 = explode(",", $array[0]);
                $total_valor = array_sum($array_1);

                foreach (Cart::content() as $pedido_carrito_validate) {
                    $carrito[] = $pedido_carrito_validate->id;
                    $carrito_total[] = $pedido_carrito_validate;
                    $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
                }

                $total_adicionales = array_sum($valor_adicion);

                foreach ($array as $value) {
                    $total_prueba = explode(",", $value); 
                }


                $tamano_array_total = sizeof($total_prueba);


                if($tamano_array_total == 3){
                
                    $solo_miles = explode(".", $total_prueba[2]);
                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];
                    $producto_unico[1] = $subtotal+$total_adicionales;
                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;
                
                }else if($tamano_array_total <= 2){
                
                    $solo_miles = explode(".", $total_prueba[1]);
                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];
                    $total = intval($subtotal+$total_adicionales)."00";
                    
                
                }else{
                }
        /* FIN VER VALOR DE LA COMPRA*/


        if($request->method == "CARD"){

            /* CAPTURAR TOKEN TERMINOS Y CONDICIONES */
                $url = "https://production.wompi.co/v1/merchants/".$llave_publica;
                


                $codigo_tienda = $request->codigo_tienda;
                $nombre_cliente = $request->nombre_wompi;
                $telefono_cliente = $request->telefono_wompi;
                $hora_cliente = $request->hora_llegada;
                $comentario_cliente = $request->comentarios;


                $curl = curl_init($url);
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

                $resp = curl_exec($curl);
                curl_close($curl);

                $data = json_decode($resp, true);


                /* SE GUARDA VARIABLE DE TOKEN*/
                $acceptance_token = $data['data']['presigned_acceptance']['acceptance_token'];
            /* FIN CAPTURAR TOKEN TERMINOS Y CONDICIONES */


            /* SE CAPTURA EL TOKEN DE ACEPTACION DE TARJETAS DE CREDITO PARA REALIZAR LA TRANSACCION*/
            
                $data = array(
                            'number' => $numero_tarjeta, 
                            'exp_month' => $request->mes_tarjeta,
                            'exp_year' => $request->ano_tarjeta, 
                            'cvc' => $request->cvv_tarjeta, 
                            'card_holder' => $request->nombre_tarjeta
                        );                                                          
            
                $data_string = json_encode($data);                                                                                   
                $ch = curl_init('https://production.wompi.co/v1/tokens/cards');               
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST"); 
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);  	                
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                            'Content-Type: application/json',
                            'Authorization: Bearer '.$llave_publica,
                            'Content-Length: ' . strlen($data_string))                                                                       
                );

                $result = curl_exec($ch);
                curl_close($ch);

                
                $data = json_decode($result, true);

                $token = $data['data']['id'];

            /* FIN CAPTURA DE TOKEN DE ACEPTACION DE TARJETAS DE CREDITO PARA REALIZAR LA TRANSACCION*/


            /* SE GENERA EL ID PDE LA TRANSACCION*/
                $data_pay = array(
                            'type' => 'CARD', 
                            'token' => $token, 
                            'customer_email' => $request->email_wompi,
                            'acceptance_token' => $acceptance_token
                        );                                                                    
                $data_string_pay = json_encode($data_pay);                                                                                   
                $ch_pay = curl_init('https://production.wompi.co/v1/payment_sources');               
                curl_setopt($ch_pay, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch_pay, CURLOPT_POSTFIELDS, $data_string_pay);                                                                  
                curl_setopt($ch_pay, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_pay , CURLOPT_HTTPHEADER, array(                                                                          
                            'Content-Type: application/json',
                            'Authorization: Bearer '.$llave_privada,
                            'Content-Length: ' . strlen($data_string_pay))                                                                       
                ); 

                $result_pay = curl_exec($ch_pay);
                curl_close($ch_pay);

                $data_pay = json_decode($result_pay, true);

                $payment_source_id = $data_pay['data']['id'];

            /* FIN GENERAR ID PARA TRANSACCION*/


            /* GENERAR TRANSACCION PARA PAGO */
            $reference = "PAGO_WOMPI_TIENDA-".time();

                $data_transaccion = array(
                            'amount_in_cents' => intval($total), 
                            'currency' => 'COP', 
                            'customer_email' => $request->email_wompi,
                            'payment_method' => [
                                    'installments' => $request->cuotas
                            ],
                            'reference' => $reference, // Referencia única de pago
                            'payment_source_id' => $payment_source_id
                        );                                                                    
                $data_string_transaccion = json_encode($data_transaccion);                                                                                   
                $ch_transaccion = curl_init('https://production.wompi.co/v1/transactions');               
                curl_setopt($ch_transaccion, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch_transaccion, CURLOPT_POSTFIELDS, $data_string_transaccion);                                                  
                curl_setopt($ch_transaccion, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_transaccion, CURLOPT_HTTPHEADER, array(                  
                            'Content-Type: application/json',
                            'Authorization: Bearer prv_prod_xFCtmHY7Of7sJAoWnMuaEWOijhDDVQif',
                            'Content-Length: ' . strlen($data_string_transaccion))                                                    
                ); 

                $result_transaction = curl_exec($ch_transaccion);
                curl_close($ch_transaccion);

                $data_transaccion = json_decode($result_transaction, true);

                return $data_transaccion;

            /* FIN GENERAR TRANSACCION PARA PAGO */


        }else if($request->method == "NEQUI"){

            $informacion_tienda = DB::table('tiendas')->where('id_tienda', $request->codigo_tienda)->get();

            

            /* CAPTURAR TOKEN TERMINOS Y CONDICIONES */

                $codigo_tienda = $request->codigo_tienda;
                $nombre_cliente = $request->nombre_wompi;
                $telefono_cliente = $request->telefono_wompi;
                $hora_cliente = $request->hora_llegada;
                $comentario_cliente = $request->comentarios;


            $curl = curl_init($informacion_tienda[0]->llave_publica);
            curl_setopt($curl, CURLOPT_URL, $informacion_tienda[0]->llave_publica);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            //for debug only!
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

            $resp = curl_exec($curl);
            
            curl_close($curl);

            $data = json_decode($resp, true);



            $acceptance_token = $data['data']['presigned_acceptance']['acceptance_token'];
            return $acceptance_token;

            /* FIN CAPTURAR TOKEN TERMINOS Y CONDICIONES */


            /* SE CAPTURA EL TOKEN DE ACEPTACION DE NEQUI PARA REALIZAR LA TRANSACCION*/
                
                $data = array(
                            'phone_number' => $request->celular_nequi
                        );                                                                    
                $data_string = json_encode($data);                                                                                   
                $ch = curl_init('https://production.wompi.co/v1/tokens/nequi');               
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                            'Content-Type: application/json',
                            'Authorization: Bearer '.$llave_publica,
                            'Content-Length: ' . strlen($data_string))                                                                       
                );

                $result = curl_exec($ch);
                curl_close($ch);

                
                $data = json_decode($result, true);

                $token = $data['data']['id'];
                $status = $data['data']['status'];

                return $token;

            /* FIN CAPTURA DE TOKEN DE ACEPTACION DE NEQUI PARA REALIZAR LA TRANSACCION*/
        }
    }

    public function verification_nequi($id){
        
         $url = "https://production.wompi.co/v1/tokens/nequi/".$id;

         $curl = curl_init($url);
         curl_setopt($curl, CURLOPT_URL, $url);
         curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
         curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
         curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

         $resp = curl_exec($curl);
         curl_close($curl);

         $data = json_decode($resp, true);
        return $data;
    }

    public function pago_con_nequi(Request $request, $token){
        
        $informacion_tienda = DB::table('tiendas')->where('id_tienda', $request->codigo_tienda)->get();
        
        foreach ($informacion_tienda as $value) {
            $llave_publica = $value->llave_publica;
            $llave_privada = $value->llave_privada;
        }


        /* VER VALOR DE LA COMPRA*/

                $i = 0;
                $carrito = [];
                $carrito_total = [];
                $producto_unico = [];
                $total_adicion = [];


                $array = explode('"', Cart::subtotal());
                $array_1 = explode(",", $array[0]);
                $total_valor = array_sum($array_1);

                foreach (Cart::content() as $pedido_carrito_validate) {
                    $carrito[] = $pedido_carrito_validate->id;
                    $carrito_total[] = $pedido_carrito_validate;
                    $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
                }

                $total_adicionales = array_sum($valor_adicion);

                foreach ($array as $value) {
                    $total_prueba = explode(",", $value); 
                }


                $tamano_array_total = sizeof($total_prueba);


                if($tamano_array_total == 3){
                
                    $solo_miles = explode(".", $total_prueba[2]);
                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];
                    $producto_unico[1] = $subtotal+$total_adicionales;
                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;
                
                }else if($tamano_array_total <= 2){
                
                    $solo_miles = explode(".", $total_prueba[1]);
                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];
                    $total = intval($subtotal+$total_adicionales)."00";
                    
                
                }else{
                	
                }
        /* FIN VER VALOR DE LA COMPRA*/



        /* SE GENERA EL ID PDE LA TRANSACCION*/

            $data_pay = array(
                'type' => 'NEQUI',
                'token' => $token
            );      

            $data_string_pay = json_encode($data_pay);                                                                                   
            $ch_pay = curl_init('https://production.wompi.co/v1/payment_sources');               
            curl_setopt($ch_pay, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch_pay, CURLOPT_POSTFIELDS, $data_string_pay);
            curl_setopt($ch_pay, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch_pay , CURLOPT_HTTPHEADER, array(                                                                          
                'Content-Type: application/json',
                'Authorization: Bearer '.$llave_privada,
                'Content-Length: ' . strlen($data_string_pay))
            ); 

            $result_pay = curl_exec($ch_pay);
            curl_close($ch_pay);

            $data_pay = json_decode($result_pay, true);

            $payment_source_id = $data_pay['data']['id'];

        /* FIN GENERAR ID PARA TRANSACCION*/
    

        /* GENERAR TRANSACCION PARA PAGO */

                $reference = "PAGO_WOMPI_TIENDA-".time();

                $data_transaccion = array(
                            'amount_in_cents' => intval($total), 
                            'currency' => 'COP', 
                            'customer_email' => $request->email_wompi,
                            'reference' => $reference, // Referencia única de pago
                            'payment_source_id' => $payment_source_id
                        );                                                                    
                $data_string_transaccion = json_encode($data_transaccion);                                                                                   
                $ch_transaccion = curl_init('https://production.wompi.co/v1/transactions');               
                curl_setopt($ch_transaccion, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch_transaccion, CURLOPT_POSTFIELDS, $data_string_transaccion);                                                  
                curl_setopt($ch_transaccion, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch_transaccion, CURLOPT_HTTPHEADER, array(                  
                            'Content-Type: application/json',
                            'Authorization: Bearer '.$llave_privada,
                            'Content-Length: ' . strlen($data_string_transaccion))                                                    
                ); 

                $result_transaction = curl_exec($ch_transaccion);
                curl_close($ch_transaccion);

                $data_transaccion = json_decode($result_transaction, true);

                return $data_transaccion;
        /* FIN GENERAR TRANSACCION PARA PAGO */
    }


    /* RESULTADO DE LA TRANSACCION */

    public function transaction($id){

        $ch_transaccion = curl_init('https://production.wompi.co/v1/transactions/'.$id);               
        curl_setopt($ch_transaccion, CURLOPT_CUSTOMREQUEST, "GET");                                                 
        curl_setopt($ch_transaccion, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch_transaccion, CURLOPT_HTTPHEADER, array(                  
            'Content-Type: application/json'
        )); 

        $result_transaction = curl_exec($ch_transaccion);
        curl_close($ch_transaccion);

        $data_transaccion = json_decode($result_transaction, true);

        return $data_transaccion;
    }  

    public function home(){
        return view('public.index');
    }   


    public function index($url_tienda){

        date_default_timezone_set('America/Bogota');
        $am_o_pm = $newDateTime = explode(" ", date('H:i:s A'));

    	$tienda = DB::table('tiendas')->where('url_tienda', $url_tienda)->count();
    	
    	if($tienda == 0){
    		return "NO SE ENCONTRO LA TIENDA";
    	}else{
    		
            $array_categorias = [];
            $categorias = [];
            $horario_tienda_general = [];

    		$tiendas = DB::table('tiendas')->where('url_tienda', $url_tienda)->get();

            $dias = [];
            $dia = ['s'];

            foreach($tiendas as $tienda_info){
                $codigo_tienda = $tienda_info->id_tienda;
                $llave_publica = $tienda_info->llave_publica;
                $horario_tienda = $tienda_info->horario_tienda;
                $estado_publico = $tienda_info->estado_tienda;
                $nombre_tienda = $tienda_info->url_tienda;
            }

            /*

            $horario_tienda_general = explode(",", $horario);
            
            foreach ($horario_tienda_general as $key => $valor) {
                $dias[] = explode("-", $valor);
            }

            foreach ($dias as $dias_semana) {
                foreach ($dias_semana as $dia_semana) {
                    $dia[] = $dia_semana;
                }
            }


            foreach ($dia as $value) {
                $dias_semana[] = $value; 
            }

            $horario_tienda = [];

            /* lunes *

            $posicion_lunes = array_search('Lunes',$dia);
            
            if($posicion_lunes){
                $letra_lunes = $dia[$posicion_lunes];
                
                $hora_inicio_lunes = explode("_", $dia[$posicion_lunes+1]);
                $hora_fin_lunes = explode("_", $dia[$posicion_lunes+1]);
                
                $dia_lunes = " - ".$letra_lunes.": ".$hora_inicio_lunes[0]." a ".$hora_fin_lunes[1];
                
                array_push($horario_tienda, $dia_lunes);
            }


            $posicion_martes = array_search('Martes',$dia);
            
            if($posicion_martes){
                $letra_martes = $dia[$posicion_martes];
                
                $hora_inicio_martes = explode("_", $dia[$posicion_martes+1]);
                $hora_fin_martes = explode("_", $dia[$posicion_martes+1]);
                
                $dia_martes = " - ".$letra_martes.": ".$hora_inicio_martes[0]." a ".$hora_fin_martes[1];
                
                array_push($horario_tienda, $dia_martes);
            }


            $posicion_miercoles = array_search('Miercoles',$dia);
            
            if($posicion_miercoles){
                $letra_miercoles = $dia[$posicion_miercoles];
                
                $hora_inicio_miercoles = explode("_", $dia[$posicion_miercoles+1]);
                $hora_fin_miercoles = explode("_", $dia[$posicion_miercoles+1]);
                
                $dia_miercoles = " - ".$letra_miercoles.": ".$hora_inicio_miercoles[0]." a ".$hora_fin_miercoles[1];
                
                array_push($horario_tienda, $dia_miercoles);
            }


            $posicion_jueves = array_search('Jueves',$dia);
            
            if($posicion_jueves){
                $letra_jueves = $dia[$posicion_jueves];
                
                $hora_inicio_jueves = explode("_", $dia[$posicion_jueves+1]);
                $hora_fin_jueves = explode("_", $dia[$posicion_jueves+1]);
                
                $dia_jueves = " - ".$letra_jueves.": ".$hora_inicio_jueves[0]." a ".$hora_fin_jueves[1];
                
                array_push($horario_tienda, $dia_jueves);
            }


            $posicion_viernes = array_search('Viernes',$dia);

            if($posicion_viernes){
                $letra_viernes = $dia[$posicion_viernes];
                
                $hora_inicio_viernes = explode("_", $dia[$posicion_viernes+1]);
                $hora_fin_viernes = explode("_", $dia[$posicion_viernes+1]);
                //return $hora_fin_viernes[1];
                $dia_viernes = " - ".$letra_viernes.": ".$hora_inicio_viernes[0]." a ".$hora_fin_viernes[1];
                
                array_push($horario_tienda, $dia_viernes);
            }

            

            $posicion_sabado = array_search('Sabado',$dia);
            
            if($posicion_sabado){
                $letra_sabado = $dia[$posicion_sabado];
                
                $hora_inicio_sabado = explode("_", $dia[$posicion_sabado+1]);
                $hora_fin_sabado = explode("_", $dia[$posicion_sabado+1]);
                
                $dia_sabado = " - ".$letra_sabado.": ".$hora_inicio_sabado[0]." a ".$hora_fin_sabado[1];
                
                array_push($horario_tienda, $dia_sabado);
            }



            $posicion_domingo = array_search('Domingo',$dia);
            
            if($posicion_domingo){
                $letra_domingo = $dia[$posicion_domingo];
                
                $hora_inicio_domingo = explode("_", $dia[$posicion_domingo+1]);
                $hora_fin_domingo = explode("_", $dia[$posicion_domingo+1]);
                
                $dia_domingo = " - ".$letra_domingo.": ".$hora_inicio_domingo[0]." a ".$hora_fin_domingo[1];
                
                array_push($horario_tienda, $dia_domingo);
            }

            */
            

            $productos = DB::table('detalle_tienda_productos')->
                        select('detalle_tienda_productos.*', 'tiendas.id_tienda', 'productos.*')
                        ->join('productos','detalle_tienda_productos.id_producto', '=', 'productos.id')
                        ->join('tiendas','detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                        ->where('productos.estado_producto', '1')
                        ->where('tiendas.id_tienda', $codigo_tienda)->get();


            $categorias = DB::table('categorias_productos_tiendas')
                        ->select('categorias_productos_tiendas.*', 'categorias_productos.*')
                        ->join('categorias_productos', 'categorias_productos_tiendas.id_categoria', '=', 'categorias_productos.id')
                        ->where('categorias_productos_tiendas.id_tienda', $codigo_tienda)->get();


            if($estado_publico == "1"){
                return view('public.tiendas.index', compact('tiendas', 'codigo_tienda', 'categorias', 'productos', 'estado_publico', 'horario_tienda', 'nombre_tienda', 'url_tienda'));
            }else{
                return "TIENDA DESACTIVADA";
            }

    		
    	}
    }


    public function consultaProducto($nombre_producto, $codigo_tienda) {

            $productos = DB::table('detalle_tienda_productos')->
                        select('detalle_tienda_productos.*', 'tiendas.id_tienda', 'productos.*')
                        ->join('productos','detalle_tienda_productos.id_producto', '=', 'productos.id')
                        ->join('tiendas','detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                        ->where("name", 'like', $nombre_producto . "%")
                        ->where('productos.estado_producto', '1')
                        ->where('tiendas.id_tienda', $codigo_tienda)->get();
           
            
            $categorias = DB::table('categorias_productos_tiendas')
                        ->select('categorias_productos_tiendas.*', 'categorias_productos.*')
                        ->join('categorias_productos', 'categorias_productos_tiendas.id_categoria', '=', 'categorias_productos.id')
                        ->where('categorias_productos_tiendas.id_tienda', $codigo_tienda)->get();

            return view('public.tiendas.productoscat', compact('codigo_tienda', 'categorias', 'productos'));
    }


    public function consultaProductoCat($id_categoria, $codigo_tienda) {
        
        if($id_categoria == "todo"){

            return "";
        }else{

            $productos = DB::table('detalle_tienda_productos')->
                        select('detalle_tienda_productos.*', 'tiendas.id_tienda', 'productos.*')
                        ->join('productos','detalle_tienda_productos.id_producto', '=', 'productos.id')
                        ->join('tiendas','detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                        ->where('productos.estado_producto', '1')
                        ->where("id_categoria", $id_categoria)
                        ->where('tiendas.id_tienda', $codigo_tienda)->get();
           
            
            $categorias = DB::table('categorias_productos_tiendas')
                        ->select('categorias_productos_tiendas.*', 'categorias_productos.*')
                        ->join('categorias_productos', 'categorias_productos_tiendas.id_categoria', '=', 'categorias_productos.id')
                        ->where('categorias_productos_tiendas.id_tienda', $codigo_tienda)->get();
            return view('public.tiendas.productoscat', compact('codigo_tienda', 'categorias', 'productos'));
        }
        
    }

    public function infoProducto($codigo_producto) {


        $adicionales = DB::table('detalle_adicionales_productos')
                        ->select('detalle_adicionales_productos.*', 'productos.id AS id_producto', 'productos.name', 'productos.description', 'productos.id_categoria', 'productos.valor', 'adicionales_productos.id_unico AS id_adicional', 'adicionales_productos.nombre_adicional', 'adicionales_productos.descripcion_adicional', 'adicionales_productos.valor_adicional')
                        ->join('adicionales_productos', 'detalle_adicionales_productos.id_adicional_detalle', '=', 'adicionales_productos.id_unico')
                        ->join('productos', 'detalle_adicionales_productos.id_producto_detalle', '=', 'productos.id')
                        ->where('productos.id', $codigo_producto)->get();

        $productos = DB::table('productos')->where('id', '=', $codigo_producto)->get();
        $categoria = DB::table('categorias_productos')->where('id', '=', $productos[0]->id_categoria)->get();

        $resultado = [];

        $resultado[0] = $adicionales;
        $resultado[1] = $productos;
        $resultado[2] = $categoria;

        
        return $resultado;
        
    }

    public function agregarProCarrito(Request $request) {


        $codigo = rand(1, 500);
        $respuesta = [];
        $valor_adicion = [];


         $productos = DB::table('productos')->where('id', '=', $request->codigo_producto)->get();



        Cart::add(['id' => $codigo, 'name' => $productos[0]->name, 'qty' => 1, 'price' => $productos[0]->valor, 'options' => ['imageProduct' => $productos[0]->imagen_producto, 'comentarios' => $request->comentarios_producto, 'adicionales' => '', 'valores_adicionales' => '0']]);

        if (isset($request->adicionales)) {


            foreach ($request->adicionales as $valor) {

                $adicion = DB::table('adicionales_productos')->where('id_unico', $valor)->get();

                foreach ($adicion as $info_adicion) {

                    foreach (Cart::content() as $pedido) {

                        if ($pedido->id == $codigo) {

                            $adicion_final = $pedido->options->valores_adicionales + $info_adicion->valor_adicional;

                            $option = ['imageProduct' => $pedido->options->imageProduct,
                                'comentarios' => $pedido->options->comentarios,
                                'adicionales' => $pedido->options->adicionales . " , " . $info_adicion->nombre_adicional.' ('.$info_adicion->valor_adicional.') ',
                                'valores_adicionales' => $adicion_final];


                            Cart::update($pedido->rowId, ['options' => $option]);
                        }
                    }
                }
            }

            foreach (Cart::content() as $pedido_carrito_validate) {
                $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
            }

            $array = explode('"', Cart::subtotal());

            $array_1 = explode(",", $array[0]);

            $total_valor = array_sum($array_1);

            if ($total_valor == "0") {
                $producto_unico[1] = "0";
                $producto_unico[2] = "0";
                $producto_unico[3] = "0";

            } else {

               $total_adicionales = array_sum($valor_adicion);

                $array = explode('"', Cart::subtotal());
                foreach ($array as $value) {
                    $total_prueba = explode(",", $value); 
                }

                $tamano_array_total = sizeof($total_prueba);

                if($tamano_array_total == 3){

                    $solo_miles = explode(".", $total_prueba[2]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;

                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else if($tamano_array_total <= 2){
                    
                    $solo_miles = explode(".", $total_prueba[1]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;

                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else{

                }
            }

            $respuesta[1] = "1";
            $respuesta[2] = $subtotal + $total_adicionales;

        } else {

            foreach (Cart::content() as $pedido_carrito_validate) {
                $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
            }

            $array = explode('"', Cart::subtotal());

            $array_1 = explode(",", $array[0]);

            $total_valor = array_sum($array_1);

            if ($total_valor == "0") {
                $producto_unico[1] = "0";
                $producto_unico[2] = "0";
                $producto_unico[3] = "0";
            } else {
                

                $total_adicionales = array_sum($valor_adicion);

                $array = explode('"', Cart::subtotal());
            
                foreach ($array as $value) {
                    $total_prueba = explode(",", $value); 
                }

                $tamano_array_total = sizeof($total_prueba);

                if($tamano_array_total == 3){

                    $solo_miles = explode(".", $total_prueba[2]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;

                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else if($tamano_array_total <= 2){
                    
                    $solo_miles = explode(".", $total_prueba[1]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;

                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else{

                }
            }

            $respuesta[1] = "1";
            $respuesta[2] = $subtotal + $total_adicionales;

        }
        return $respuesta;
    }

    public function eliminarProducto($id, $codigo_tienda) {

        Cart::remove($id);

        $i = 0;
        $carrito = [];
        $carrito_total = [];
        $producto_unico = [];
        $total_adicion = [];

        $tiendas = DB::table('tiendas')->where('id_tienda', '=', $codigo_tienda)->get();

        foreach (Cart::content() as $pedido_carrito_validate) {
            $carrito[] = $pedido_carrito_validate->id;
            $carrito_total[] = $pedido_carrito_validate;
            $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
        }

        foreach ($tiendas as $info_tienda) {
            $domicilio = $info_tienda->domicilio;
        }

        $array = explode('"', Cart::subtotal());

        $array_1 = explode(",", $array[0]);

        $total_valor = array_sum($array_1);

        if ($total_valor == "0") {
            $producto_unico[1] = "0";
            $producto_unico[2] = "0";
            $producto_unico[3] = "0";
        } else {

           $total_adicionales = array_sum($valor_adicion);

            $array = explode('"', Cart::subtotal());
            foreach ($array as $value) {
                $total_prueba = explode(",", $value); 
            }

            $tamano_array_total = sizeof($total_prueba);

            if($tamano_array_total == 3){

                $solo_miles = explode(".", $total_prueba[2]);

                $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                $producto_unico[1] = $subtotal+$domicilio+$total_adicionales;

                $producto_unico[2] = $subtotal;
                $producto_unico[3] = $total_adicionales;

            }else if($tamano_array_total <= 2){
                
                $solo_miles = explode(".", $total_prueba[1]);

                $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                $producto_unico[1] = $subtotal+$domicilio+$total_adicionales;

                $producto_unico[2] = $subtotal;
                $producto_unico[3] = $total_adicionales;

            }else{

            }

        }

        return view('conf.carrito', compact('producto_unico', 'domicilio'));
    }

    public function opcionEnvio($opcion, $codigo_tienda) {


        

        if($opcion == 'local'){

            $informacion = [];
            $valor_adicion = [];

            $explode_carrito = explode(".", Cart::subtotal());  
            $explode_carrito_2 = explode(",", $explode_carrito[0]);
             
            $miles = $explode_carrito_2[0];
            $decimales = $explode_carrito_2[1];

            $subtotal = intval($miles.$decimales);
            
            foreach (Cart::content() as $carrito) {
                $valor_adicion[] = $carrito->options->valores_adicionales;
            }

            $adicionales = array_sum($valor_adicion);

            $informacion[0] = number_format($subtotal+$adicionales);
            $informacion[1] = number_format("0");


            return $informacion;
         }else{

            $tienda = DB::table('tiendas')->where('id_tienda', $codigo_tienda)->get();


            $informacion = [];
            $valor_adicion = [];

            $explode_carrito = explode(".", Cart::subtotal());  
            $explode_carrito_2 = explode(",", $explode_carrito[0]);
             
            $miles = $explode_carrito_2[0];
            $decimales = $explode_carrito_2[1];

            $subtotal = intval($miles.$decimales);
            
            foreach (Cart::content() as $carrito) {
                $valor_adicion[] = $carrito->options->valores_adicionales;
            }

            $adicionales = array_sum($valor_adicion);

            $informacion[0] = number_format($subtotal+$adicionales+$tienda[0]->domicilio);
            $informacion[1] = number_format($tienda[0]->domicilio);


            return $informacion;
         }

        $i = 0;
        $carrito = [];
        $carrito_total = [];
        $producto_unico = [];
        $total_adicion = [];
        $valor_adicion = [];

        foreach (Cart::content() as $pedido_carrito_validate) {
            $carrito[] = $pedido_carrito_validate->id;
            $carrito_total[] = $pedido_carrito_validate;
            $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
        }

        if ($opcion == "domi") {

            $tiendas = DB::table('tiendas')->where('id_tienda', '=', $codigo_tienda)->get();

            foreach ($tiendas as $info_tienda) {
                $domicilio = $info_tienda->domicilio;
            }
        } else {
            $domicilio = 0;
        }


        $total_adicionales = array_sum($valor_adicion);

        $array = explode('"', Cart::subtotal());
        foreach ($array as $value) {
            $total_prueba = explode(",", $value); 
        }

        $tamano_array_total = sizeof($total_prueba);

        if($tamano_array_total == 3){

            $solo_miles = explode(".", $total_prueba[2]);

            $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

            $producto_unico[1] = $subtotal+$domicilio+$total_adicionales;

            $producto_unico[2] = $subtotal;
            $producto_unico[3] = $total_adicionales;

        }else if($tamano_array_total <= 2){
            
            $solo_miles = explode(".", $total_prueba[1]);

            $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

            $producto_unico[1] = $subtotal+$domicilio+$total_adicionales;

            $producto_unico[2] = $subtotal;
            $producto_unico[3] = $total_adicionales;

        }else{

        }


        return view('conf.opcion_envio', compact('producto_unico', 'domicilio'));
    }

    public function formulario($opcion, $codigo_tienda) {

        if ($opcion == "Whatsapp-domicilio") {

            $tiendas = DB::table('tiendas')->where('id_tienda', '=', $codigo_tienda)->get();

            foreach ($tiendas as $info_tienda) {
                $codigo_tienda = $info_tienda->id_tienda;
                $wompi = $info_tienda->llave_publica;
            }

            $zonas = DB::table('detalle_zonas_tiendas')
                        ->select('zonas_tiendas.*', 'detalle_zonas_tiendas.*', 'tiendas.*')
                        ->join('zonas_tiendas', 'detalle_zonas_tiendas.id_zona_detalle', '=', 'zonas_tiendas.id_zona')
                        ->join('tiendas', 'detalle_zonas_tiendas.codigo_tienda_detalle', '=', 'tiendas.id_tienda')
                        ->where('detalle_zonas_tiendas.codigo_tienda_detalle', $codigo_tienda)
                        ->get();


            if(empty($wompi)){
                $validate_wompi = 0;
            }else{
                $validacion = explode("_", $wompi);

                if($validacion[0] === "pub"){
                    $validate_wompi = 1;
                }else{
                    $validate_wompi = 0;
                }

            }

            return view('conf.formulario-whatsapp-domicilio', compact('codigo_tienda', 'validate_wompi', 'zonas'));
            
        } else {

            $tiendas = DB::table('tiendas')->where('id_tienda', '=', $codigo_tienda)->get();

            foreach ($tiendas as $info_tienda) {
                $wompi = $info_tienda->llave_publica;
            $wompi = $info_tienda->llave_publica;
            }

            $zonas = DB::table('detalle_zonas_tiendas')
                        ->select('zonas_tiendas.*', 'detalle_zonas_tiendas.*', 'tiendas.*')
                        ->join('zonas_tiendas', 'detalle_zonas_tiendas.id_zona_detalle', '=', 'zonas_tiendas.id_zona')
                        ->join('tiendas', 'detalle_zonas_tiendas.codigo_tienda_detalle', '=', 'tiendas.id_tienda')
                        ->where('detalle_zonas_tiendas.codigo_tienda_detalle', $codigo_tienda)
                        ->get();

            if(empty($wompi)){
                $validate_wompi = 0;
            }else{
                $validacion = explode("_", $wompi);

                if($validacion[0] === "pub"){
                    $validate_wompi = 1;
                }else{
                    $validate_wompi = 0;
                }

            }


            return view('conf.formulario-whatsapp-local', compact('codigo_tienda', 'validate_wompi', 'zonas'));
        }
    }

    public function sugerencia($codigo_tienda) {
        return view('conf.formulario_buzon_sugerencia', compact('codigo_tienda'));
    }

    public function registroSugerencia(Request $request){

        $request->validate([
            'nombreSugerencia' => 'required',
            'telefonoSugerencia' => 'required',
            'estrellas' => 'required'
        ]);

        DB::table('formulario_buzon_sugerencia')->insert([
            'nombre_persona' => $request->nombreSugerencia,
            'telefono_persona' => $request->telefonoSugerencia,
            'calificacion_persona' => $request->estrellas,
            'mensaje_sugerencia' => $request->comentariosSugerencia
        ]);



        $buzon_ultimo_registro = DB::table('formulario_buzon_sugerencia')->orderby('id','DESC')->take(1)->get();

        foreach ($buzon_ultimo_registro as $value) {
            DB::table('detalle_buzon_tienda')->insert([
                'codigo_tienda_buzon_fk' => $request->codigo_tienda,
                'codigo_formulario_buzon_fk' => $value->id
            ]);
        }

        DB::table('tabla_notificaciones')->insert([
            'seccion_notificaciones' => 'formulario-sugerencia',
            'codigo_tienda_notificaciones' => $request->codigo_tienda,
            'estado_notificacion' => 'recibida'
        ]);


        $tienda = DB::table('tiendas')->where('id_tienda', $request->codigo_tienda)->get();

        return redirect()->route('tienda.principal', $tienda[0]->url_tienda)
            ->with('status_success','Tu sugerencia se ha registrado correctamente!');
    }

    public function total_carrito(){
        $i = 0;
        $carrito = [];
        $carrito_total = [];
        $producto_unico = [];
        $total_adicion = [];



        $array = explode('"', Cart::subtotal());

        $array_1 = explode(",", $array[0]);

        $total_valor = array_sum($array_1);
        
        if ($total_valor == "0") {
            
            $producto_unico[1] = "0";
            $producto_unico[2] = "0";
            $producto_unico[3] = "0";

        } else {

            foreach (Cart::content() as $pedido_carrito_validate) {
                $carrito[] = $pedido_carrito_validate->id;
                $carrito_total[] = $pedido_carrito_validate;
                $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
            }

            $domicilio = 0;

            $total_adicionales = array_sum($valor_adicion);

            foreach ($array as $value) {
                $total_prueba = explode(",", $value); 
            }

            $tamano_array_total = sizeof($total_prueba);

            if($tamano_array_total == 3){

                $solo_miles = explode(".", $total_prueba[2]);

                $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                $producto_unico[1] = $subtotal+$domicilio+$total_adicionales;

                $producto_unico[2] = $subtotal;
                $producto_unico[3] = $total_adicionales;

            }else if($tamano_array_total <= 2){
                
                $solo_miles = explode(".", $total_prueba[1]);

                $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                $producto_unico[1] = $subtotal+$domicilio+$total_adicionales;

                $producto_unico[2] = $subtotal;
                $producto_unico[3] = $total_adicionales;

            }else{

            }

        }

        return $producto_unico;
    }

    public function listadoPedido($codigo_tienda) {

        $i = 0;
        $carrito = [];
        $carrito_total = [];
        $producto_unico = [];
        $total_adicion = [];

        $tiendas = DB::table('tiendas')->where('id_tienda', '=', $codigo_tienda)->get();

        foreach (Cart::content() as $pedido_carrito_validate) {
            $carrito[] = $pedido_carrito_validate->id;
            $carrito_total[] = $pedido_carrito_validate;
            $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
        }

        foreach ($tiendas as $info_tienda) {
            $domicilio = $info_tienda->domicilio;
            $nombre_tienda = $info_tienda->nombre_tienda;
        }

        $total_adicionales = array_sum($valor_adicion);

        $array = explode('"', Cart::subtotal());
        foreach ($array as $value) {
            $total_prueba = explode(",", $value); 
        }

        $tamano_array_total = sizeof($total_prueba);

        if($tamano_array_total == 3){

            $solo_miles = explode(".", $total_prueba[2]);

            $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

            $producto_unico[1] = $subtotal+$domicilio+$total_adicionales;

            $producto_unico[2] = $subtotal;
            $producto_unico[3] = $total_adicionales;

        }else if($tamano_array_total <= 2){
            
            $solo_miles = explode(".", $total_prueba[1]);

            $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

            $producto_unico[1] = $subtotal+$domicilio+$total_adicionales;

            $producto_unico[2] = $subtotal;
            $producto_unico[3] = $total_adicionales;

        }else{

        }


        return view('conf.listado', compact('producto_unico', 'domicilio', 'nombre_tienda'));
    }

    public function carrito($codigo_tienda) {

        $i = 0;
        $carrito = [];
        $carrito_total = [];
        $producto_unico = [];
        $total_adicion = [];

        $tiendas = DB::table('tiendas')->where('id_tienda', '=', $codigo_tienda)->get();

        foreach (Cart::content() as $pedido_carrito_validate) {
            $carrito[] = $pedido_carrito_validate->id;
            $carrito_total[] = $pedido_carrito_validate;
            $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
        }

        foreach ($tiendas as $info_tienda) {
            $domicilio = $info_tienda->domicilio;
            $nombre_tienda = $info_tienda->nombre_tienda;
            $wompi = $info_tienda->llave_publica;
        }

        if(!empty($wompi)){
            $validate_wompi = false;
        }else{
            $validate_wompi = true;
        }

        $total_adicionales = array_sum($valor_adicion);

        $array = explode('"', Cart::subtotal());
        foreach ($array as $value) {
            $total_prueba = explode(",", $value); 
        }

        $tamano_array_total = sizeof($total_prueba);

        if($tamano_array_total == 3){

            $solo_miles = explode(".", $total_prueba[2]);

            $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

            $producto_unico[1] = $subtotal+$domicilio+$total_adicionales;

            $producto_unico[2] = $subtotal;
            $producto_unico[3] = $total_adicionales;

        }else if($tamano_array_total <= 2){
            
            $solo_miles = explode(".", $total_prueba[1]);

            $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

            $producto_unico[1] = $subtotal+$domicilio+$total_adicionales;

            $producto_unico[2] = $subtotal;
            $producto_unico[3] = $total_adicionales;

        }else{

        }


        return view('conf.carrito', compact('producto_unico', 'domicilio', 'nombre_tienda', 'validate_wompi'));
    }

    public function formularioEnvioNoPay(Request $request){

        if ($request->opcion == "whatsapp") {

            $tiendas = DB::table('tiendas')->where('id_tienda', '=', $request->codigo_tienda)->get();

            foreach ($tiendas as $tienda) {
                $nombre_tienda = $tienda->nombre_tienda;
                $domicilio = $tienda->domicilio;
                $telefono = $tienda->telefono_movil;
            }

            foreach (Cart::content() as $pedido_carrito_validate) {
                $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
            }

            foreach ($tiendas as $info_tienda) {
                $domicilio = $info_tienda->domicilio;
            }

            $array = explode('"', Cart::subtotal());

            $array_1 = explode(",", $array[0]);

            $total_valor = array_sum($array_1);

            if ($total_valor == "0") {

                $producto_unico[1] = "0";
                $producto_unico[2] = "0";
                $producto_unico[3] = "0";

            } else {
                

                $total_adicionales = array_sum($valor_adicion);

                $array = explode('"', Cart::subtotal());
            
                foreach ($array as $value) {
                    $total_prueba = explode(",", $value); 
                }

                $tamano_array_total = sizeof($total_prueba);

                if($tamano_array_total == 3){

                    $solo_miles = explode(".", $total_prueba[2]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;

                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else if($tamano_array_total <= 2){
                    
                    $solo_miles = explode(".", $total_prueba[1]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;
                    $total_compra = $subtotal+$total_adicionales+$domicilio;
                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else{

                }
            }            
            

            $codigo_pedido = rand(0, 8000);
            $nombres_productos = [];
            $nombres_adicionales = [];

            foreach (Cart::content() as $pedido) {
                $nombres_productos[] = $pedido->name . ",";
                $nombres_adicionales[] = $pedido->options->adicionales . ",";
            };
            
            $productos_string = implode(",", $nombres_productos);
            $adicionales_string = implode(",", $nombres_adicionales);


            if(empty($request->indicacion_adicional)){
                $indicacionadicional = "Sin informacion";
            }else{
                $indicacionadicional = str_replace('#', 'Numero', $request->indicacion_adicional);
            }

            if(empty($request->direccion)){
                $comentariosDomicilio = "Sin informacion";
            }else{
                $comentariosDomicilio = str_replace('#', ' ', $request->comentarios);
            }


            $direccion = str_replace('#', 'Numero', $request->direccion);


            DB::table('pedido_domicilio')->insert([
                'codigo_pedido' => $codigo_pedido,
                'nombre' => $request->nombre,
                'telefono' => $request->telefono,
                'direccion' => $request->direccion,
                'barrio' => $request->barrio,
                'indicacion_adicional' => $indicacionadicional,
                'comentarios' => $comentariosDomicilio,
                'productos' => $productos_string,
                'adicionales' => $adicionales_string,
                'domicilio' => $domicilio,
                'valor_adicional' => $producto_unico[3],
                'valor_productos' => $producto_unico[2],
                'total_pedido' => $producto_unico[1]
            ]);
            
            if(isset($request->zonas)){

                $zona = DB::table('zonas_tiendas')->where('id_zona', $request->zonas)->get();

                $celular = $zona[0]->celular_zona;

            }else{
                $celular = $telefono;
            }


                $referencia_pago = "";
            
            date_default_timezone_set('America/Bogota');

            DB::table('historial_pedido')->insert([
                'codigo_pedido' => $codigo_pedido,
                'codigo_tienda' => $request->codigo_tienda,
                'estado_pedido' => 'Recibido',
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            
            
            echo "https://wa.me/57" . $celular . "?text=¡Hola " . $nombre_tienda . " soy " . $request->nombre . " y quiero hacer el siguiente pedido: %0a";

            foreach (Cart::content() as $pedido) {
                echo "%0a - " . $pedido->qty;
                echo " x " . $pedido->name;
                echo " $ (" . $pedido->qty * $pedido->price . ") %0a *comentarios del producto:* " . $pedido->options->comentarios . "%0a *Adicionales del producto:* " . $pedido->options->adicionales;
            };

            echo "%0a%0aMe encuentro ubicado en la siguiente dirección: " . $direccion . " %0a%0a Indicacion adicional: " . $indicacionadicional ." %0a%0a Barrio: " . $request->barrio . ".%0a%0a Valor compra: *$".$producto_unico[1]."* %0a%0a Costo domicilio: *$".$domicilio."* %0a%0a *TOTAL A PAGAR: $" . intval($total_compra)."* %0a%0a" .$referencia_pago. "%0a%0a Comentarios: " . $comentariosDomicilio . " %0a%0a *Muchas gracias*";

        }else{

            $tiendas = DB::table('tiendas')->where('id_tienda', '=', $request->codigo_tienda)->get();

            foreach (Cart::content() as $pedido_carrito_validate) {
                $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
            }

            foreach ($tiendas as $info_tienda) {
                $domicilio = $info_tienda->domicilio;
                $nombre_tienda = $info_tienda->url_tienda;
                $telefono = $info_tienda->telefono_movil;
            }

            $array = explode('"', Cart::subtotal());

            $array_1 = explode(",", $array[0]);

            $total_valor = array_sum($array_1);

            if ($total_valor == "0") {
                $producto_unico[1] = "0";
                $producto_unico[2] = "0";
                $producto_unico[3] = "0";
            } else {
                

                $total_adicionales = array_sum($valor_adicion);

                $array = explode('"', Cart::subtotal());
            
                foreach ($array as $value) {
                    $total_prueba = explode(",", $value); 
                }

                $tamano_array_total = sizeof($total_prueba);

                if($tamano_array_total == 3){

                    $solo_miles = explode(".", $total_prueba[2]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;

                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else if($tamano_array_total <= 2){
                    
                    $solo_miles = explode(".", $total_prueba[1]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;

                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else{

                }
            }

            
            
            $codigo_pedido = rand(0, 8000);
            $nombres_productos = [];
            $nombres_adicionales = [];

            foreach (Cart::content() as $pedido) {
                $nombres_productos[] = $pedido->name . ",";
                $nombres_adicionales[] = $pedido->options->adicionales . ",";
            };
            
            $productos_string = implode(",", $nombres_productos);
            $adicionales_string = implode(",", $nombres_adicionales);

            if(empty($request->comentarios)){
                $comentarios = "Sin comentarios";
            }else{
                $comentarios = $request->comentarios;
            }



            DB::table('pedido_local')->insert([
                'codigo_pedido' => $codigo_pedido,
                'nombre' => $request->nombre,
                'telefono' => $request->telefono,
                'hora_llegada' => $request->hora_llegada,
                'comentarios' => $comentarios,
                'productos' => $productos_string,
                'adicionales' => $adicionales_string,
                'domicilio' => $domicilio,
                'valor_adicional' => $producto_unico[3],
                'valor_productos' => $producto_unico[2],
                'total_pedido' => $producto_unico[1]
            ]);


            if(isset($request->zonas)){

                $zona = DB::table('zonas_tiendas')->where('id_zona', $request->zonas)->get();

                $celular = $zona[0]->celular_zona;

            }else{
                $celular = $telefono;
            }
            
            $referencia_pago = "";
            
            date_default_timezone_set('America/Bogota');
            
            DB::table('historial_pedido')->insert([
                'codigo_pedido' => $codigo_pedido,
                'codigo_tienda' => $request->codigo_tienda,
                'estado_pedido' => 'Recibido',
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            

             echo "https://wa.me/57" . $celular . "?text=¡Hola " . $nombre_tienda . " soy " . $request->nombre . " y quiero hacer el siguiente pedido: %0a";

            foreach (Cart::content() as $pedido) {
                echo "%0a - " . $pedido->qty;
                echo " x " . $pedido->name;
                echo " $ (" . $pedido->qty * $pedido->price . ") %0a *comentarios del producto:* " . $pedido->options->comentarios . "%0a *Adicionales del producto:* " . $pedido->options->adicionales."%0a";
            };

            echo "%0a%0aLlegaré al local a las: *" .$request->hora_llegada."* %0a%0a Valor compra: *$" . intval($producto_unico[1]) . "* %0a%0a" . $referencia_pago . "* %0a%0a *Se recogerá en el local* %0a%0a Comentarios: " . $comentarios . " %0a%0a *Muchas gracias*";     
        };
    }

    public function formularioEnvio(Request $request, $reference) {

        date_default_timezone_set('America/Bogota');

        if ($request->opcion == "whatsapp") {

            $tiendas = DB::table('tiendas')->where('id_tienda', '=', $request->codigo_tienda)->get();

            foreach ($tiendas as $tienda) {
                $nombre_tienda = $tienda->nombre_tienda;
                $domicilio = $tienda->domicilio;
                $telefono = $tienda->telefono_movil;
            }

            foreach (Cart::content() as $pedido_carrito_validate) {
                $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
            }

            foreach ($tiendas as $info_tienda) {
                $domicilio = $info_tienda->domicilio;
            }

            $array = explode('"', Cart::subtotal());

            $array_1 = explode(",", $array[0]);

            $total_valor = array_sum($array_1);

            if ($total_valor == "0") {

                $producto_unico[1] = "0";
                $producto_unico[2] = "0";
                $producto_unico[3] = "0";

            } else {
                

                $total_adicionales = array_sum($valor_adicion);

                $array = explode('"', Cart::subtotal());
            
                foreach ($array as $value) {
                    $total_prueba = explode(",", $value); 
                }

                $tamano_array_total = sizeof($total_prueba);

                if($tamano_array_total == 3){

                    $solo_miles = explode(".", $total_prueba[2]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;

                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else if($tamano_array_total <= 2){
                    
                    $solo_miles = explode(".", $total_prueba[1]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;
                    $total_compra = $subtotal+$total_adicionales+$domicilio;
                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else{

                }
            }            
            

            $codigo_pedido = rand(0, 8000);
            $nombres_productos = [];
            $nombres_adicionales = [];

            foreach (Cart::content() as $pedido) {
                $nombres_productos[] = $pedido->name . ",";
                $nombres_adicionales[] = $pedido->options->adicionales . ",";
            };
            
            $productos_string = implode(",", $nombres_productos);
            $adicionales_string = implode(",", $nombres_adicionales);


            if(empty($request->indicacion_adicional)){
                $indicacionadicional = "Sin informacion";
            }else{
                $indicacionadicional = str_replace('#', 'Numero', $request->indicacion_adicional);
            }

            if(empty($request->direccion)){
                $comentariosDomicilio = "Sin informacion";
            }else{
                $comentariosDomicilio = str_replace('#', ' ', $request->comentarios);
            }


            $direccion = str_replace('#', 'Numero', $request->direccion);


            DB::table('pedido_domicilio')->insert([
                'codigo_pedido' => $codigo_pedido,
                'nombre' => $request->nombre,
                'telefono' => $request->telefono,
                'direccion' => $request->direccion,
                'barrio' => $request->barrio,
                'indicacion_adicional' => $indicacionadicional,
                'comentarios' => $comentariosDomicilio,
                'productos' => $productos_string,
                'adicionales' => $adicionales_string,
                'domicilio' => $domicilio,
                'valor_adicional' => $producto_unico[3],
                'valor_productos' => $producto_unico[2],
                'total_pedido' => $producto_unico[1],
                'referencia_pago_wompi' => $reference,
            ]);
            
            if(isset($reference)){
                $referencia_pago = "PAGUE CON WOMPI *Referencia*: ".$reference;
            }else{
                $referencia_pago = "";
            }
            
            date_default_timezone_set('America/Bogota');

            DB::table('historial_pedido')->insert([
                'codigo_pedido' => $codigo_pedido,
                'codigo_tienda' => $request->codigo_tienda,
                'estado_pedido' => 'Recibido',
                'creado' => date('Y-m-d H:i:s'),
            ]);
            
            
            echo "https://wa.me/57" . $telefono . "?text=¡Hola " . $nombre_tienda . " soy " . $request->nombre . " y quiero hacer el siguiente pedido: %0a";

            foreach (Cart::content() as $pedido) {
                echo "%0a - " . $pedido->qty;
                echo " x " . $pedido->name;
                echo " $ (" . $pedido->qty * $pedido->price . ") %0a *comentarios del producto:* " . $pedido->options->comentarios . "%0a *Adicionales del producto:* " . $pedido->options->adicionales;
            };

            echo "%0a%0aMe encuentro ubicado en la siguiente dirección: " . $direccion . " %0a%0a Indicacion adicional: " . $indicacionadicional ." %0a%0a Barrio: " . $request->barrio . ".%0a%0a Valor compra: *$".intval($producto_unico[1])."* %0a%0a Costo domicilio: *$".$domicilio."* %0a%0a *TOTAL A PAGAR: $" . intval($total_compra)."* %0a%0a" .$referencia_pago. "%0a%0a Comentarios: " . $comentariosDomicilio . " %0a%0a *Muchas gracias*";

        }else{
            
            $tiendas = DB::table('tiendas')->where('id_tienda', '=', $request->codigo_tienda)->get();

            foreach (Cart::content() as $pedido_carrito_validate) {
                $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
            }

            foreach ($tiendas as $info_tienda) {
                $domicilio = $info_tienda->domicilio;
                $nombre_tienda = $info_tienda->url_tienda;
                $telefono = $info_tienda->telefono_movil;
            }

            $array = explode('"', Cart::subtotal());

            $array_1 = explode(",", $array[0]);

            $total_valor = array_sum($array_1);

            if ($total_valor == "0") {
                $producto_unico[1] = "0";
                $producto_unico[2] = "0";
                $producto_unico[3] = "0";
            } else {

                $total_adicionales = array_sum($valor_adicion);

                $array = explode('"', Cart::subtotal());
            
                foreach ($array as $value) {
                    $total_prueba = explode(",", $value); 
                }

                $tamano_array_total = sizeof($total_prueba);

                if($tamano_array_total == 3){

                    $solo_miles = explode(".", $total_prueba[2]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;

                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else if($tamano_array_total <= 2){
                    
                    $solo_miles = explode(".", $total_prueba[1]);

                    $subtotal = $total_prueba[0].$total_prueba[1].$solo_miles[0];

                    $producto_unico[1] = $subtotal+$total_adicionales;

                    $producto_unico[2] = $subtotal;
                    $producto_unico[3] = $total_adicionales;

                }else{

                }
            }

            
            
            $codigo_pedido = rand(0, 8000);
            $nombres_productos = [];
            $nombres_adicionales = [];

            foreach (Cart::content() as $pedido) {
                $nombres_productos[] = $pedido->name . ",";
                $nombres_adicionales[] = $pedido->options->adicionales . ",";
            };
            
            $productos_string = implode(",", $nombres_productos);
            $adicionales_string = implode(",", $nombres_adicionales);

            if(empty($request->comentarios)){
                $comentarios = "Sin comentarios";
            }else{
                $comentarios = $request->comentarios;
            }


            DB::table('pedido_local')->insert([
                'codigo_pedido' => $codigo_pedido,
                'nombre' => $request->nombre_wompi,
                'telefono' => $request->telefono_wompi,
                'hora_llegada' => $request->hora_llegada,
                'comentarios' => $comentarios,
                'productos' => $productos_string,
                'adicionales' => $adicionales_string,
                'domicilio' => $domicilio,
                'valor_adicional' => $producto_unico[3],
                'valor_productos' => $producto_unico[2],
                'total_pedido' => $producto_unico[1],
                'referencia_pago_wompi' => $reference,
            ]);
            

            if(isset($reference)){
                $referencia_pago = "PAGUE EN LINEA *Referencia*: ".$reference;
            }else{
                $referencia_pago = "";
            }
            
            date_default_timezone_set('America/Bogota');
            DB::table('historial_pedido')->insert([
                'codigo_pedido' => $codigo_pedido,
                'codigo_tienda' => $request->codigo_tienda,
                'estado_pedido' => 'Recibido',
                'creado' => date('Y-m-d H:i:s'),
            ]);
            

            echo "https://wa.me/57" . $telefono . "?text=¡Hola " . $nombre_tienda . " soy " . $request->nombre . " y quiero hacer el siguiente pedido: %0a";

            foreach (Cart::content() as $pedido) {
                echo "%0a - " . $pedido->qty;
                echo " x " . $pedido->name;
                echo " $ (" . $pedido->qty * $pedido->price . ") %0a *comentarios del producto:* " . $pedido->options->comentarios . "%0a *Adicionales del producto:* " . $pedido->options->adicionales."%0a";
            };
            echo "%0a%0aLlegaré al local a las: *" .$request->hora_llegada."* %0a%0a Valor compra: *$" . intval($producto_unico[1]) . "* %0a%0a" . $referencia_pago . "* %0a%0a *Se recogerá en el local* %0a%0a Comentarios: " . $comentarios . " %0a%0a *Muchas gracias*";     
        };
    }

    public function buzondesugerencia(Request $request){
    }

    public function eliminar(){
        Cart::destroy();
    }

    public function registro_formulario_cliente(Request $request){
        
        DB::table('formulario_contacto')->insert([
            'nombre' => $request->nombre,
            'empresa' => $request->empresa,
            'correo' => $request->correo,
            'telefono' => $request->telefono,
            'mensaje' => $request->mensaje
        ]);

        return true;
    }

    public function informacion_tienda($id){
        $tiendas = Db::table('tiendas')->where('id_tienda', $id)->get();
        return view('conf.informacion_tienda', compact('tiendas'));
        
    }
}
