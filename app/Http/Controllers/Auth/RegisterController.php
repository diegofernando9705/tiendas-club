<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use DB;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {

        $codigo_tienda = rand(1, 9999);

        DB::table('personas')->insert([
            'foto_persona'       => 'Tiendas club',
            'primer_nombre'      => 'Tiendas club',
            'segundo_nombre'     => '',
            'primer_apellido'    => 'Tiendas club',
            'segundo_apellido'   => '',
            'telefono'           => '12345678',
            'correo_electronico' => $data['email'],
            'id_ciudad'          => '0',
            'direccion'          => 'Tiendas club',
            'barrio'             => 'Tiendas club',
            'user_register'      => '0',
            'estado_persona'     => '1',
        ]);

        DB::table('tiendas')->insert([
            'id_tienda'          => $codigo_tienda,
            'image_tienda'       => 'Tiendas Club',
            'nombre_tienda'      => 'Tiendas Club',
            'descripcion_tienda' => 'Tiendas Club',
            'direccion_tienda'   => 'Tiendas Club',
            'horario_tienda'     => 'Tiendas Club',
            'id_ciudad'          => '1',
            'telefono_movil'     => '1',
            'telefono_fijo'      => '1',
            'instagram_link'     => 'Tiendas Club',
            'facebook_link'      => 'Tiendas Club',
            'twitter_link'       => 'Tiendas Club',
            'estado_tienda'      => '0',
        ]);

        User::create([
            'name'     => $data['name'],
            'email'    => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $correo = $data['name'];

        $sql_personas = DB::table('personas')
            ->join('users', 'users.email', '=', 'personas.correo_electronico')
            ->select('users.id as id_usuario', 'personas.id as id_persona', 'personas.correo_electronico', 'users.email')
            ->orderBy('users.id', 'desc')
            ->first();

        DB::table('personas')
            ->where('id', $sql_personas->id_persona)
            ->update(array('user_register' => $sql_personas->id_usuario));

        DB::table('detalle_usuario_tiendas')->insert([
            'id_persona' => $sql_personas->id_persona,
            'id_tienda'  => $codigo_tienda,
        ]);

        /* REGISTRO AL ENVIO */

    }
}
