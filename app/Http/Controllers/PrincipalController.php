<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Cart;

class PrincipalController extends Controller {

    var $valor = 0;

    public function aliados($tienda) {

        $array_categorias = [];
        $categorias = [];

        $productos = DB::table('productos')->orderBy('productos.id_categoria', 'asc')->get();

        $total_categorias = DB::table('detalle_tienda_productos')
                ->select('categoria.nombre_categoria', 'categoria.id as categoria_id', 'productos.id', 'productos.id_categoria', 'tiendas.url_tienda', 'tiendas.id_tienda')
                ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                ->join('tiendas', 'detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                ->join('categoria', 'productos.id_categoria', '=', 'categoria.id')
                ->where('tiendas.url_tienda', '=', $tienda)
                ->get();

        foreach ($total_categorias as $cat) {
            $array_categorias[] = $cat->categoria_id;
        }

        $catagoria_unica = array_unique($array_categorias);


        foreach ($catagoria_unica as $value) {
            $sql = DB::table('categoria')->where('id', '=', $value)->get();

            $categorias[] = $sql;
        }


        $personas = DB::table('personas')->get();
        $tiendas = DB::table('tiendas')->where('url_tienda', $tienda)->get();

        $productos_array = [];

        $detalle = DB::table('detalle_tienda_productos')
                ->select('tiendas.id_tienda', 'productos.id', 'detalle_tienda_productos.id_tienda', 'detalle_tienda_productos.id_producto', 'detalle_tienda_productos.id as id_detalle')
                ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                ->join('tiendas', 'detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                ->where('tiendas.url_tienda', '=', $tienda)
                ->get();

        foreach ($detalle as $producto_arr) {
            $productos_array[] = $producto_arr->id_producto;
        }

        foreach ($tiendas as $tienda_unica) {
            $nombre_tienda = $tienda_unica->url_tienda;
        }

        return view("public.tiendas.index", compact("productos", "categorias", "detalle", "tiendas", "productos_array", "nombre_tienda"));
    }

    

    

    public function carrito($codigo_tienda) {

        $i = 0;
        $carrito = [];
        $carrito_total = [];
        $producto_unico = [];
        $total_adicion = [];

        $tiendas = DB::table('tiendas')->where('id_tienda', '=', $codigo_tienda)->get();

        foreach (Cart::content() as $pedido_carrito_validate) {
            $carrito[] = $pedido_carrito_validate->id;
            $carrito_total[] = $pedido_carrito_validate;
            $valor_adicion[] = $pedido_carrito_validate->options->valores_adicionales;
        }

        foreach ($tiendas as $info_tienda) {
            $domicilio = $info_tienda->domicilio;
            $nombre_tienda = $info_tienda->nombre_tienda;
        }

        $array = explode('"', Cart::subtotal());

        $array_1 = explode(",", $array[0]);

        $centavos = explode(".", $array_1[1]);

        $miles = $array_1[0];
        $centavos_final = $centavos[0];

        $subtotal = $miles . $centavos_final;

        $total_adicionales = array_sum($valor_adicion);

        $producto_unico[1] = $subtotal + $domicilio + $total_adicionales;
        $producto_unico[2] = $subtotal;
        $producto_unico[3] = $total_adicionales;

        return view('conf.carrito', compact('producto_unico', 'domicilio', 'nombre_tienda'));
    }

   

    

    public function borrarCarrito() {
        Cart::destroy();
    }

    

    public function agregarComentario($id_producto, $comentario) {

        $productos = [];

        foreach (Cart::content() as $pedido) {
            if ($pedido->id == $id_producto) {

                $item = Cart::get($pedido->rowId);
                $option = ['code_adicionales' => $pedido->options->code_adicionales, 'name_adicionales' => $pedido->options->name_adicionales, 'valor_adicionales' => $pedido->options->valor_adicionales, 'comentarios' => $comentario, 'tipo' => 'productos'];

                Cart::update($pedido->rowId, ['options' => $option]);
            }
        }
    }

    public function busquedaProductoWeb($pedido, $tienda) {

        $productos = DB::table('productos')->where('name', 'LIKE', '%' . $pedido . '%')->get();

        $personas = DB::table('personas')->get();
        $tiendas = DB::table('tiendas')->where('url_tienda', $tienda)->get();

        $productos_array = [];

        $detalle = DB::table('detalle_tienda_productos')
                ->select('tiendas.id_tienda', 'productos.id', 'detalle_tienda_productos.id_tienda', 'detalle_tienda_productos.id_producto', 'detalle_tienda_productos.id as id_detalle')
                ->join('productos', 'detalle_tienda_productos.id_producto', '=', 'productos.id')
                ->join('tiendas', 'detalle_tienda_productos.id_tienda', '=', 'tiendas.id_tienda')
                ->where('tiendas.url_tienda', '=', $tienda)
                ->get();

        foreach ($detalle as $producto_arr) {
            $productos_array[] = $producto_arr->id_producto;
        }

        return view("public.tiendas.resultado-busqueda-producto", compact("productos", "detalle", "tiendas", "productos_array"));
    }





    

}
