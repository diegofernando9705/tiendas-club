<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TiendaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'imgTienda'         => 'required|mimes:jpg,jpeg,png',
            'nombreTienda'      => 'required',
            'descripcionTienda' => 'required',
            'direccionTienda'   => 'required',
            'horarioTienda'     => 'required',
            'ciudadTienda'      => 'required',
            'representante'     => 'required|not_in:0',
            'telMovil'          => 'required',
        ];
    }
}
