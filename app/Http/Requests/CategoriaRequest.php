<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'imgCategoria'         => 'required|mimes:jpg,jpeg,png',
            'nombreCategoria'      => 'required',
            'descripcionCategoria' => 'required',
            'estado_categoria'     => 'required',
        ];
    }
}
