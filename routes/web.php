<?php

use Illuminate\Support\Facades\Route;



Route::get('/', 'PublicoController@home');


Route::get('/{tienda}', 'PublicoController@index');
Route::get('/transaction/{tienda}', 'PublicoController@confirmacion_transferencia');


Route::get('/{tienda}', 'PublicoController@index')->name('tienda.principal');
Route::get('/productoCompra/{codigo_producto}', 'PublicoController@infoProducto');
Route::post('/productoAgregado', 'PublicoController@agregarProCarrito');

Route::get('/search/{nombre_producto}/{id_tienda}', 'PublicoController@consultaProducto'); // Búsqueda del producto

Route::get('/search/categoria/{id_categoria}/{id_tienda}', 'PublicoController@consultaProductoCat'); // Búsqueda del producto

  
/* ventana modal carrito de compras */
Route::get('/eliminar/{id}/{codigo_tienda}', 'PublicoController@eliminarProducto'); //Opion eliminar producto desde el Carrito
Route::get('/carritoOpcion/{opcion}/{codigo_tienda}', 'PublicoController@opcionEnvio'); //Opion domicilio o local
Route::get('/formulario/{opcion}/{codigo_tienda}', 'PublicoController@formulario'); //Abrir Formulario segun Local o domicilio
Route::post('/envioformulario/wompi/{reference}', 'PublicoController@formularioEnvio'); //Envio Formulario COMPRA EN LINEA segun Local o domicilio
Route::post('/envioformulario', 'PublicoController@formularioEnvioNoPay'); //Envio Formulario COMPRA EN LINEA segun Local o domicilio

Route::get('/sugerencia/sugerencia/{codigo_tienda}', 'PublicoController@sugerencia'); //Abrir Formulario sugerencia
Route::post('/envioformulario/sugerencia', 'PublicoController@registroSugerencia'); //Registro del formulario de sugerencia


Route::get('/carrito/{codigo_tienda}', 'PublicoController@carrito');
Route::get('/carrito/listado/{codigo_tienda}', 'PublicoController@listadoPedido');

Route::get('/capturar/total_carrito', 'PublicoController@total_carrito');

Route::get('wompi/api', 'PublicoController@api_wompi');

Route::post('/wompi/cliente/', 'PublicoController@client'); // se solicita el token de aceptacion de Wompi
Route::post('/wompi/get/method_pay', 'PublicoController@method_pay'); // segun el metodo de pago, mostrar formulario

//Route::post('/wompi/pay/token', 'PublicoController@pay_token'); // se enviara datos de informacion al API para crear transferencia

Route::post('/wompi/pay/success', 'PublicoController@pay_wompi'); // se enviara datos de las tarjetas de credito para realizar el pago


Route::get('/wompi/pay/result/{id_transaction}', 'PublicoController@transaction'); //resultado de la transacciòn

Route::GET('/wompi/verification/nequi/{id?}', 'PublicoController@verification_nequi'); // se enviara datos de las tarjetas de credito para realizar el pago
Route::post('/wompi/pago/nequi/{token}', 'PublicoController@pago_con_nequi'); // funcion para pagar con NEQUI


//* Route::get('/domicilio/{tienda}', 'PrincipalController@formpedido'); */


Route::get('/limpiar/carrito/tiendas', 'PublicoController@eliminar'); //Ver adicionales del producto

Route::post('/registro/formulario/contacto', 'PublicoController@registro_formulario_cliente'); //Ver adicionales del producto

Route::get('/informacion_tienda/{code}', 'PublicoController@informacion_tienda'); //Ver adicionales del producto

