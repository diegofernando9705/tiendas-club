$(document).ready(function () {

        window.onbeforeunload=function(){
            alert();
            $.ajax({
                type: 'get',
                url: '/borrar-carrito',
                success: function (data) {}
            });
            ///return "Are you sure you want to exit?";
        }

    $('.mi-selector').select2();

    $("#departamento").change(function () {
        var valor = $(this).val();

        $.ajax({
            type: 'post',
            url: '/consulta-ciudad-id/search/' + valor,
            success: function (data) {
                document.getElementById("ciudad").innerHTML = data;
            }
        });
    });

    $(window).scroll(function () {
        if ($(window).scrollTop() > 80) {

            $("header").attr("style", "position:fixed; background-color:white; -webkit-box-shadow: 0px 7px 18px -2px rgba(0,0,0,0.4); -moz-box-shadow: 0px 7px 18px -2px rgba(0,0,0,0.4); box-shadow: 0px 7px 18px -2px rgba(0,0,0,0.4); transition:1s; ");
            $(".menu li a").attr("style", "color:black;");
            $("#logo h1 a").attr("style", "color:black;");

        } else {

            $("header").attr("style", "position: absolute; color:black !important; width: 100%; padding: 15px 0; z-index: 9; transition:1s;");
            $(".menu li a").attr("style", "color:white;");
            $("#logo h1 a").attr("style", "color:white;");

        }
    });


    $(document).on("click", ".login", function () {

        $('#create').modal({
            show: true
        });

        $.ajax({
            type: 'get',
            headers: {
                Accept: "application/json"
            },
            url: "/login",
            success: function (data) {

            }
        });

    });


    // CODIGO SECCION DE PRODUCTOS

    $(document).on("keyup", "#inputProducto", function () {

        var valor = $(this).val();

        $.ajax({
            type: 'post',
            url: '/productos/search/' + valor,
            success: function (data) {
                console.log(data);
                document.getElementById("resultadoProducto").innerHTML = data;
            }
        });

    });


    // ============================ CODIGO SECCION CATEGORIA DEL PRODUCTO ================================= //

    $(document).on("keyup", "#inputCategoria", function () {

        var valor = $(this).val();

        $.ajax({
            type: 'get',
            url: '/categoria/search/' + valor,
            success: function (data) {
                console.log(data);
                document.getElementById("resultadoCategoria").innerHTML = data;
            }
        });

    });



    //Ver adicionales productos
    $(document).on("click", ".adicionales", function () {

        var id = $(this).attr("data-id");
        var comentarios = $(this).attr("data-comentarios");

        $("#comentarioProductoModal").attr("data-producto", id);
        //alert(comentarios);

        $.ajax({
            url: '/verAdcionales/' + id,
            type: 'get',
            success: function (data) {
                console.log(comentarios);

                $("#adicionalesProductos").html(data);

            }
        });

    });




    //CÓDIGO SECCIÓN DE CREACION TIENDA/*


    $(document).on("click", "#ver_pedido", function () {

        var valor = $(this).attr("data-id");

        $.ajax({
            type: 'get',
            url: '/carrito/' + valor,
            success: function (data) {
                console.log(data);
                document.getElementById("resultadoCategoria").innerHTML = data;
            }
        });

    })


    $(document).on("change", "#imgTienda", function () {

        var fileInput = document.getElementById('imgTienda');
        var filePath = fileInput.value;
        var allowedExtensions = /(.jpg|.jpeg|.png|.gif)$/i;
        if (!allowedExtensions.exec(filePath)) {
            alert('Por favor sube un arcivo de imagen (.jpeg/.jpg/.png/.gif)');
            fileInput.value = '';
            return false;
        } else {
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById('imagePreview').innerHTML = '<code>Imagen seleccionada: </code> <br><img src="' + e.target.result + '"/ width=300px>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        }


    });

    $(document).on("keyup", "#nombreTienda", function () {
        valor = document.getElementById("nombreTienda").value;
        if (valor == null || valor.length == 0 || /^\s+$/.test(valor)) {
            $(".nombreTiendaCode").attr("style", "display:block;");
            return false;
        } else {
            $(".nombreTiendaCode").attr("style", "display:none;");
            return true;

        }
    });

    $(document).on("keyup", "#descripcionTienda", function () {
        valor = document.getElementById("descripcionTienda").value;
        if (valor == null || valor.length == 0 || /^\s+$/.test(valor)) {
            $(".descripcionTiendaCode").attr("style", "display:block;");
            return false;
        } else {
            $(".descripcionTiendaCode").attr("style", "display:none;");
            return true;

        }
    });

    $(document).on("keyup", "#direccionTienda", function () {
        valor = document.getElementById("direccionTienda").value;
        if (valor == null || valor.length == 0 || /^\s+$/.test(valor)) {
            $(".direccionTiendaCode").attr("style", "display:block;");
            return false;
        } else {
            $(".direccionTiendaCode").attr("style", "display:none;");
            return true;
        }
    });


    $(document).on("click", ".registroTienda", function () {


        var fileInput = document.getElementById('imgTienda');
        var filePath = fileInput.value;
        var allowedExtensions = /(.jpg|.jpeg|.png|.gif)$/i;
        if (!allowedExtensions.exec(filePath)) {
            alert('Por favor sube un arcivo de imagen (.jpeg/.jpg/.png/.gif)');
            fileInput.value = '';
            return false;
        } else {
            //Image preview
            if (fileInput.files && fileInput.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    document.getElementById('imagePreview').innerHTML = '<code>Imagen seleccionada: </code> <br><img src="' + e.target.result + '"/ width=300px>';
                };
                reader.readAsDataURL(fileInput.files[0]);
            }
        }



        valor = document.getElementById("nombreTienda").value;
        if (valor == null || valor.length == 0 || /^\s+$/.test(valor)) {
            $(".nombreTiendaCode").attr("style", "display:block;");
            $('html, body').animate({scrollTop: 150}, 'slow');
            return false;
        } else {
            $(".descripcionTiendaCode").attr("style", "display:none;");
        }

        valor = document.getElementById("descripcionTienda").value;
        if (valor == null || valor.length == 0 || /^\s+$/.test(valor)) {
            $(".descripcionTiendaCode").attr("style", "display:block;");
            $('html, body').animate({scrollTop: 150}, 'slow');
            return false;
        } else {
            $(".descripcionTiendaCode").attr("style", "display:none;");
        }


        valor = document.getElementById("direccionTienda").value;
        if (valor == null || valor.length == 0 || /^\s+$/.test(valor)) {
            $(".direccionTiendaCode").attr("style", "display:block;");
            $('html, body').animate({scrollTop: 150}, 'slow');
            return false;
        } else {
            $(".direccionTiendaCode").attr("style", "display:none;");
        }


        valorDepto = $("#departamento").val();
        if (valorDepto == 0 || valorDepto.length == 0 || /^\s+$/.test(valorDepto)) {
            $(".departamentoCode").attr("style", "display:block;");
            $('html, body').animate({scrollTop: 150}, 'slow');
            return false;
        } else {
            $(".departamentoCode").attr("style", "display:none;");
        }


        valorDepto = $("#ciudad").val();
        if (valorDepto == 0 || valorDepto.length == 0 || /^\s+$/.test(valorDepto)) {
            $(".ciudadTiendaCode").attr("style", "display:block;");
            $('html, body').animate({scrollTop: 150}, 'slow');
            return false;
        } else {
            $(".ciudadTiendaCode").attr("style", "display:none;");
        }


        valorMovil = $("#telMovil").val();
        if (valorMovil == 0 || valorMovil.length == 0 || /^\s+$/.test(valorMovil)) {
            $(".movilTiendaCode").attr("style", "display:block;");
            $('html, body').animate({scrollTop: 150}, 'slow');
            return false;
        } else {
            $(".movilTiendaCode").attr("style", "display:none;");
        }

    });


    $(document).on("click", ".edicionCategoria", function () {
        var valor = $(this).attr("data-valor");
        var url = $(this).attr("data-url");

        //alert(valor);

        $('#detalleCategoriaModal').modal({
            show: true
        });

        $.ajax({
            type: 'post',
            url: url + '/' + valor,
            success: function (data) {
                console.log(data);
                document.getElementById("contenidoModalDetalle").innerHTML = data;
            }
        });

    });


    $(document).on("click", ".eliminarCategoria", function () {
        var valor = $(this).attr("data-valor");
        var url = $(this).attr("data-url");

        $.ajax({
            type: 'delete',
            url: url + '/' + valor,
            success: function (data) {
                $(".edicionCategoria").attr("disabled", "disabled");
                $(".eliminarCategoria").attr("disabled", "disabled");

                $(".eliminacion").attr("style", "position:fixed; top:40px; right:40px; display:block; transition:2s");

                setTimeout(actualizar, 2000);

            }
        });

    });




    $(document).on("click", ".verUsuario", function () {

        $('#detalleUsuarioModal').modal({
            show: true
        });

        var url = $(this).attr("data-url");
        var id = $(this).attr("data-id");

        $.ajax({
            type: "get",
            url: url + '/' + id,
            success: function (data) {
                console.log(data);
                document.getElementById("contenidoModalDetalleUsuairo").innerHTML = data;
            }
        });


    });



    $(document).on("click", ".carritoOpcion", function () {
        /*$(".carroCompraWhat").attr("style","height:100%; transition:2s;");*/

        $(".carroCompra").attr("style", "transition:1s; transform: translateX(-200%);");

        $("#footer-grids").fadeIn("slow");
        $(".carroCompraWhat").fadeOut("slow");
    });

    $(document).on("click", ".verPedido", function () {
        /*$(".carroCompraWhat").attr("style","height:100%; transition:2s;");*/

        $(".carroCompra").attr("style", "transition:1s; transform: translateX(-200%);");

        $("#footer-grids").fadeOut("slow");
        $(".carroCompraWhat").fadeIn("slow");
    });-


    $(document).on("click", ".regresarPedidoWeb", function () {
        /*$(".carroCompraWhat").attr("style","height:100%; transition:2s;");*/

        $("#footer-grids").fadeIn("slow");
        $(".carroCompraWhat").fadeOut("slow");
    });


    $(document).on("click", ".cancelarPedido", function () {
        /*$(".carroCompraWhat").attr("style","height:100%; transition:2s;");*/
        $(".carroCompra").attr("style", "transition:1s; transform: translateX(-200%);");
        var url = $(this).attr("data-url");


        $.get({
            type: "get",
            url: url,
            succes: function () {
            	
            }
        })

        recargar();

        var cantidad = $(".detectarCantidadDelete").attr("data-cantidad");
        
        if(cantidad >= 1){
        	$(".detectarCantidadDelete").attr("data-cantidad","0");
        }

         document.getElementById("descriptionModal").innerHTML = "";
         document.getElementById("valueModal").innerHTML = "";
         document.getElementById("adicionalesProductos").innerHTML = "";
         document.getElementById("cantidadProduct").innerHTML = "Sin unidad";
                  document.getElementById("valorPedido").innerHTML = "Carrito vacio";



        
    });


    $(document).on("click", ".verProducto", function () {

        $('#detalleProductoModal').modal({
            show: true
        });

        var url = $(this).attr("data-url");
        var id = $(this).attr("data-id");

        $.ajax({
            type: "get",
            url: url + '/' + id,
            success: function (data) {
                console.log(data);
                document.getElementById("contenidoModalDetalleProducto").innerHTML = data;
            }
        });


    });

    $(document).on("click", ".edicionProducto", function () {

        $('#detalleProductoModal').modal({
            show: true
        });

        var url = $(this).attr("data-url");
        var id = $(this).attr("data-id");

        $.ajax({
            type: "get",
            url: url + '/' + id,
            success: function (data) {
                console.log(data);
                document.getElementById("contenidoModalDetalleProducto").innerHTML = data;
            }
        });


    });


    $(document).on("click", ".categoriaBusqueda", function () {

        var url = "/categoria/busqueda/asignacion";
        var id = $(this).attr("data-id");

        $.ajax({
            type: "get",
            url: url + '/' + id,
            success: function (data) {
                console.log(data);
                document.getElementById("resultadoCategoria").innerHTML = data;
            }
        });

    });


    /* BUSCADOR NOMBRE UNICO DE LA TIENDA*/


    $(document).on("keyup", "#nombreTiendaUnico", function () {

        var valor = $(this).val();
        var url = "/tienda/searchUnique/";


        var string = valor.replace(/ /g, "-");

        $(this).val(string);

        console.log(string);

        $.ajax({
            type: 'post',
            url: url + string,
            success: function (data) {

                if (data == "1") {

                    document.getElementById("nombreTiendaUnicoDiv").innerHTML = "<div class='alert alert-danger' style='margin-top:10px;'>El nombre de la tienda esta registrado, intenta con otro</div>";

                    document.getElementById("nombreTienda").disabled = true;
                    document.getElementById("descripcionTienda").disabled = true;
                    document.getElementById("direccionTienda").disabled = true;
                    document.getElementById("horarioTienda").disabled = true;
                    document.getElementById("departamento").disabled = true;
                    document.getElementById("ciudad").disabled = true;
                    document.getElementById("representante").disabled = true;
                    document.getElementById("telMovil").disabled = true;
                    document.getElementById("telFijo").disabled = true;
                    document.getElementById("linkFb").disabled = true;
                    document.getElementById("linkIns").disabled = true;
                    document.getElementById("linkTwitter").disabled = true;


                    document.getElementById("registroTienda").disabled = true;


                } else {

                    document.getElementById("nombreTiendaUnicoDiv").innerHTML = "<div class='alert alert-success' style='margin-top:10px;'>¡El nombre de la tienda se encuentra disponible!</div>";

                    document.getElementById("nombreTienda").disabled = false;
                    document.getElementById("descripcionTienda").disabled = false;
                    document.getElementById("direccionTienda").disabled = false;
                    document.getElementById("horarioTienda").disabled = false;
                    document.getElementById("departamento").disabled = false;
                    document.getElementById("ciudad").disabled = false;
                    document.getElementById("representante").disabled = false;
                    document.getElementById("telMovil").disabled = false;
                    document.getElementById("telFijo").disabled = false;
                    document.getElementById("linkFb").disabled = false;
                    document.getElementById("linkIns").disabled = false;
                    document.getElementById("linkTwitter").disabled = false;

                    document.getElementById("registroTienda").disabled = false;


                }
                console.log(data);


            }
        });

    });

    var sum = 1;
    var valor = 0;

    $(document).on("click", ".adicionalInput", function () {

        if( $(this).prop('checked') ) {

            var id_adicional = $(this).attr('value');
            var id_producto = $(this).attr('data-producto');
            var nombre_adicional = $(this).attr('data-nombre');
            var valor_adicional = $(this).attr('data-valor');

            
            $.ajax({
                url: '/agregarAdicional/' + id_adicional + '/' + id_producto + '/' + nombre_adicional + '/' + valor_adicional,
                type: 'get',
                success: function (data) {
                    
                    $('#adicionalesTotalesDiv').html(data);
                    recargar();
                }
            });

        }else{

            var id_adicional = $(this).attr('value');
            var id_producto = $(this).attr('data-producto');
            var nombre_adicional = $(this).attr('data-nombre');
            var valor_adicional = $(this).attr('data-valor');

            
            $.ajax({
                url: '/eliminarAdicional/' + id_adicional + '/' + id_producto + '/' + nombre_adicional + '/' + valor_adicional,
                type: 'get',
                success: function (data) {
                    console.log(data);
                }
            });

            recargar();

        }

    });


    $(document).on("click", ".iconAddPro", function () {
    	$("#adicionalesProductos").attr("style","display:block;");


        var id = $(this).attr("data-id");

        var producto = "#producto" + $(this).attr("data-id");

        $.ajax({
            url: '/addProducto/' + id,
            type: 'get',
            success: function (data) {
                console.log(data);
                if (data[1] > 1) {
                    $(producto).attr("data-cantidad", data[1]);
                    $("#cantidadProduct").html(data[1] + " unidades");
                } else {
                    $(producto).attr("data-cantidad", data[1]);
                    $("#cantidadProduct").html(data[1] + " unidad");
                }
                recargar();
            }
        });

    });



    $(document).on("click", ".iconAddProCarrito", function () {
        var id = $(this).attr("data-id");
        //alert(id);
        $("#adicionalesProductos").attr("style","display:block;");
        $.ajax({
            url: '/addProducto/' + id,
            type: 'get',
            success: function (data) {
                $("#cantidadProduct").html(data);
            }
        });

    });


    $(document).on("click", ".iconDelPro", function () {
        var id = $(this).attr("data-id");
        //alert(id);
        


        $.ajax({
            url: '/delProducto/' + id,
            type: 'get',
            success: function (data) {

                if (data === "") {
                    $("#cantidadProduct").html(0);
                    $("#producto" + id).attr("data-cantidad", "0");
                } else {

                    if (data > 1) {
                        $("#producto" + id).attr("data-cantidad", data);
                        $("#cantidadProduct").html(data + " unidades");
                    } else {
                        $("#producto" + id).attr("data-cantidad", data);
                        $("#cantidadProduct").html(data + " unidad");
                    }
                }
                recargar();
            }


        });

    });

    $(document).on("click", ".iconosProMenos", function () {
        var id = $(this).attr("data-id");
        //alert(id);

        $.ajax({
            url: '/delProducto/' + id,
            type: 'get',
            success: function (data) {
                window.location.reload();
            }
        });

    });


    $(document).on("click", ".btnEliminarProducto", function () {

        var id = $(this).attr("data-id");
        //alert(id);
        $.ajax({
            url: '/deleteProducto/' + id,
            type: 'get',
            success: function (data) {
                console.log(data);
            }
        });

    });

    $(document).on("click", ".identificador_categoria", function () {
        var id = $(this).attr("data-id");
        var id_tienda = $("#id_tienda").attr("data-id");

        //alert(id);
        $.ajax({
            url: '/consulta-tienda/categoria/' + id + '/' + id_tienda,
            type: 'get',
            success: function (data) {
                //alert(data);
                $("#resultadoSQLProducto").html(data);

            }
        });

    });


    $(document).on("click", ".productoUnico", function () {

        var id = $(this).attr("data-id");
        var nombre = $(this).attr("data-name");
        var description = $(this).attr("data-description");
        var valor = $(this).attr("data-value");
        var cantidad = $(this).attr("data-cantidad");

        $("#iconDelPro").attr("data-id", id);

        if (cantidad == 0) {
        	$("#adicionalesProductos").attr("style","display:none;"); 
			valorCantidad = "Sin unidad";
        } else {

            if (cantidad == 1) {
                valorCantidad = cantidad + " unidad";
            } else {
                valorCantidad = cantidad + " unidades";
            }

        }


        $("#iconAddPro").attr("data-id", id);

        $("#modalProductos").modal("show");

        $("#nameModal").html(nombre);
        $("#descriptionModal").html(description);
        $("#valueModal").html(valor);
        $("#cantidadProduct").html(valorCantidad);
    });

    $(document).on("click", "#btn-opcion-domi", function () {
        var valor_domi = $("#valor_domicilio_option_2").attr("value");

        document.getElementById("table_op_domi").innerHTML = "$ " + valor_domi;
        $("#valor_domicilio").attr("value", valor_domi);

        $(this).attr("class", "btn btn-success float-left");
        $("#btn-opcion-local").attr("class", "btn btn-info float-left btn-domi");

    });

    $(document).on("click", "#btn-opcion-local", function () {
        $("#valor_domicilio").attr("value", "0");
        document.getElementById("table_op_domi").innerHTML = "<b>Ir al Local</b>";


        $(this).attr("class", "btn btn-success float-left");
        $("#btn-opcion-domi").attr("class", "btn btn-info float-left btn-domi");

    });


    /* FUNCIONES DE RECARGA */
    $(document).on("click", ".listadoPedido", function () {

        ///alert($(this).attr("data-click"));

        if ($(this).attr("data-click") == 0) {
            $(".carroCompra").attr("style", "transition:1s; transform: translateX(0px);");
            $(this).attr("data-click", "1");
        } else {
            $(".carroCompra").attr("style", "transition:1s; transform: translateX(-200%);");
            $(this).attr("data-click", "0");
        }

    });

    /*
     $(document).on("click", ".carritoOpcion", function(){
     //alert();
     if($(this).attr("data-click") == 0){
     $(".valorPedido").attr("style", "transition:1s; transform: translateY(0px);");
     $(this).attr("data-click", "1");
     }else{
     $(".valorPedido").attr("style", "transition:1s; transform: translateY(150%);");
     $(this).attr("data-click", "0");
     }
     
     });
     */
    function actualizar() {
        location.reload();
    }

});