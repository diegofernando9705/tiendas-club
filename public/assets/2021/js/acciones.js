


    $(window).on('beforeunload', function() {
       $.ajax({
            type: 'get',
            url: '/limpiar/carrito/tiendas',
            success: function (data) {
               $("#valor_cantidad").html("0");
            }
        });
        
    });


    $(document).ready(function () {
        $.ajax({
            type: 'get',
            url: '/limpiar/carrito/tiendas',
            success: function (data) {
               $("#cantidadCarrito").html('0');
            }
        });

        $('.mi-selector').select2();



    $(window).scroll(function () {
        if ($(window).scrollTop() > 80) {
            
            $("header").attr("style", "position:fixed; background-color:white; -webkit-box-shadow: 0px 7px 18px -2px rgba(0,0,0,0.4); -moz-box-shadow: 0px 7px 18px -2px rgba(0,0,0,0.4); box-shadow: 0px 7px 18px -2px rgba(0,0,0,0.4); transition:1s; ");
            $(".menu li a").attr("style", "color:black;");
            $("#logo h1 a").attr("style", "color:black;");

        } else {

            $("header").attr("style", "position: absolute; color:black !important; width: 100%; padding: 15px 0; z-index: 9; transition:1s;");
            $(".menu li a").attr("style", "color:white;");
            $("#logo h1 a").attr("style", "color:white;");

        }
    });

    //BUSCAR PRODUCTO EN LA TIENDA AL PUBLICO
    
    $(document).on("click", ".menuOpcion", function(){
        var code = $(this).attr("data-code");
        $("#modalInformacionTienda").modal("show");
        
        $.ajax({
            type: 'get',
            url: '/informacion_tienda/' + code,
            success: function (data) {
                document.getElementById("modalCarroTienda").innerHTML = data;
            }
        });

    });



    //BUSCAR PRODUCTO EN LA TIENDA AL PUBLICO
    $(document).on("keyup", "#busquedaProducto", function(){
    	var pedido = $(this).val();
    	var tienda = $(this).attr("data-tienda");


		$.ajax({
            type: 'get',
            url: '/productos/search/web/' + pedido + "/" + tienda,
            success: function (data) {
                document.getElementById("resultadoSQLProducto").innerHTML = data;
			}
        });

    });


    // CODIGO SECCION DE PRODUCTOS

    $(document).on("keyup", "#inputProducto", function () {

        var valor = $(this).val();

        $.ajax({
            type: 'post',
            url: '/productos/search/' + valor,
            success: function (data) {
                
                document.getElementById("resultadoProducto").innerHTML = data;
            }
        });

    });


    // ============================ CODIGO SECCION CATEGORIA DEL PRODUCTO ================================= //

    $(document).on("keyup", "#busquedaProducto", function () {

        var valor = $(this).val();
        var codigo_tienda = $(this).attr('data-tienda');

        $.ajax({
            type: 'get',
            url: '/search/' + valor + '/' + codigo_tienda,
            success: function (data) {
                
                document.getElementById("resultadoSQLProducto").innerHTML = data;
                
            }
        });

    });

    /* MODAL BUZON SUGERENCIA */
       $(document).on("click", "#abrirFormularioSugerencia", function () {
        $("#modalFormularioSugerencia").modal("show");
        
        var codigo_tienda = $("#id_tienda").val();

        $.ajax({
            type: 'get',
            url: '/sugerencia/sugerencia/'+codigo_tienda,
            success: function (data) {
                $("#modalFormularioBodySugerencia").html(data);
            }
        });
    });

    //CÓDIGO SECCIÓN DE CREACION TIENDA/*


    $(document).on("click", ".carritoOpcion", function () {
        /*$(".carroCompraWhat").attr("style","height:100%; transition:2s;");*/

        $(".carroCompra").attr("style", "transition:1s; transform: translateX(-200%);");

        $("#footer-grids").fadeIn("slow");
        $(".carroCompraWhat").fadeOut("slow");
    });


    $(document).on("click", ".barra_opcion", function () {
        $("#modalInformacionCarrito").modal("show");
        
        var codigo_tienda = $("#id_tienda").val();

        $.ajax({
            type: 'get',
            url: '/carrito/' + codigo_tienda,
            success: function (data) {
                
                $("#modalCarroCompra").html(data);
            }
        });
    });


    $(document).on("click", ".btn-local", function () {
        $(this).attr("style","color: #FBFFFE !important; background-color: #FCA311 !important");
        $(".btn-domicilio").attr("style","color: #FBFFFE !important; background-color: #96031A !important");
        $("#btn-envio-pedido").attr("data-op","Whatsapp-local");

        var opcion = $(this).val();

        console.log(opcion);

        if(opcion == 'domicilio'){
            $("#btn-envio-pedido").attr("data-op","Whatsapp-domicilio");
            document.getElementById("local").checked = false;
        }else{
            $("#btn-envio-pedido").attr("data-op","Whatsapp-local");
            document.getElementById("domi").checked = false;
        }

        var codigo_tienda = $("#id_tienda").val();
        
        $.ajax({
            type: 'get',
            url: '/carritoOpcion/' + opcion + '/' + codigo_tienda,
            success: function (data) {

                $(".valor_precio").html("$"+data[1]);
                $(".valor_total_carrito").html("$"+data[0]);     
            }
        });
    });



    $(document).on("click", ".btn-domicilio", function () {
        
        $(this).attr("style","color: #FBFFFE !important; background-color: #FCA311 !important");
        $(".btn-local").attr("style","color: #FBFFFE !important; background-color: #96031A !important");
        $("#btn-envio-pedido").attr("data-op","Whatsapp-domicilio");


        var opcion = $(this).attr("data-op");
        var codigo_tienda = $("#id_tienda").val();
        
        $.ajax({
            type: 'get',
            url: '/carritoOpcion/' + opcion + '/' + codigo_tienda,
            success: function (data) {
                $("#contenedor_valores").html(data);     
            }
        });
    });

    $(document).on("click", ".btn-envio-pedido", function () {
    
        var opcion = $(this).attr("data-op");
        var codigo_tienda = $("#id_tienda").val();
        
         $.ajax({
            type: 'get',
            url: '/formulario/' + opcion + '/' + codigo_tienda,
            success: function (data) {

                $("#modalInformacionCarrito").modal("hide");
                $("#modalFormulario").modal("show");
                $("#modalFormularioBody").html(data);
            }
        });
    });

    $(document).on("click", ".envio-formulario", function () {
        
        $opcion = $(this).attr("data-op");

        if($opcion == "whatsapp"){
            
            var formData = $("#formularioWhatsapp").serialize();
            

            if($("#nombre").val().trim().length > 0){
                $(".nombreDomicilio").attr("style","display:none;");
                $nombreDomi = 1;
            }else{
                $(".nombreDomicilio").attr("style","display:block;");
            }

            
            if($("#telefono").val().trim().length > 0){
                $(".telefonoDomicilio").attr("style","display:none;");
                
                var celular = $("#telefono").val();

                var expresion = /^3[\d]{9}$/;
                
                if (isNaN(celular) || !expresion.test(celular)){
                    alert("Debe ingresar un número válido.");
                }else{
                    $telefono = 1;
                    var celular_cliente = $("#telefono").val();
                }

            }else{
                $(".telefonoDomicilio").attr("style","display:block;");
            }
            

            if($("#direccion").val().trim().length > 0){
                $(".direccionDomicilio").attr("style","display:none;");
                $direccionDomi = 1;
            }else{
                $nombreDomi = 0;
                $(".direccionDomicilio").attr("style","display:block;");
            }

            
            if($("#barrio").val().trim().length > 0){
                $(".barrioDomicilio").attr("style","display:none;");
                $barrioDomi = 1;
            }else{
                $(".barrioDomicilio").attr("style","display:block;");
            }

            if($nombreDomi == 1 && $direccionDomi == 1 && $barrioDomi == 1 && $telefono == 1){

                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: "POST",
                    url: "/envioformulario",// ó {{url(/admin/empresa)}} depende a tu peticion se dirigira a el index(get) o tu store(post) de tu controlador 
                    data: formData,
                    success: function (data) {
                        console.log(data);
                        if(data == "local"){
                            
                        }else{
                            window.onpageshow = function() {};
                            window.location = data;
                            setTimeout(function(){
                                location.reload(true);  
                            },1000);
                        }

                    }
                });

            }

        }else if($opcion == "formulario-sugerencia"){
            
            var formularioData = $("#formularioSugerencia").serialize();

            if($("#nombreSugerencia").val().trim().length > 0){
                $(".nombreSugerencia").attr("style","display:none;");
                $nombreSugerencia = 1;
            }else{
                $(".nombreSugerencia").attr("style","display:block;");
            }

            
            if($("#telefonoSugerencia").val().trim().length > 0){
                $(".telefonoSugerencia").attr("style","display:none;");
                $telefonoSugerencia = 1;
            }else{
                $(".telefonoSugerencia").attr("style","display:block;");
            }

            
            

            if($nombreSugerencia == 1 && $telefonoSugerencia == 1){

                $.ajax({

                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: "POST",
                    url: "/envioformulario/sugerencia/",// ó {{url(/admin/empresa)}} depende a tu peticion se dirigira a el index(get) o tu store(post) de tu controlador 
                    data: formularioData,
                    success: function (data) {
                        if(data == true){
                            alert('Su mensaje ha sido enviado');
                        }
                         window.onpageshow = function() {};
                                location.reload(true);  
                          
                    }
                });
            }

        }else{
            
            if($("#nombre").val().trim().length > 0){
                $(".nombreLocal").attr("style","display:none;");
                $nombre = 1;
            }else{
                $(".nombreLocal").attr("style","display:block;");
            }

            
            if($("#telefono").val().trim().length > 0){
                $(".telefonoLocal").attr("style","display:none;");
                
                var celular = $("#telefono").val();

                var expresion = /^3[\d]{9}$/;
                
                if (isNaN(celular) || !expresion.test(celular)){
                    alert("Debe ingresar un número válido.");
                }else{
                    $telefono = 1;
                    var celular_cliente = $("#telefono").val();
                }

            }else{
                $(".telefonoLocal").attr("style","display:block;");
            }

            
            if($("#hora_llegada").val().trim().length > 0){
                $(".telefonoLocal").attr("style","display:none;");
                $hora = 1;
            }else{
                $(".horaLocal").attr("style","display:block;");
            }

            if($nombre == 1 && $telefono == 1 && $hora == 1 && $telefono == 1){

                var formData = $("#formularioLocal").serialize();

                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: "POST",
                    url: "/envioformulario",// ó {{url(/admin/empresa)}} depende a tu peticion se dirigira a el index(get) o tu store(post) de tu controlador 
                    data: formData,
                    success: function (data) {
                        window.onpageshow = function() {};
                        window.location = data;
                        setTimeout( function(){
                            location.reload(true);  
                        },1000); 

                    }
                });

            }
        
        }

    });

    $(document).on("click", ".opcion-eliminar", function () {
        
        var id = $(this).attr("data-id");
        var codigo_tienda = $("#id_tienda").val();
        
        $.ajax({
            type: 'get',
            url: '/eliminar/' + id + '/' + codigo_tienda,
            success: function (data) {
                $.ajax({
                    url: '/capturar/total_carrito',
                    type: 'get',
                    success: function (data) {
                        
                        if(data[1] != "0"){
                            $("#valorPedido").html("$ "+parseInt(data[1]));
                        }else{
                            $("#valorPedido").html("CARRITO VACIO");
                        }
                    }
                });
                $("#valorPedido").html($("#total_pedido_carrito").val());
                $("#modalCarroCompra").html(data);
                $("#cantidadCarrito").load(" #cantidadCarrito");
                $("#carroCompra").load(" #carroCompra");
            }
        });
    });

    $(document).on("click", ".btn-vovler-carrito", function () {

        $("#modalFormulario").modal("hide");
        $("#modalInformacionProceso").modal("hide");
        $("#pantalla_cubierta").attr("style", "display:none;");

        $("#modalInformacionCarrito").modal("show");

        var codigo_tienda = $("#id_tienda").val();

        $.ajax({
            type: 'get',
            url: '/carrito/' + codigo_tienda,
            success: function (data) {
                $("#modalCarroCompra").html(data);
            }
        });
    })


    $(document).on("click", ".btn-abrir-carrito", function () {

        $("#modalFormulario").modal("hide");
        $("#modalInformacionProceso").modal("hide");
        $("#pantalla_cubierta").attr("style", "display:none;");

        $("#modalInformacionCarrito").modal("show");

        var codigo_tienda = $("#id_tienda").val();

        $.ajax({
            type: 'get',
            url: '/carrito/' + codigo_tienda,
            success: function (data) {
                $("#modalCarroCompra").html(data);
            }
        });
    })

    
    $(document).on("click", ".envioPedido", function(){
        
        var opcion = "Whatsapp-domicilio";
        var codigo_tienda = $("#id_tienda").val();

         $.ajax({
            type: 'get',
            url: '/formulario/' + opcion + '/' + codigo_tienda,
            success: function (data) {
                $("#modalInformacionCarrito").modal("hide");
                $("#modalFormulario").modal("show");
                $("#modalFormularioBody").html(data);
            }
        });
    })

    $(document).on("click", ".regresarPedidoWeb", function () {
        /*$(".carroCompraWhat").attr("style","height:100%; transition:2s;");*/

        $("#footer-grids").fadeIn("slow");
        $(".carroCompraWhat").fadeOut("slow");
    });



      /* ABRIR MODAL PARA AGREGAR CANTIDAD Y ADICIONALES */

    $(document).on("click", ".productoUnico", function () {
        var id = $(this).attr("data-id");
        $(".checked-adicionales").prop('checked', false); 
        $(".adicionales").html("");
        

        document.getElementById("iconAddPro").disabled = false;
        document.getElementById("comentarios_producto").value = "";

        $.get({
            url: '/productoCompra/' + id,
            type: 'get',
            success: function (data) {
                console.log(data);
                $(".ver-fotografia").attr("data-img", "https://clientes.tiendas.club/storage/"+data[1][0]['imagen_producto']+"");
                $(".imagen_modal_completa").attr("style","background-image:url('https://clientes.tiendas.club/storage/"+data[1][0]['imagen_producto']+"'); background-size:cover; transform:translateY(0%); transition:1s;");
                $(".informacion_modal_completa").attr("style","background-color: #D5D5D5; transform:translateY(0%); transition:1s; ");
                $("#categoria_producto").html("<p><span class='span_nombre_categoria'>"+data[2][0]['nombre_categoria']+"</span></p>");
                $(".boton_agregar").attr("style", "transform: translateY(0); transition:1s;");
                
                $("#codigo_producto").attr("value", data[1][0]['id']);

                $("#informacion_producto_formulario .nombre").html(data[1][0]['name']);
                $("#informacion_producto_formulario .precio").html("$"+data[1][0]['valor']);
                $("#informacion_producto_formulario .descripcion").html(data[1][0]['description']);

                    $.each(data[0], function (valor, indice, array) {
                        $(".adicionales").attr("style", "display:block;");
                        $(".adicionales").append("<div class='adicional_unico' id='adicional_unico'><input type='checkbox' class='checked-adicionales' name='adicionales[]' id='"+indice['id']+"' value='"+indice['id_adicional']+"' /><label for='"+indice['id']+"'>"+indice['nombre_adicional']+"</label></div>")
                    });

            }
        })
    });

    $(document).on("click", ".ver-fotografia", function () {
        $("#modalInformacionFotoProducto").modal("show");
        var imagen = $(this).attr("data-img");
        $(".fade").attr("style", "z-index: 999999999 !important; transition: opacity 0.15s linear;");
        $("#modalInformacionFotoProducto").attr("style", "z-index:99999999999999 !important");
        
        $("#bodyInformacionFotoproducto").html("<img src='"+imagen+"' width='100%'>");

    });

    $(document).on("click", ".btnAtras_modal", function () {
        $("#modalInformacionFotoProducto").modal("hide");

        $(".imagen_modal_completa").attr("style","background-size:cover; background-blend-mode: color-burn; background-color: rgba(28,30,62,0.50) !important; transform:translateY(-100%); transition:1s;");
        $(".informacion_modal_completa").attr("style","background-color:white; transform:translateY(100%); transition:1s;");
        $(".boton_agregar").attr("style", "transform: translateY(100vh) !important; transition:1s;");
        
    });


    $(document).on("click","#iconAddPro", function(){

        $(this).attr("disabled","disabled");

        var formData = $("#formularioPedido").serialize();
        console.log(formData);
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            type: "POST",
            url: "/productoAgregado",// ó {{url(/admin/empresa)}} depende a tu peticion se dirigira a el index(get) o tu store(post) de tu controlador 
            data: formData,
            success: function (data) {

                if(data[1] == true){
                    $.ajax({
                        url: '/capturar/total_carrito',
                        type: 'get',
                        success: function (data) {
                            $("#valorPedido").html("$   "+parseInt(data[1]));
                        }
                    });

                    $("#alerta-exito").attr("style","transform: translateY(0%); transition:0.5s;")

                   setTimeout( function(){
                        $("#alerta-exito").attr("style","transform: translateY(-300%); transition:1s;");
                        $("#modalInformacionProducto").modal("hide");  
                        $(".imagen_modal_completa").attr("style","background-size:cover; background-blend-mode: color-burn; background-color: rgba(28,30,62,0.50) !important; transform:translateY(-100%); transition:1s;");
                        $(".informacion_modal_completa").attr("style","background-color:white; transform:translateY(100%); transition:1s;");
                         $(".boton_agregar").attr("style", "transform: translateY(100vh) !important; transition:1s;");
                   },1500);

                }

                $("#valorPedido").html("$" + parseInt(data[2]));
                $("#cantidadCarrito").load(" #cantidadCarrito");
                $("#row_opciones").load(" #row_opciones");

                $("#carroCompra").load(" #carroCompra");

            }
        });
    });

    /* ---------------------------------------------------- */


    $(document).on("click", ".btn-cerrar-modal", function () {
        $(".titleAgregarBtn").attr("style","display:block;");

        $(".titleAgregarBtn").html("<l class='titleAgregarBtn' style='color:white;''>Agregar producto</l><svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:svgjs='http://svgjs.com/svgjs' version='1.1' width='20' height='20' x='0' y='0' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512' xml:space='preserve' class=''><g><g xmlns='http://www.w3.org/2000/svg' ><g><path d='M256,11C120.9,11,11,120.9,11,256s109.9,245,245,245s245-109.9,245-245S391.1,11,256,11z M256,460.2    c-112.6,0-204.2-91.6-204.2-204.2S143.4,51.8,256,51.8S460.2,143.4,460.2,256S368.6,460.2,256,460.2z' fill='#ffffff' data-original='#000000' style='' class=''/><path d='m357.6,235.6h-81.2v-81.2c0-11.3-9.1-20.4-20.4-20.4-11.3,0-20.4,9.1-20.4,20.4v81.2h-81.2c-11.3,0-20.4,9.1-20.4,20.4s9.1,20.4 20.4,20.4h81.2v81.2c0,11.3 9.1,20.4 20.4,20.4 11.3,0 20.4-9.1 20.4-20.4v-81.2h81.2c11.3,0 20.4-9.1 20.4-20.4s-9.1-20.4-20.4-20.4z' fill='#ffffff' data-original='#000000' style='' class=''/></g></g></g></svg>");
        $(".titleAgregarBtn").attr("style","background-color: #1B1B1E !important; color: white !important;");
        /*$(this).attr("class","btn iconAddPro titleAgregarBtn");

        $(this).attr("data-dismiss","");
        $(this).attr("aria-label","");*/
    });


    $(document).on("click", ".identificador_categoria", function () {
        
        

        var id_cateoria = $(this).attr("data-valor");
        var codigo_tienda = $("#id_tienda").val();
      
        $.ajax({
            url: '/search/categoria/' + id_cateoria + '/' + codigo_tienda,
            type: 'get',
            success: function (data) {
                document.getElementById("resultadoSQLProducto").innerHTML = data;
                /* funciones carrusel cuando buscan por categoria */
                    var divs = document.getElementsByClassName("resultado_consulta_producto").length;
                    console.log("Hay " + divs + " elementos");
                    
                    var medida = 300*divs+30;
                    var medida_identificador_categoria = medida/divs-7;
                    console.log(medida);

                    $(".row_resultado_search_categoria").attr("style", "margin-left: 0px; width:"+medida+"px !important;");

                /* funciones carrusel cuando buscan por categoria */
            }
        });
    });

    /* OPCIONES DOMICILIO E IR AL LOCAL*/


    /* FUNCIONES DE RECARGA */

        /* FUNCIONES DE RECARGA */


    /* FUNCIONES DE RECARGA */
    $(document).on("click", "#cerrarVentana", function () {
        $(".carroCompra").attr("style", "transition:1s; transform: translateX(-200%);");
        $(".listadoPedido").attr("id","");
        $(this).attr("data-click", "0");
    });


    $("html").click(function (e) { if (e.target == document.getElementById("barraLateralCarroCompra"))  $(".carroCompra").attr("style", "transition:1s; transform: translateX(0px);"); else $(".carroCompra").attr("style", "transition:1s; transform: translateX(-200%);"); }); 
    $("html").click(function (e) { if (e.target == document.getElementById("barraLateralCarroCompra"))   $(".listadoPedido").attr("data-click", "1"); else $(".listadoPedido").attr("data-click", "0"); }); 

    $(document).on("click",".info_img_producto", function() {
        $("#modalImagenProductos").modal("show");
        var img = $(this).attr("src");
        $("#contenidoImagenModal").attr("src",img);
    });


    /* WOMPI */

    $(document).on("click", ".envio_formulario_api", function(){

        var metodo = $(this).attr("data-metodo");

        if($('#terminos_condiciones_wompi').prop('checked')){
           

            if($(this).attr("data-funcion") == "capturar-informacion"){
                console.log('CAPTURAR INFORMACION');



                if(metodo == "domicilio"){

                    $("#div_correo").attr("style", "display:block;");

                    if($("#nombre").val().trim().length > 0){
                        $(".nombreDomicilio").attr("style","display:none;");
                        $nombre_wompi = 1;
                    }else{
                        $(".nombreDomicilio").attr("style","display:block;");
                    }

                    if($("#telefono").val().trim().length > 0){
                        $(".telefonoDomicilio").attr("style","display:none;");
                        var celular = $("#telefono").val();

                        var expresion = /^3[\d]{9}$/;
                        
                        if (isNaN(celular) || !expresion.test(celular)){
                            alert("Debe ingresar un número celular válido");
                        }else{
                            $telefono_wompi = 1;
                        }

                    }else{
                        $(".telefonoDomicilio").attr("style","display:block;");
                    }
                    

                    if($("#direccion").val().trim().length > 0){
                        $(".direccionDomicilio").attr("style","display:none;");
                        $direccion_wompi = 1;
                    }else{
                        $(".direccionDomicilio").attr("style","display:block;");
                    }


                    if($("#barrio").val().trim().length > 0){
                        $(".barrioDomicilio").attr("style","display:none;");
                        $barrio_wompi = 1;
                    }else{
                        $(".barrioDomicilio").attr("style","display:block;");
                    }
                    

                    if($("#email_wompi").val().trim().length > 0){
                        
                        console.log('HAY EMAIL WOMPI');

                        $(".email_wompi").attr("style","display:none;");
                        var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

                        var email = $("#email_wompi").val();
                        
                        if (emailRegex.test(email)) {
                            console.log('HAY EMAIL WOMPI Y VALIDADO');

                            $email_wompi = 1;
                        } else {
                            alert("La dirección de email es incorrecta.");
                        }

                    }else{
                        $(".email_wompi").attr("style","display:block;");
                    }


                    if($("#method_pay").val().trim().length > 0){
                        $method_pay = 1;
                    }else{
                        alert('Seleccione un Método de pago');
                    }


                    if($nombre_wompi == 1 && $email_wompi == 1 && $telefono_wompi == 1 && $method_pay == 1){
                        
                        $(this).attr("data-funcion", "enviar-informacion");

                        var formData = $("#wompi_formulario").serialize();
                        
                        $.ajax({
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            type: "POST",
                            url: "/wompi/get/method_pay",
                            data: formData,
                            success: function (data) {
                                $("#div_metodo_pago").html(data);
                            }
                        });
                    }


                }else{

                    if($("#nombre_wompi").val().trim().length > 0){
                             $(".nombre_wompi").attr("style","display:none;");
                                $nombre_wompi = 1;
                    }else{
                        $(".nombre_wompi").attr("style","display:block;");
                    }

                    if($("#email_wompi").val().trim().length > 0){

                        $(".email_wompi").attr("style","display:none;");
                        var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;

                        var email = $("#email_wompi").val();
                        
                        if (emailRegex.test(email)) {
                            $email_wompi = 1;
                        } else {
                            alert("La dirección de email es incorrecta.");
                        }

                    }else{
                        $(".email_wompi").attr("style","display:block;");
                    }

                    if($("#telefono_wompi").val().trim().length > 0){

                        $(".telefono_wompi").attr("style","display:none;");
                        var celular = $("#telefono_wompi").val();

                        var expresion = /^3[\d]{9}$/;
                        
                        if (isNaN(celular) || !expresion.test(celular)){
                            alert("Debe ingresar un número celular válido");
                        }else{
                            $telefono_wompi = 1;
                        }
                        
                    }else{
                        $(".telefono_wompi").attr("style","display:block;");
                    }


                    if($("#method_pay").val().trim().length > 0){
                        $method_pay = 1;
                    }else{
                        alert('Seleccione un Método de pago');
                    }

                    if($nombre_wompi == 1 && $email_wompi == 1 && $telefono_wompi == 1 && $method_pay == 1){
                        
                        $(this).attr("data-funcion", "enviar-informacion");

                        var formData = $("#wompi_formulario").serialize();
                        
                        $.ajax({
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            type: "POST",
                            url: "/wompi/get/method_pay",
                            data: formData,
                            success: function (data) {
                                $("#div_metodo_pago").html(data);
                            }
                        });
                    }

                }

            }else if($(this).attr("data-funcion") == "enviar-informacion"){

                console.log('ENVIAR INFORMACION');
                
                if($("#method_pay").val() == "CARD"){
                    
                    console.log("Seleccinado tarjeta de credito");

                    var isCardValid = $.payform.validateCardNumber(cardNumber.val());
                    var isCvvValid = $.payform.validateCardCVC(CVV.val());

                    if(owner.val().length < 5){
                        alert("Nombre de propietario incorrecto");
                    } else if (!isCardValid) {
                        alert("Número de tarjeta incorrecto");
                    } else if (!isCvvValid) {
                        alert("CVV incorrecto");
                    } else {

                        $("#pantalla_cubierta").attr("style","display:block; position: fixed; top:0px; height: 100%; width: 100%; background: rgba(0,0,0,0.8); z-index: 99999999 !important;");
                        $("#modalInformacionProceso").modal("show");
                        $("#modalInformacionProcesoBody").html("Procesando...");


                        var formData = $("#wompi_formulario").serialize();
                        
                        $.ajax({
                            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                            type: "POST",
                            url: "/wompi/pay/success",
                            data: formData,
                            success: function (data) {
                                console.log('ENTRO AL SUCCESS DE WOMPI/PAY/SUCCESS');
                                console.log(data);
                                $("#pantalla_cubierta").attr("style","display:block; position: fixed; top:0px; height: 100%; width: 100%; background: rgba(0,0,0,0.8); z-index: 99999999 !important;");
                                $("#modalInformacionProceso").modal("show");
                                $("#modalInformacionProcesoBody").html("Procesando tarjeta de credito...");

                                var formData = $("#wompi_formulario").serialize();
                                var id_transaccion = data['data']['id'];
                                var reference = data['data']['reference'];

                                var pago_card = setInterval(function(){
                                    $.ajax({
                                        type: "GET",
                                        url: "/wompi/pay/result/"+id_transaccion,
                                        success: function (data) {
                                            console.log(data);
                                            if(data['data']['status'] == "PENDING"){
                                                $("#modalInformacionProcesoBody").html("Generando token de seguridad...");
                                            }else if(data['data']['status'] == "APPROVED"){
                                                    
                                                    $("#modalInformacionProcesoBody").html("Enviando a WhatsApp...");
                                                    clearInterval(pago_card);
                                                    
                                                    var formData = $("#wompi_formulario").serialize();
                                                    $.ajax({
                                                        headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                                        type: "POST",
                                                        url: "/envioformulario/wompi/"+reference,
                                                        data: formData,
                                                        success: function (data) {
                                                            alert(data);
                                                            window.onpageshow = function() {};
                                                            window.location = data;
                                                            setTimeout( function(){
                                                                location.reload(true);  
                                                            },1000);
                                                        }
                                                    });

                                            }else{
                                                clearInterval(pago_card);
                                                $("#modalInformacionProcesoBody").html("ERROR EN EL PAGO, POR FAVOR SELECCIONA UN NUEVO MÉTODO. <br> <br> <button type='button' class='btn btn-danger btn-vovler-carrito'>Atrás</button>");
                                            }
                                        }
                                    });
                                }, 1000);



                            }
                        });
                    }
                    
                // ============================================================ //
                        /* FIN VALIDACION SI ES TARJETA DE CREDITO */
                // ============================================================ //


                // ============================================================ //
                            /* INICIA VALIDACION SI ES POR NEQUI */
                // ============================================================ //

                }else if($("#method_pay").val() == "NEQUI"){ 
                    
                    console.log("NEQUI ENVIO INFORMACION");

                    if($("#celular_nequi").val().trim().length > 0){
                    
                            $(".telefono_wompi_nequi").attr("style","display:none;");
                            var celular = $("#celular_nequi").val();

                            var expresion = /^3[\d]{9}$/;
                            
                            if (isNaN(celular) || !expresion.test(celular)){
                                $(".telefono_wompi_nequi").attr("style","display:block;");
                            }else{
                                $celular_nequi = 1;
                                 $(".telefono_wompi_nequi").attr("style","display:none;");
                            }
                    }else{
                        $(".telefono_wompi_nequi").attr("style","display:block;");
                    }


                    if($celular_nequi == 1){

                        $("#nombre_wompi").attr("readonly");
                        $("#email_wompi").attr("readonly");
                        $("#telefono_wompi").attr("readonly");
                        $("#celular_nequi").attr("readonly");

                        console.log("Desarrollado y programado por: https://softworldcolombia.com");
                        console.log("Seleccinado NEQUI");

                        var formData = $("#wompi_formulario").serialize();
                            //$("#modalFormularioBody").html("<img src='../../assets/images/procesando.gif' width='100%'>");

                            $.ajax({
                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                type: "POST",
                                url: "/wompi/pay/success",
                                data: formData,
                                success: function (data) {
                                    console.log(data);
                                    return false;
                                     $("#nombre_wompi").attr("readonly");
                                    $("#email_wompi").attr("readonly");
                                    $("#telefono_wompi").attr("readonly");
                                    $("#celular_nequi").attr("readonly");
                                    ejecutandoValidacion(data);
                                }
                            });
                        }

                /* FIN VALIDACION ES POR NEQUI */



                 // ============================================================ //
                            /* INICIA VALIDACION SI ES POR PSE */
                // ============================================================ //

                }else if($("#method_pay").val() == "PSE"){
                    
                }else{  

                }
                

            }else{

            }
            

        }else{
            document.getElementById('terminos_condiciones_wompi').focus();
            alert("Por favor acepta los terminos y condiciones");
        }
    });
        

        var tiempo = $("#tiempo_contador").val();
    
    
        function ejecutandoValidacion(id_token){

            
            $("#pantalla_cubierta").attr("style","display:block; position: fixed; top:0px; height: 100%; width: 100%; background: rgba(0,0,0,0.8); z-index: 99999999 !important;");
            $("#modalInformacionProceso").modal("show");
            $("#modalInformacionProcesoBody").html("Por favor revise su celular, esperando respuesta...");


            var id = setInterval(function(){
                $.ajax({
                        type: "GET",
                        url: "/wompi/verification/nequi/"+id_token,
                        success: function (data) {
                            if(data['data']['status'] == "PENDING"){
                                $("#modalInformacionProcesoBody").html("Por favor revise su celular, esperando respuesta...");
                            }else{
                                clearInterval(id);
                                respuestaSolicitud(data);
                            }
                        }
                    });
            }, 1000); 
        }

        function respuestaSolicitud(data){
            $("#tiempo_contador").val("15000");
            clearInterval();
            if(data['data']['status'] == "APPROVED"){

                $("#modalInformacionProcesoBody").html("Suscripcion exitosa, realizando pago con NEQUI. Espera un momento por favor...");
                var formData = $("#wompi_formulario").serialize();
                var token = data['data']['id'];

                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: "POST",
                    url: "/wompi/pago/nequi/"+token,
                    data: formData,
                    success: function (data) {
                        
                        var id_transaccion = data['data']['id'];

                        var pago_wompi = setInterval(function(){

                            $.ajax({
                                    type: "GET",
                                    url: "/wompi/pay/result/"+id_transaccion,
                                    success: function (data) {
                                        if(data['data']['status'] == "PENDING"){
                                            $("#modalInformacionProcesoBody").html("Procesando...");
                                        }else if(data['data']['status'] == "APPROVED"){

                                             clearInterval(pago_wompi);
                                            
                                             var reference = data['data']['reference'];
                                            console.log(reference);

                                            alert(reference);


                                             $("#modalInformacionProcesoBody").html("Pago exitoso..");
                                             $("#modalInformacionProcesoBody").html("Enviando a WhatsApp...");

                                             var formData = $("#wompi_formulario").serialize();

                                             $.ajax({
                                                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                                                type: "POST",
                                                url: "/envioformulario/wompi/"+reference,
                                                data: formData,
                                                success: function (data) {
                                                    window.onpageshow = function() {};
                                                    window.location = data;
                                                    setTimeout( function(){
                                                        location.reload(true);  
                                                    },1000);
                                                }
                                             });
                                        }else{
                                            clearInterval(pago_wompi);
                                            $("#modalInformacionProcesoBody").html("ERROR EN EL PAGO, POR FAVOR SELECCIONA UN NUEVO MÉTODO. <br> <br> <button type='button' class='btn btn-danger btn-vovler-carrito'>Atrás</button>");
                                        }
                                    }
                                });
                        }, 1000);

                    }
                });

            }else{
                $("#modalInformacionProcesoBody").html("ERROR EN EL PAGO, POR FAVOR SELECCIONA UN NUEVO MÉTODO. <br> <br> <button type='button' class='btn btn-danger btn-vovler-carrito'>Atrás</button>");
            }
        }

       


    /* SLIDER DE MENU DE NAVEGACION CATEGORIAS*/
    
    var divs = document.getElementsByClassName("identificador_categoria").length;
    console.log("Hay " + divs + " elementos");

    var medida = 120*divs+30;
    var medida_identificador_categoria = medida/divs-7;
    console.log(medida_identificador_categoria);

    $(".categorias").attr("style", "width:"+medida+"px !important;");
    $(".identificador_categoria").attr("style", "width:"+medida_identificador_categoria+"px !important; float: left; background-color: #0E1F4F; margin:2px; border-radius: 15px; height: 42px;");
    


    /* FIN SLIDER DE MENU DE NAVEGACION CATEGORIAS*/


    });