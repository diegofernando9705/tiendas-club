$(document).ready(function(){

	 /* FORMULARIO DE CONTACTO PAGINA PRINCIPAL */

        $(document).on("click","#btn-formulario-contacto", function(){
            
            if($("#nombre").val().trim().length > 0){
                $nombre_formulario = 1;
            }else{
                $nombre_formulario = 0;
            }


            if($("#empresa").val().trim().length > 0){
                $empresa_formulario = 1;
            }else{
            	$empresa_formulario = 0;
            }


            if($("#correo").val().trim().length > 0){


            	var emailRegex = /^[-\w.%+]{1,64}@(?:[A-Z0-9-]{1,63}\.){1,125}[A-Z]{2,63}$/i;
            	var email = $("#correo").val();

            	if (emailRegex.test(email)) {
            		$email_formulario = 1;
            	} else {
            		alert("La dirección de email es incorrecta.");
            	}
            }else{
            	$email_formulario = 0;
            }


            if($("#telefono").val().trim().length > 0){
                var celular = $("#telefono").val();

                var expresion = /^3[\d]{9}$/;
                
                if (isNaN(celular) || !expresion.test(celular)){
                    alert("Debe ingresar un número válido.");
                }else{
                    //var celular_formulario = $("#telefono").val();
                    $celular_formulario = 1;
                }

            }else{
            	$celular_formulario = 0;
            }


            if($("#mensaje").val().trim().length > 0){
                $mensaje_formulario = 1;
            }else{
                $mensaje_formulario = 0;
            }

            if($nombre_formulario  == 1 && $empresa_formulario  == 1 && $email_formulario  == 1 && $celular_formulario  == 1 && $mensaje_formulario == 1){

            	var formData = $("#formulario-contacto").serialize();

	            $.ajax({
	                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
	                type: "POST",
	                url: "/registro/formulario/contacto",// ó {{url(/admin/empresa)}} depende a tu peticion se dirigira a el index(get) o tu store(post) de tu controlador 
	                data: formData,
	                success: function (data) {

	                	document.getElementById('nombre').disabled = true;
	                	document.getElementById('empresa').disabled = true;
	                	document.getElementById('correo').disabled = true;
	                	document.getElementById('telefono').disabled = true;
	                	document.getElementById('mensaje').disabled = true;

	                	document.getElementById('btn-formulario-contacto').disabled = true;


	                   console.log(data);
	                   if(data == true){
	                   	alert('Mensaje enviado');
	                   	setTimeout( window.location.reload(), 1000);
	                   }else{
	                   	alert('Imposible enviar mensaje');
	                   }
	               }
	            });

            }else{
            	console.log($nombre_formulario, $empresa_formulario, $email_formulario, $celular_formulario, $mensaje_formulario);
            	alert("Uno o mas campos se encuentran vacios");
            }
            
        });

})