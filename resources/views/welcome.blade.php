@extends('header.administrador.appnologin')

@section('content')


<!-- about -->
<section class="services py-5" id="services">
    <div class="container py-md-5 py-3">
        <div class="row rowFormularioBusqueda">

                <div class="form-group col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                    <label for="exampleInputEmail1">Nombre producto</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div>

                <div class="form-group col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                    <label for="exampleInputEmail1">Categoria producto</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div>

                <div class="form-group col-12 col-sm-3 col-md-3 col-lg-3 col-xl-3">
                    <label for="exampleInputEmail1">Tamaño</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div>

                <div class="form-group col-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
                    <label for="exampleInputEmail1">Tamaño</label>
                    <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
                </div>

                <div class="form-group col-12 col-sm-1 col-md-1 col-lg-1 col-xl-1">
                    <button type="button" class="btn btn-info">Buscar</button>
                </div>

            </div>

    </div>
      
</section>


      <h5 class="heading mb-2">Exclusive Services</h5>
        <h3 class="heading  mb-5">We Provide Awesome Services</h3>
        <div class="feature-grids row">
            <div class="col-lg-4 col-md-6 col-sm-6 mb-5">
                <div class="bottom-gd">
                    <span>01</span>
                    <h3 class="mt-4">Content Marketing </h3>
                    <p class="mt-2">Integer sit amet mattis quam, sit amet ul tricies velit. Praesent ullam corper dui turpis dolor sit amet.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 mb-5">
                <div class="bottom-gd">
                    <span>02</span>
                    <h3 class="mt-4"> Distribution Content</h3>
                    <p class="mt-2">Integer sit amet mattis quam, sit amet ul tricies velit. Praesent ullam corper dui turpis dolor sit amet.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 mb-5">
                <div class="bottom-gd">
                    <span>03</span>
                    <h3 class="mt-4"> Measurement Content</h3>
                    <p class="mt-2">Integer sit amet mattis quam, sit amet ul tricies velit. Praesent ullam corper dui turpis dolor sit amet.</p>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 col-sm-6 mb-5">
                <div class="bottom-gd">
                    <span>04</span>
                    <h3 class="mt-4">Editorial Content</h3>
                    <p class="mt-2">Integer sit amet mattis quam, sit amet ul tricies velit. Praesent ullam corper dui turpis dolor sit amet.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6  mb-5">
                <div class="bottom-gd">
                    <span>05</span>
                    <h3 class="mt-4"> Creative Content</h3>
                    <p class="mt-2">Integer sit amet mattis quam, sit amet ul tricies velit. Praesent ullam corper dui turpis dolor sit amet.</p>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 mb-5">
                <div class="bottom-gd">
                    <span>06</span>
                    <h3 class="mt-4"> Analytics Content</h3>
                    <p class="mt-2">Integer sit amet mattis quam, sit amet ul tricies velit. Praesent ullam corper dui turpis dolor sit amet.</p>
                </div>
            </div>
            </div>
    </div>
</section>
<!-- //about -->
<!-- about bottom -->
<section class="about-bottom py-5" id="about">
    <div class="container py-md-5 py-3">
    <h5 class="heading mb-2">How we work</h5>
        <h3 class="heading mb-sm-5 mb-3">HOw we deal about business</h3>
        <div class="row">
            <div class="col-lg-6 left-img">
                <img src="images/about.jpg" class="img-fluid" alt="" />
            </div>
            <div class="col-lg-6 mt-lg-0 mt-4">
                <div class="row inner-heading">
                    <div class="col-md-2">
                        <span class="fa fa-bullseye"></span>
                    </div>
                    <div class="col-md-10">
                        <h4 class="mt-md-0 mt-2">We work fast</h4>
                        <p class="mt-3">Sed ut perspiciatis unde omnis natus error dolor volup tatem ed accusantium doloremque
                        laudantium, tota rem aperiam, eaqu ipsa quae ab illo quasi architi ecto beatae vita.</p>
                        <a href="#" class="btn">Read More</a>
                    </div>
                    <div class="col-md-2 mt-5">
                        <span class="fa fa-lightbulb-o"></span>
                    </div>
                    <div class="col-md-10 mt-md-5">
                        <h4 class="mt-md-0 mt-2">Creative ideas</h4>
                        <p class="mt-3">Sed ut perspiciatis unde omnis natus error dolor volup tatem ed accusantium doloremque
                        laudantium, tota rem aperiam, eaqu ipsa quae ab illo quasi architi ecto beatae vita.</p>
                        <a href="#" class="btn">Read More</a>
                    </div>
                </div>
            </div>
        </div>
            <div class="row mt-5">
            <div class="col-lg-6 mt-lg-0 mt-4">
                <div class="row inner-heading">
                    <div class="col-md-2">
                        <span class="fa fa-question-circle"></span>
                    </div>
                    <div class="col-md-10">
                        <h4 class="mt-md-0 mt-2">Further support</h4>
                        <p class="mt-3">Sed ut perspiciatis unde omnis natus error dolor volup tatem ed accusantium doloremque
                        laudantium, tota rem aperiam, eaqu ipsa quae ab illo quasi architi ecto beatae vita.</p>
                        <a href="#" class="btn">Read More</a>
                    </div>
                    <div class="col-md-2 mt-5">
                        <span class="fa fa-bullhorn"></span>
                    </div>
                    <div class="col-md-10 mt-md-5">
                        <h4 class="mt-md-0 mt-2">Working on result</h4>
                        <p class="mt-3">Sed ut perspiciatis unde omnis natus error dolor volup tatem ed accusantium doloremque
                        laudantium, tota rem aperiam, eaqu ipsa quae ab illo quasi architi ecto beatae vita.</p>
                        <a href="#" class="btn">Read More</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 left-img">
                <img src="images/st11.png" class="img-fluid" alt="" />
            </div>
        </div>
    </div>
</section>
<!-- //about bottom -->
<!-- team -->
<section class="team py-5" id="team">
    <div class="container py-md-5 py-3">
        <div class="title-desc text-center">
            <h5 class="heading heading1 mb-2"> Our Team</h5>
        <h3 class="heading heading1 mb-sm-5 mb-3">Our Expert Minds</h3>
        </div>
        <div class="row team-grid">
            <div class="col-lg-3 col-sm-6 mb-4">
                <div class="box13">
                    <img src="images/team1.jpg" class="img-fluid img-thumbnail" alt="" />
                    <div class="box-content">
                        <h3 class="title">robert</h3>
                        <span class="post">business planer</span>
                        <ul class="social">
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mb-4">
                <div class="box13">
                    <img src="images/team2.jpg" class="img-fluid img-thumbnail" alt="" />
                    <div class="box-content">
                        <h3 class="title">pollard</h3>
                        <span class="post">business dealer</span>
                        <ul class="social">
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mb-4">
                <div class="box13">
                    <img src="images/team3.jpg" class="img-fluid img-thumbnail" alt="" />
                    <div class="box-content">
                        <h3 class="title">billings</h3>
                        <span class="post">business manager</span>
                        <ul class="social">
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-sm-6 mb-4">
                <div class="box13">
                    <img src="images/team4.jpg" class="img-fluid img-thumbnail" alt="" />
                    <div class="box-content">
                        <h3 class="title">bravo</h3>
                        <span class="post">sales manager</span>
                        <ul class="social">
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //team -->

<!-- portfolio -->
<section class="portfolio py-5" id="portfolio">
    <div class="container py-md-5 py-3">
    <h5 class="heading mb-2">our recent works</h5>
        <h3 class="heading mb-sm-5 mb-3">Our Portfolio</h3>
        <div class="row news-grids text-center">
            <div class="col-md-3 col-6 gal-img">
                <a href="#gal1"><img src="images/g1.jpg" alt="portfolio image" class="img-fluid"></a>
            </div>
            <div class="col-md-3 col-6 gal-img">
                <a href="#gal2"><img src="images/g2.jpg" alt="portfolio image" class="img-fluid"></a>
            </div>
            <div class="col-md-3 col-6 gal-img">
                <a href="#gal3"><img src="images/g3.jpg" alt="portfolio image" class="img-fluid"></a>
            </div>
            <div class="col-md-3 col-6 gal-img">
                <a href="#gal4"><img src="images/g4.jpg" alt="portfolio image" class="img-fluid"></a>
            </div>
            <div class="col-md-3 col-6 gal-img">
                <a href="#gal5"><img src="images/g5.jpg" alt="portfolio image" class="img-fluid"></a>
            </div>
            <div class="col-md-3 col-6 gal-img">
                <a href="#gal6"><img src="images/g6.jpg" alt="portfolio image" class="img-fluid"></a>
            </div>
            <div class="col-md-3 col-6 gal-img">
                <a href="#gal7"><img src="images/g7.jpg" alt="portfolio image" class="img-fluid"></a>
            </div>
            <div class="col-md-3 col-6 gal-img">
                <a href="#gal8"><img src="images/g8.jpg" alt="portfolio image" class="img-fluid"></a>
            </div>
        </div>
        <!-- portfolio popups -->
            <!-- popup-->
            <div id="gal1" class="pop-overlay animate">
                <div class="popup">
                    <img src="images/g1.jpg" alt="Popup Image" class="img-fluid" />
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#portfolio">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal2" class="pop-overlay animate">
                <div class="popup">
                    <img src="images/g2.jpg" alt="Popup Image" class="img-fluid" />
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#portfolio">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal3" class="pop-overlay animate">
                <div class="popup">
                    <img src="images/g3.jpg" alt="Popup Image" class="img-fluid" />
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#portfolio">&times;</a>
                </div>
            </div>
            <!-- //popup3 -->
            <!-- popup-->
            <div id="gal4" class="pop-overlay animate">
                <div class="popup">
                    <img src="images/g4.jpg" alt="Popup Image" class="img-fluid" />
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#portfolio">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal5" class="pop-overlay animate">
                <div class="popup">
                    <img src="images/g5.jpg" alt="Popup Image" class="img-fluid" />
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#portfolio">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal6" class="pop-overlay animate">
                <div class="popup">
                    <img src="images/g6.jpg" alt="Popup Image" class="img-fluid" />
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#portfolio">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal7" class="pop-overlay animate">
                <div class="popup">
                    <img src="images/g7.jpg" alt="Popup Image" class="img-fluid" />
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#portfolio">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- popup-->
            <div id="gal8" class="pop-overlay animate">
                <div class="popup">
                    <img src="images/g8.jpg" alt="Popup Image" class="img-fluid" />
                    <p class="mt-4">Nulla viverra pharetra se, eget pulvinar neque pharetra ac int. placerat placerat dolor.</p>
                    <a class="close" href="#portfolio">&times;</a>
                </div>
            </div>
            <!-- //popup -->
            <!-- //portfolio popups -->
    </div>
</section>
<!-- //Projects -->
<!-- /plans -->
    <section class="plans-sec py-5" id="plans">
        <div class="container py-md-5 py-3">
        <h5 class="heading mb-2"> Exclusive prices</h5>
        <h3 class="heading mb-sm-5 mb-3">We Provide Best price</h3>
                    <div class="row pricing-plans">
                        <div class="col-md-4 price-main text-center mb-4">
                            <div class="pricing-grid card">
                                <div class="card-body">
                                    <span class="fa fa-user-o" aria-hidden="true"></span>
                                    <h4 class="text-uppercase">Basic</h4>
                                    <h5 class="card-title pricing-card-title">
                                        <span class="align-top">$</span>199

                                    </h5>
                                   <p>We help you to grow up your business and solution for your impressive projects.</p>
                                    <div class="price-button mt-md-3 mt-2">
                                        <a class="btn text-uppercase" href="#contact">
                                            Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 price-main price-main1  text-center mb-4">
                            <div class="pricing-grid card">
                                <div class="card-body">
                                    <span class="fa fa-female" aria-hidden="true"></span>
                                    <h4 class="text-uppercase">Standard</h4>
                                    <h5 class="card-title pricing-card-title">
                                        <span class="align-top">$</span>199

                                    </h5>
                                    <p>We help you to grow up your business and solution for your impressive projects.</p>
                                    <div class="price-button mt-md-3 mt-2">
                                        <a class="btn text-uppercase" href="#contact">
                                            Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 price-main text-center mb-4">
                            <div class="pricing-grid card">
                                <div class="card-body">
                                    <span class="fa fa-file-video-o" aria-hidden="true"></span>
                                    <h4 class="text-uppercase">Premium</h4>
                                    <h5 class="card-title pricing-card-title">
                                        <span class="align-top">$</span>399

                                    </h5>
                                   <p>We help you to grow up your business and solution for your impressive projects.</p>
                                    <div class="price-button mt-md-3 mt-2">
                                        <a class="btn text-uppercase" href="#contact">
                                            Read More</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
        </div>
    </section>
    <!-- //plans -->

<!-- text -->
<section class="text py-5" id="register">
    <div class="container py-md-5 py-3 text-center">
        <div class="row">
            <div class="col-12">
                <h2 class="mb-4 heading">The Best Business <span>company</span>.</h2>
                <p>Sed ut perspiciatis unde omnis natus error dolor volup tatem ed accus antium dolor emque
                laudantium, totam rem aperiam, eaqu ipsa quae ab illo quasi architi ecto beatae vitae dicta
                sunt dolor ipsum.</p>
                <a href="#contact" class="btn mr-3"> Work With Us</a>
                <a href="#portfolio" class="btn"> Projects </a>
            </div>
                    <div class="col-md-6 padding">
                            <!-- banner form -->
                            <form action="#" method="post">
                                <h5 class="mb-3">Register Here</h5>
                                <div class="form-style-w3ls">
                                    <input placeholder="Your Name" name="name" type="text" required="">
                                    <input placeholder="Your Email Id" name="email" type="email" required="">
                                    <input placeholder="Contact Number" name="number" type="text" required="">
                                    <select>
                                      <option value="0">Business Type</option>
                                      <option value="1">Corporate</option>
                                      <option value="1">Partnership</option>
                                      <option value="1">Other</option>
                                    </select>
                                    <!--<input placeholder="Password" name="password" type="password" required=""> -->
                                    <button Class="btn"> Get registered</button>
                                    <span>By registering, you agree to our <a href="#">Terms & Conditions.</a></span>
                                </div>
                            </form>
                            <!-- //banner form -->
            </div>
        </div>
    </div>
</section>
<!-- //text -->
<!-- testimonials -->
<section class="testi py-5" id="testi">
    <div class="container py-md-5 py-3">
            <h5 class="heading mb-2">Testimonial</h5>
        <h3 class="heading mb-sm-5 mb-3">What Our Client Say</h3>
        <div class="row">
            <div class="col-lg-6 mb-4">
                <div class="row testi-cgrid border-right-grid">
                    <div class="col-sm-4 testi-icon mb-sm-0 mb-3">
                        <img src="images/test1.jpg" alt="" class="img-fluid"/>
                    </div>
                    <div class="col-sm-8">
                        <p class="mx-auto"><span class="fa fa-quote-left"></span> Onec consequat sapien utleo dolor rhoncus. Nullam dui mi, vulputater act metus semper. Vestibulum sed dolor.</p>
                        <h6 class="b-w3ltxt mt-3">Johnson - <span>customer</span></h6>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 mb-4">
                <div class="row testi-cgrid border-left-grid">
                    <div class="col-sm-4 testi-icon mb-sm-0 mb-3">
                        <img src="images/test2.jpg" alt="" class="img-fluid"/>
                    </div>
                    <div class="col-sm-8">
                        <p class="mx-auto"><span class="fa fa-quote-left"></span> Onec consequat sapien utleo dolor rhoncus. Nullam dui mi, vulputater act metus semper. Vestibulum sed dolor.</p>
                        <h6 class="b-w3ltxt mt-3">walkner - <span>customer</span></h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- testimonials -->
<!-- Contact page -->
<section class="contact py-5" id="contact">
    <div class="container py-md-5 py-5">
    <h5 class="heading mb-2">Contact Us</h5>
        <h3 class="heading mb-sm-5 mb-3">BUSINESS GROWTH CONTACT US</h3>
        <div class="row contact_information">
            <div class="col-md-6 contact_left">
                <div class="contact_border p-4">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6350041.310790406!2d30.68773492426509!3d39.0014851732576!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x14b0155c964f2671%3A0x40d9dbd42a625f2a!2sTurkey!5e0!3m2!1sen!2sin!4v1522753415269"></iframe>
                </div>
            </div>
            <div class="col-md-6 mt-md-0 mt-4">
                <div class="contact_right p-lg-5 p-4">
                    <form action="#" method="post">
                        <div class="w3_agileits_contact_left">
                            <h3 class="mb-3">Contact form</h3>
                            <input type="text" name="Name" placeholder="Your Name" required="">
                            <input type="email" name="Email" placeholder="Your Email" required="">
                            <input type="text" name="Phone" placeholder="Phone Number" required="">
                            <textarea placeholder="Your Message Here..." required=""></textarea>
                        </div>
                        <div class="w3_agileits_contact_right">
                            <button type="submit" >Submit</button>
                        </div>
                        <div class="clearfix"> </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- //Contact page -->


@endsection
