
<!DOCTYPE html>
<html lang="zxx">

    <head>
        <title>Tiendas Club | Bienvenidos</title>
        <!-- Meta tag Keywords -->
        <link rel="icon" href="{{ asset('web_principal/images/TIENDAS_CLUB.png') }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8" />
        <meta name="keywords" content="Tiendas club, tiendas en cali, pedidos en Cali, delivery" />
        <!-- //Meta tag Keywords -->
         <script src="https://www.google.com/recaptcha/api.js"></script>

         <script type="text/javascript" src="{{ asset('assets/web/js/wompi.js') }}"></script>
        <!-- Style CSS -->
        <link rel="stylesheet" href="{{ asset('assets/2021/css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/2021/css/estilos.css') }}" type="text/css" media="all" />
        <link rel="stylesheet" href="{{ asset('assets/2021/css/responsive.css') }}" type="text/css" media="all" />
        




        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
        <link rel="stylesheet" href="{{ asset('assets/css/slider.css') }}" type="text/css" media="all" />
        <link rel="stylesheet" href="{{ asset('assets/web/css/tarjetas_creditos.css') }}" type="text/css" media="all" />


        <link href="{{ asset('assets/css/font-awesome.css') }}" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
        <!-- //Fonts -->


        <!-- CONFIGURACION SLIDER -->
        

        <!-- FIN CONFIGURACION SLIDER -->

        {!! htmlScriptTagJsApi([
            'action' => 'homepage'
        ]) !!}
        
    </head>

    <body>
        
        <div class="contenedorWeb">
            @yield('content')
        </div>

        <!-- move top -->

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
        <script type="text/javascript" src="{{ asset('assets/2021/js/bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/2021/js/bootstrap.bundle.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/2021/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/2021/js/acciones.js') }}"></script>
            
        <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
        <script>
           function onSubmit(token) {
             document.getElementById("demo-form").submit();
           }
         </script>
         <script src="https://demo.tutorialzine.com/2016/11/simple-credit-card-validation-form/assets/js/jquery.payform.min.js" charset="utf-8"></script>
        <!-- CONFIGURACION SLIDER -->
        
        <script type="text/javascript" src="{{ asset('assets/2021/js/purejscarousel.3.0.0.js') }}"></script>

        @yield('scripts')

        <!-- copyright -->
        <div class="copyright">
            <div class="container py-4">
                <div class=" text-center">
                    <p>© 2020. All Rights Reserved | Desarrollo por: <a href="https://marketingesmarka.com/tiendasclub/">#TEAMMARKA</a> </p>
                </div>
            </div>
        </div>
        <!-- //copyright -->

        <!-- move top -->
        <div class="move-top text-right">
            <a href="#home" class="move-top">
                <span class="fa fa-angle-up  mb-3" aria-hidden="true"></span>
            </a>
        </div>



    </body>


</html>
