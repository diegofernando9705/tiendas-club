<div class="productoUnico" id="producto{{ $producto->id }}" data-id="{{ $producto->id }}" style="background-image: url('https://clientes.tiendas.club/storage/{{ $producto->imagen_producto }}'); ">
    <div class="row descripcion_texto_producto_unico">
        <div class="tituloProducto">
            <p>{{ $producto->name }}</p>
        </div>
        <div class="valorProductoUnico ">
            <strong>${{ intval($producto->valor) }}</strong>
        </div>
    </div>
</div>