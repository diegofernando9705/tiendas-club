<div class="valores_precios">
            <div class="descripcion_precio">
                VALOR DOMICILIOS
            </div>
            <div class="valor_precio">
                ${{ number_format($domicilio) }}
            </div>
        </div>
        <div class="valores_precios">
            <div class="descripcion_precio">
                COSTO ADICIONALES
            </div>
            <div class="valor_precio">
                ${{ number_format($producto_unico[3]) }}
            </div>
        </div>
         <div class="valores_precios">
            <div class="descripcion_precio">
                COSTO PRODUCTOS
            </div>
            <div class="valor_precio">
                ${{ number_format($producto_unico[2]) }}
            </div>
        </div>
        <div class="valores_precios">
            <div class="descripcion_precio">
                <strong>TOTAL</strong>
            </div>
            <div class="valor_precio" id="total_pedido_carrito_envio" value="{{ $producto_unico[1] }}00" data-valor="TOTAL SIN DOMICILIO: $ {{ number_format($producto_unico[1]) }}">
                <strong>${{ number_format($producto_unico[1]) }}</strong>
            </div>
        </div>