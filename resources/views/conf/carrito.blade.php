<h4 class="titulo_modal" style="text-align: center;">Resumen de la compra</h4>
<hr>

<div class="productos_clientes">
    @foreach(Cart::content() as $carrito)
        <div class="contenedor-carrito">
            <div class="imagen-producto-carrito">
                <div class="caratula-img" style="background-image: url('https://clientes.tiendas.club/storage/{{ $carrito->options->imageProduct }}')"></div>
            </div>
            <div class="informacion-carrito-producto">
                <div class="nombre-producto-carrito">
                    <p>{{ $carrito->name }}</p>
                </div>
                <div class="nombre-adicional-carrito">
                    {{ $carrito->options->adicionales }} ({{ $carrito->options->valor_adicional_unica }})
                </div>
            </div>
            <div class="valor-producto-carrito">
                <p>${{ $carrito->price }}</p>
                <p>${{ $carrito->options->valores_adicionales }}</p>
            </div>
            <div class="opcion-eliminar" data-id="{{ $carrito->rowId }}">
                <img src="{{ asset('assets/2021/image/cerrar.gif') }}">
            </div>
        </div>
    @endforeach
</div>
<div class="contenedor-carrito">
    <div class="descripcion_precio">
        VALOR DOMICILIO
    </div>
    <div class="valor_precio">
        ${{ number_format($domicilio) }}
    </div>
</div>
<div class="opciones_domicilio">
    <div class="opcion-domicilio">
        <input type="checkbox" class="checked-adicionales btn-local" value="domicilio" id="domi"><label for="domi">Domicilio</label>
    </div>
    <div class="opcion-local">
        <input type="checkbox" class="checked-adicionales btn-local" value="local" id="local"><label for="local">Local</label>
    </div>
</div>

<div class="valor_total">
    <div class="descripcion_valor_total_carrito">
        <p>TOTAL</p>
    </div>
    <div class="valor_total_carrito">
        0
    </div>
</div>

<input type="button" name="" id="btn-envio-pedido" class="envio-pedido btn-envio-pedido" data-op="Whatsapp-domicilio" value="Enviar Pedido">

<!--
<button type="button" class="btn btn-danger btn-cerrar-modal-carrito" data-dismiss="modal" aria-label="close">
    Cerrar ventana
</button>-->