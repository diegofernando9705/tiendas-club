
            <!-- MODAL PEDIDO EN TIENDA -->

            <div class="modal fade" id="formularioDomicilioModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">
                                <svg id="_x33_0" enable-background="new 0 0 64 64" height="40" viewBox="0 0 64 64" width="40" xmlns="http://www.w3.org/2000/svg"><g><path d="m7 33h6v2h-6z"/><path d="m15 33h26v2h-26z"/><path d="m7 37h34v2h-34z"/><path d="m62.765 37.644-1.53-1.288-7.028 8.345c-1.807-2.03-4.347-3.391-7.207-3.65v-32.051h-6v-4h-9.557c-1.028-2.351-3.374-4-6.1-4h-2.687c-2.726 0-5.072 1.649-6.1 4h-9.556v4h-6v54h45c6.065 0 11-4.935 11-11 0-2.063-.582-3.989-1.574-5.641zm-32.715-25.28c1.258-1.258 1.95-2.929 1.95-4.707 0-.222-.012-.441-.033-.657h7.033v22h-4v-4.664c0-2.966-2.215-5.52-5.151-5.939l-2.095-.299-.38-1.14c1.47-1.004 2.468-2.645 2.605-4.521zm-5.668 11.636h-.764l-.401-.803.783-.783.783.783zm2.419-1.613-1.232-1.232 1.668-1.112.894.128zm-2.801-6.387c-2.206-.001-4-1.796-4-4.002v-1.358c0-.475.338-.888.804-.981l1.431-.286c.689-.138 1.296-.478 1.765-.952.469.474 1.076.814 1.765.952l1.431.286c.466.093.804.506.804.981v1.358c0 2.206-1.794 4.001-4 4.002zm-.002 2h.004c.533 0 1.048-.077 1.541-.209l.267.801-1.81 1.206-1.81-1.207.267-.801c.493.133 1.008.21 1.541.21zm-3.235 2.044 1.668 1.112-1.232 1.232-1.329-2.216zm-3.006.494 3.044 5.075.925-.925.226.452-.772 3.86h-6.18v-4.664c0-1.741 1.146-3.267 2.757-3.798zm6.063 5.462h.36l.6 3h-1.56zm3 3-.772-3.86.226-.452.925.925 3.044-5.075c1.611.531 2.757 2.057 2.757 3.798v4.664zm-4.163-26h2.687c2.567 0 4.656 2.089 4.656 4.657 0 .58-.115 1.141-.317 1.667-.402-.816-1.158-1.439-2.095-1.626l-1.431-.286c-.67-.135-1.157-.728-1.157-1.412v-1h-2v1c0 .684-.487 1.277-1.157 1.412l-1.431.286c-.937.187-1.692.81-2.095 1.626-.202-.526-.317-1.087-.317-1.667 0-2.568 2.089-4.657 4.657-4.657zm-13.657 4h7.033c-.021.216-.033.435-.033.657 0 1.778.692 3.45 1.95 4.707l.072.072c.137 1.877 1.134 3.517 2.605 4.521l-.38 1.14-2.095.299c-2.937.42-5.152 2.974-5.152 5.94v4.664h-4zm-6 54v-50h4v20h34v-20h4v30.051c-1.43.13-2.779.537-4 1.164v-1.215h-10v2h8.695c-.81.569-1.544 1.239-2.174 2h-6.521v2h5.214c-.325.634-.59 1.302-.788 2h-4.426v2h4.051c-.03.33-.051.662-.051 1s.021.67.051 1h-4.051v2h4.426c.699 2.458 2.228 4.566 4.269 6zm52-9c0 4.962-4.038 9-9 9s-9-4.038-9-9 4.038-9 9-9c2.774 0 5.257 1.263 6.909 3.243l-6.972 8.28-4.23-4.229-1.414 1.414 5.77 5.771 7.985-9.482c.604 1.206.952 2.563.952 4.003z"/><path d="m7 47h6v-6h-6zm2-4h2v2h-2z"/><path d="m15 41h6v2h-6z"/><path d="m15 45h6v2h-6z"/><path d="m7 55h6v-6h-6zm2-4h2v2h-2z"/><path d="m15 49h6v2h-6z"/><path d="m15 53h6v2h-6z"/><path d="m23 47h6v-6h-6zm2-4h2v2h-2z"/><path d="m23 55h6v-6h-6zm2-4h2v2h-2z"/><path d="m7 57h6v2h-6z"/><path d="m15 57h14v2h-14z"/></g></svg>
                                Complete sus datos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label><b>Nombre(*)</b></label>
                                    <input type="text" class="form-control" id="nombreModal">
                                    <code id="codeNombre">Este campo es obligatorio</code>
                                </div>
                                <div class="form-group">
                                    <label><b>Domicilio (*)</b></label>
                                    <input type="text" class="form-control" id="domicilioModal">
                                    <code id="codeDomicilio">Este campo es obligatorio</code>
                                </div>
                                <div class="form-group">
                                    <label><b>Barrio (*)</b></label>
                                    <input type="text" class="form-control" id="barrioModal">
                                    <code id="codeBarrio">Este campo es obligatorio</code>
                                </div>
                                <div class="form-group">
                                    <label><b>Indicaci&oacute;n Adicional(*)</b></label>
                                    <input type="text" class="form-control" id="calleModal">
                                    <code id="codeCalles">Este campo es obligatorio</code>
                                </div>
                                <div class="form-group">
                                    <label><b>Comentarios</b></label>
                                    <textarea class="form-control" id="comentariosPedidosModal"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">

                            <div id="arregloProductos" style="display: none;">

                            </div>


                            <a id="hrefEnvioFormulario">
                                <button type="button" class="btn btn-success" id="enviarFormWhatsapp" >

                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="22" height="22" x="0" y="0" viewBox="0 0 30.667 30.667" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    <path d="M30.667,14.939c0,8.25-6.74,14.938-15.056,14.938c-2.639,0-5.118-0.675-7.276-1.857L0,30.667l2.717-8.017   c-1.37-2.25-2.159-4.892-2.159-7.712C0.559,6.688,7.297,0,15.613,0C23.928,0.002,30.667,6.689,30.667,14.939z M15.61,2.382   c-6.979,0-12.656,5.634-12.656,12.56c0,2.748,0.896,5.292,2.411,7.362l-1.58,4.663l4.862-1.545c2,1.312,4.393,2.076,6.963,2.076   c6.979,0,12.658-5.633,12.658-12.559C28.27,8.016,22.59,2.382,15.61,2.382z M23.214,18.38c-0.094-0.151-0.34-0.243-0.708-0.427   c-0.367-0.184-2.184-1.069-2.521-1.189c-0.34-0.123-0.586-0.185-0.832,0.182c-0.243,0.367-0.951,1.191-1.168,1.437   c-0.215,0.245-0.43,0.276-0.799,0.095c-0.369-0.186-1.559-0.57-2.969-1.817c-1.097-0.972-1.838-2.169-2.052-2.536   c-0.217-0.366-0.022-0.564,0.161-0.746c0.165-0.165,0.369-0.428,0.554-0.643c0.185-0.213,0.246-0.364,0.369-0.609   c0.121-0.245,0.06-0.458-0.031-0.643c-0.092-0.184-0.829-1.984-1.138-2.717c-0.307-0.732-0.614-0.611-0.83-0.611   c-0.215,0-0.461-0.03-0.707-0.03S9.897,8.215,9.56,8.582s-1.291,1.252-1.291,3.054c0,1.804,1.321,3.543,1.506,3.787   c0.186,0.243,2.554,4.062,6.305,5.528c3.753,1.465,3.753,0.976,4.429,0.914c0.678-0.062,2.184-0.885,2.49-1.739   C23.307,19.268,23.307,18.533,23.214,18.38z" fill="#ffffff" data-original="#000000" style="" class=""/>
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    </g></svg>
                                    Enviar por Whatsapp</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- --->



            <!-- MODAL PEDIDO EN TIENDA -->

            <div class="modal fade" id="formularioTiendaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">
                                <svg id="_x33_0" enable-background="new 0 0 64 64" height="40" viewBox="0 0 64 64" width="40" xmlns="http://www.w3.org/2000/svg"><g><path d="m7 33h6v2h-6z"/><path d="m15 33h26v2h-26z"/><path d="m7 37h34v2h-34z"/><path d="m62.765 37.644-1.53-1.288-7.028 8.345c-1.807-2.03-4.347-3.391-7.207-3.65v-32.051h-6v-4h-9.557c-1.028-2.351-3.374-4-6.1-4h-2.687c-2.726 0-5.072 1.649-6.1 4h-9.556v4h-6v54h45c6.065 0 11-4.935 11-11 0-2.063-.582-3.989-1.574-5.641zm-32.715-25.28c1.258-1.258 1.95-2.929 1.95-4.707 0-.222-.012-.441-.033-.657h7.033v22h-4v-4.664c0-2.966-2.215-5.52-5.151-5.939l-2.095-.299-.38-1.14c1.47-1.004 2.468-2.645 2.605-4.521zm-5.668 11.636h-.764l-.401-.803.783-.783.783.783zm2.419-1.613-1.232-1.232 1.668-1.112.894.128zm-2.801-6.387c-2.206-.001-4-1.796-4-4.002v-1.358c0-.475.338-.888.804-.981l1.431-.286c.689-.138 1.296-.478 1.765-.952.469.474 1.076.814 1.765.952l1.431.286c.466.093.804.506.804.981v1.358c0 2.206-1.794 4.001-4 4.002zm-.002 2h.004c.533 0 1.048-.077 1.541-.209l.267.801-1.81 1.206-1.81-1.207.267-.801c.493.133 1.008.21 1.541.21zm-3.235 2.044 1.668 1.112-1.232 1.232-1.329-2.216zm-3.006.494 3.044 5.075.925-.925.226.452-.772 3.86h-6.18v-4.664c0-1.741 1.146-3.267 2.757-3.798zm6.063 5.462h.36l.6 3h-1.56zm3 3-.772-3.86.226-.452.925.925 3.044-5.075c1.611.531 2.757 2.057 2.757 3.798v4.664zm-4.163-26h2.687c2.567 0 4.656 2.089 4.656 4.657 0 .58-.115 1.141-.317 1.667-.402-.816-1.158-1.439-2.095-1.626l-1.431-.286c-.67-.135-1.157-.728-1.157-1.412v-1h-2v1c0 .684-.487 1.277-1.157 1.412l-1.431.286c-.937.187-1.692.81-2.095 1.626-.202-.526-.317-1.087-.317-1.667 0-2.568 2.089-4.657 4.657-4.657zm-13.657 4h7.033c-.021.216-.033.435-.033.657 0 1.778.692 3.45 1.95 4.707l.072.072c.137 1.877 1.134 3.517 2.605 4.521l-.38 1.14-2.095.299c-2.937.42-5.152 2.974-5.152 5.94v4.664h-4zm-6 54v-50h4v20h34v-20h4v30.051c-1.43.13-2.779.537-4 1.164v-1.215h-10v2h8.695c-.81.569-1.544 1.239-2.174 2h-6.521v2h5.214c-.325.634-.59 1.302-.788 2h-4.426v2h4.051c-.03.33-.051.662-.051 1s.021.67.051 1h-4.051v2h4.426c.699 2.458 2.228 4.566 4.269 6zm52-9c0 4.962-4.038 9-9 9s-9-4.038-9-9 4.038-9 9-9c2.774 0 5.257 1.263 6.909 3.243l-6.972 8.28-4.23-4.229-1.414 1.414 5.77 5.771 7.985-9.482c.604 1.206.952 2.563.952 4.003z"/><path d="m7 47h6v-6h-6zm2-4h2v2h-2z"/><path d="m15 41h6v2h-6z"/><path d="m15 45h6v2h-6z"/><path d="m7 55h6v-6h-6zm2-4h2v2h-2z"/><path d="m15 49h6v2h-6z"/><path d="m15 53h6v2h-6z"/><path d="m23 47h6v-6h-6zm2-4h2v2h-2z"/><path d="m23 55h6v-6h-6zm2-4h2v2h-2z"/><path d="m7 57h6v2h-6z"/><path d="m15 57h14v2h-14z"/></g></svg>
                                Complete sus datos</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <label><b>Nombre(*)</b></label>
                                    <input type="text" class="form-control" id="nombreModalTienda">
                                    <code id="codeNombre">Este campo es obligatorio</code>
                                </div>
                                <div class="form-group">
                                    <label><b>Telefono(*)</b></label>
                                    <input type="text" class="form-control" id="telefonoModalTienda">
                                    <code id="codeDomicilio">Este campo es obligatorio</code>
                                </div>
                                <div class="form-group">
                                    <label><b>Tiempo de Llegada(*)</b></label>
                                    <input type="text" class="form-control" id="tiempoLlegadaModalTienda">
                                    <code id="codeBarrio">Este campo es obligatorio</code>
                                </div>
                                <div class="form-group">
                                    <label><b>Comentarios</b></label>
                                    <textarea class="form-control" id="comentariosPedidosModalTienda"></textarea>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">

                            <div id="arregloProductos" style="display: none;">

                            </div>


                            <a id="hrefEnvioFormulario">
                                <button type="button" class="btn btn-success" id="enviarFormWhatsappTienda" >

                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.com/svgjs" version="1.1" width="22" height="22" x="0" y="0" viewBox="0 0 30.667 30.667" style="enable-background:new 0 0 512 512" xml:space="preserve" class=""><g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    <path d="M30.667,14.939c0,8.25-6.74,14.938-15.056,14.938c-2.639,0-5.118-0.675-7.276-1.857L0,30.667l2.717-8.017   c-1.37-2.25-2.159-4.892-2.159-7.712C0.559,6.688,7.297,0,15.613,0C23.928,0.002,30.667,6.689,30.667,14.939z M15.61,2.382   c-6.979,0-12.656,5.634-12.656,12.56c0,2.748,0.896,5.292,2.411,7.362l-1.58,4.663l4.862-1.545c2,1.312,4.393,2.076,6.963,2.076   c6.979,0,12.658-5.633,12.658-12.559C28.27,8.016,22.59,2.382,15.61,2.382z M23.214,18.38c-0.094-0.151-0.34-0.243-0.708-0.427   c-0.367-0.184-2.184-1.069-2.521-1.189c-0.34-0.123-0.586-0.185-0.832,0.182c-0.243,0.367-0.951,1.191-1.168,1.437   c-0.215,0.245-0.43,0.276-0.799,0.095c-0.369-0.186-1.559-0.57-2.969-1.817c-1.097-0.972-1.838-2.169-2.052-2.536   c-0.217-0.366-0.022-0.564,0.161-0.746c0.165-0.165,0.369-0.428,0.554-0.643c0.185-0.213,0.246-0.364,0.369-0.609   c0.121-0.245,0.06-0.458-0.031-0.643c-0.092-0.184-0.829-1.984-1.138-2.717c-0.307-0.732-0.614-0.611-0.83-0.611   c-0.215,0-0.461-0.03-0.707-0.03S9.897,8.215,9.56,8.582s-1.291,1.252-1.291,3.054c0,1.804,1.321,3.543,1.506,3.787   c0.186,0.243,2.554,4.062,6.305,5.528c3.753,1.465,3.753,0.976,4.429,0.914c0.678-0.062,2.184-0.885,2.49-1.739   C23.307,19.268,23.307,18.533,23.214,18.38z" fill="#ffffff" data-original="#000000" style="" class=""/>
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    <g xmlns="http://www.w3.org/2000/svg">
                                    </g>
                                    </g></svg>
                                    Enviar por Whatsapp</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            
<!-- MODAL PARA VER LAS IMAGENES DE LOS PRODUCTOS -->

<!-- The Modal -->





<!-- MODAL PARA VER LAS IMAGENES DE LOS PRODUCTOS -->


<!-- MODAL AGREGAR ADICIONALES, COMENTARIOS DEL PRODUCTO -->

<div class="modal fade" id="modalProductos"  tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body" id="modalProductosBody">
                <!--<p style="font-size: 18px; font-weight: bold;" id="nameModal"></p>
                <hr>-->
                <table class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <tr>
                        <td style="" id="imagenModal"></td>
                    </tr>
                    <tr>
                    <td style="text-align: left;" id="descriptionModal"></td>
                    </tr>
                    <tr>
                    <td style="text-align: left;" id="valueModal"></td>
                    </tr>
                </table>
                <div id="adicionalesProductos"></div>

                

            </div>
        </div>
    </div>
</div>


