<div class="productoUnicoCerrado" id="producto{{ $producto->id }}" data-id="{{ $producto->id }}">
    <div class="row" >
        <div class="imgProductoUnico">
            <img src="https://clientes.tiendas.club/storage/{{ $producto->imagen_producto }}" >

        </div>
        <div class="descripcionProductoUnico">
            <a>{{ $producto->name }}</a>
        </div>
        <div class="valorProductoUnico ">
            <strong>${{ $producto->valor }}</strong>
        </div>
    </div>
</div>