<h4>Buz&oacute;n de Sugerencia</h4>
<div class="alert alert-danger">
	Todos los campos marcados con asteriscos (*) son obligatorios. 

</div>
<form action="{{ url('envioformulario/sugerencia') }}" id="formularioSugerencia" method="POST">
	@csrf
	@method('POST')
	<div class="form-group">
		<label><strong><code>(*)</code></strong> Nombre:</label>
		<input type="text" name="nombreSugerencia" id="nombreSugerencia" class="form-control">
		<code class="nombreSugerencia" style="display: none;">Por favor, coloque su nombre.</code>
	</div>
	<div class="form-group">
		<label><strong><code>(*)</code></strong> Tel&eacute;fono:</label>
		<input type="number" name="telefonoSugerencia" id="telefonoSugerencia" class="form-control">
		<code class="telefonoSugerencia" style="display: none;">Por favor, coloque un número de contacto.</code>
	</div>
	<div class="form-group">
		<label><strong><code>(*)</code></strong> Calificaci&oacute;n: </label>
		  <p class="clasificacion">
		    	
		    	<input id="radio1" type="radio" name="estrellas" value="5" ><!--
		    --><label for="radio1" class="estrellas_label">&#9733;</label><!--
		    --><input id="radio2" type="radio" name="estrellas" value="4"><!--
		    --><label for="radio2" class="estrellas_label">&#9733;</label><!--
		    --><input id="radio3" type="radio" name="estrellas" value="3"><!--
		    --><label for="radio3" class="estrellas_label">&#9733;</label><!--
		    --><input id="radio4" type="radio" name="estrellas" value="2"><!--
		    --><label for="radio4" class="estrellas_label">&#9733;</label><!--
		    --><input id="radio5" type="radio" name="estrellas" value="1"><!--
		    --><label for="radio5" class="estrellas_label">&#9733;</label>
		  </p>
		<code class="calificacionSugerencia" style="display: none;">Por favor, coloque su calificaci&oacute;n.</code>
	</div>
	<div class="form-group">
		<label><strong><code></code></strong> Mensaje: </label>
		<textarea class="form-control" name="comentariosSugerencia" id="comentariosSugerencia"></textarea>
	</div>
	
	<input type="hidden" name="codigo_tienda" value="{{ $codigo_tienda }}">
	<!--<button type="button" class="btn btn-danger btn-cerrar-modal-carrito" data-dismiss="modal" aria-label="close">
		Cerrar ventana
	</button>-->

	<button type="submit" class="btn btn-success btn-envio-pedido-form" style="margin-top: 0px; margin-right: -12px;">Enviar</button>
	
</form>