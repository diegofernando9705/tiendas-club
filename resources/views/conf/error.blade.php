@if(count($errors))

<div class="alert alert-danger">
	<button type="button" class="close" data-dismiss="alert">
		&times;
	</button>
	<ul>
		@foreach($errors->all() as $error)
			 <li style="margin-left: 15px;">{{ $error }}</li>
		@endforeach
	</ul>
</div>
@endif

@if (session('status_success'))
<div class="alert alert-success">
	<button type="button" class="close" data-dismiss="alert">
		&times;
	</button>
	<ul>
        <li>{{ session('status_success') }}</li>
</div>
@endif