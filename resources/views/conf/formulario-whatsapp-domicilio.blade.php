<h4 class="titulo">Formulario para Domicilio</h4>
<div class="alert alert-danger">
	Todos los campos marcados con asteriscos (*) son obligatorios.
</div>
<form method="POST" id="formularioWhatsapp">
	@csrf
	@method('POST')
		<input type="hidden" name="opcion" class="form-control" value="whatsapp">

	<div class="form-group">
		<strong><code>(*)</code></strong> Nombre:
		<input type="text" name="nombre" id="nombre" class="form-control">
		<code class="nombreDomicilio" style="display: none;">Por favor, coloque su nombre.</code>
	</div>
	<div class="form-group">
		<strong><code>(*)</code></strong> Celular:
		<input type="number" name="telefono" id="telefono" class="form-control">
		<code class="telefonoDomicilio" style="display: none;">Por favor, coloque un número de contacto.</code>
	</div>
	<div class="form-group">
		<strong><code>(*)</code></strong> Direccion: 
		<input type="text" name="direccion" id="direccion" class="form-control">
		<code class="direccionDomicilio" style="display: none;">Por favor, coloque su direccion.</code>
	</div>
	<div class="form-group">
		<strong><code>(*)</code></strong> Barrio: 
		<input type="text" name="barrio" id="barrio" class="form-control">
		<code class="barrioDomicilio" style="display: none;">Por favor, coloque su Barrio.</code>
	</div>
	<div class="form-group">
		Indicacion adicional: 
		<input type="text" name="indicacion_adicional" class="form-control">
	</div>


	@if(count($zonas) > 0)
		<div class="form-group">
			Por favor, selecciona una zona: 
			<select class="form-control" name="zonas">
				<option value="" selected="">Selecciona una zona</option>
				@foreach($zonas as $zona)
					@if($zona->estado_zona == 1)
						<option value="{{ $zona->id_zona }}">{{ $zona->nombre_zona }}</option>
					@else
					@endif
				@endforeach
			</select>
		</div>
	@else
		
	@endif

	<input type="hidden" id="codigo_referencia_wompi" name="codigo_referencia_wompi" value="">
	<div class="form-group">
		Comentarios: 
		<textarea class="form-control" name="comentarios"></textarea>
	</div>
	<input type="hidden" name="codigo_tienda" value="{{ $codigo_tienda }}">
	
	@if($validate_wompi == 1)
	<div class="row">
		<div class="boton-whatsapp">
			<button type="button" class="envio-formulario" data-op="whatsapp">
				Enviar por WhatsApp
			</button>
		</div>
		<!--
		<div class="boton-wompi">
			<button type="button" class="envio-formulario-wompi" data-op="whatsapp">
				Enviar por Wompi
			</button>
		</div>-->
	</div>
	@else
	<div class="row">
		<div class="boton-whatsapp">
			<button type="button" class="envio-formulario" data-op="whatsapp">
				Enviar por WhatsApp
			</button>
		</div>
	</div>
	@endif
	<!--
	<button type="button" class="btn btn-danger btn-vovler-carrito">
    	Atrás
	</button>-->
</form>