 @foreach($tiendas as $tienda)

        @if(isset($tienda->background_tienda))
            <div class="encabezadoTiendasInformacion col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="background-image: url('https://clientes.tiendas.club/storage/{{ $tienda->background_tienda }}') !important; background-size: cover !important;">
        @else
            <div class="encabezadoTiendasInformacion col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="background-image: url('{{ asset('assets/images/banner.jpg') }} ') !important; background-size: cover !important;">
        @endif

        <!-- ENCABEZADO DE LA TIENDA -->

        <div class="container" style="padding-top: 0px; margin-top: 0px;">
        	<div class="row">
        		<div class="col-3 col-md-3 col-sm-3 col-lg-3 col-xl-3 btnAtras">
        		</div>
        		<div class="col-6 col-md-6 col-sm-6 col-lg-6 col-xl-6 logoEncabezado">
        		</div>
        		<div class="col-3 col-md-3 col-sm-3 col-lg-3 col-xl-3 menuOpcion" data-code="{{ $tienda->id_tienda }}">
        		</div>
        	</div>
        	<input type="hidden" id="id_tienda" value="{{ $tienda->id_tienda }}"/>
        	<div class="tituloTienda">    
        		<img src="https://clientes.tiendas.club/storage/{{ $tienda->image_tienda }}" width="200px">
        	</div>
        </div>

    </div>
    <div class="container">
    	<div class="informacion_general">
    		<table class="table table-hovered tablaInformacion">
    			<tr>
    				<th><b>Nombre de la tienda:</b></th>
    				<td>{{ $tienda->nombre_tienda }}</td>
    			</tr>
    			<tr>
    				<th><b>Dirección de la tienda:</b></th>
    				<td>{{ $tienda->direccion_tienda }}</td>
    			</tr>
    			<tr>
    				<th><b>Sobre la tienda:</b></th>
    				<td>{{ $tienda->descripcion_tienda }}</td>
    			</tr>
    			<tr>
    				<th><b>Celular de la tienda:</b></th>
    				<td>{{ $tienda->telefono_movil }}</td>
    			</tr>
    		</table>
    	</div>
    </div>  
    @endforeach