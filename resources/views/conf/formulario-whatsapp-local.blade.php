<h4 class="titulo">Formulario para Local</h4>
<div class="alert alert-danger">
	Todos los campos marcados con asteriscos (*) son obligatorios.
</div>
<form method="POST" id="formularioLocal">
    @csrf
    @method('POST')
	<div class="form-group">
		<strong><code>(*)</code></strong> Nombre: 
		<input type="text" name="nombre" id="nombre" class="form-control" required="">
		<code class="nombreLocal" style="display: none;">Por favor, coloque su nombre.</code>
	</div>
	<div class="form-group">
		<strong><code>(*)</code></strong> Celular:
		<input type="number" name="telefono" id="telefono" class="form-control"  required="">
		<code class="telefonoLocal" style="display: none;">Por favor, coloque su telefono.</code>
	</div>
	<div class="form-group">
		<strong><code>(*)</code></strong> Llegare a las:
		<input type="time" name="hora_llegada" id="hora_llegada" class="form-control"  required="">
		<code class="horaLocal" style="display: none;">Por favor, coloque su Hora de llegada.</code>
	</div>
	
	@if(count($zonas) > 0)
		<div class="form-group">
			Por favor, selecciona una zona: 
			<select class="form-control" name="zonas">
				<option value="" selected="">Selecciona una zona</option>
				@foreach($zonas as $zona)
					@if($zona->estado_zona == 1)
						<option value="{{ $zona->id_zona }}">{{ $zona->nombre_zona }}</option>
					@else
					@endif
				@endforeach
			</select>
		</div>
	@else
		
	@endif

	<div class="form-group">
		Comentarios:
        <textarea class="form-control" name="comentarios"></textarea>
	</div>
	<div class="row">
    	<input type="hidden" name="codigo_tienda" value="{{ $codigo_tienda }}">
    	<input type="hidden" name="opcion_domicio" id="opcion_domicio" value="">

    	<input type="hidden" id="codigo_referencia_wompi" name="codigo_referencia_wompi" value="">

@if($validate_wompi == 1)
	<div class="row">
		<div class="boton-whatsapp">
			<button type="button" class="envio-formulario" data-op="local">
				Enviar por WhatsApp
			</button>
		</div>
		<!--
		<div class="boton-wompi">
			<button type="button" class="envio-formulario-wompi" data-op="local">
				Enviar por Wompi
			</button>
		</div>-->
	</div>
	@else
	<div class="row">
		<div class="boton-whatsapp">
			<button type="button" class="envio-formulario" data-op="local">
				Enviar por WhatsApp
			</button>
		</div>
	</div>
	@endif
    	
    	
	</div>
	<!--
	<button type="button" class="btn btn-danger btn-vovler-carrito">
    	Atrás
	</button>-->
</form>