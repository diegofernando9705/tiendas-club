<h4 style="text-align: center;">Resumen de la compra</h4>
<hr>
<div class="contenidoCarrito">
    <table>
        <tr>
            <td>Imagen</td>
            <td>Nombre</td>
            <td>Adicionales</td>
            <!--<td>Total</td>-->

        </tr>
        <?php $i=1; ?>
        @foreach(Cart::content() as $carrito)
            <tr>

                <td>
                    <img src="https://clientes.tiendas.club/storage/{{ $carrito->options->imageProduct }}">
                </td>
                <td>
                    <p>{{ $carrito->name }}</p>
                </td>
                <td>
                    <p>{{ $carrito->options->adicionales }}</p>
                </td>
                <td></td>
                <!--
                <td>
                    <p>{{ $carrito->price }}</p>
                </td>-->
            </tr>

        @endforeach
    </table>

</div>
<br><br>
<!--
<button type="button" class="btn btn-danger btn-cerrar-modal-carrito" data-dismiss="modal" aria-label="close">
    Cerrar ventana
</button>-->