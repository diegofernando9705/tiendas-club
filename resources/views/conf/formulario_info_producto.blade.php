<form id="formularioPedido" method="POST">
    @csrf
    @method('POST')
    @foreach($productos as $producto)
        <input type="hidden" name="codigo_producto" value="{{ $producto->id }}">
        <input type="hidden" name="nombre_producto" value="{{ $producto->name }}">
        <input type="hidden" name="valor_producto" value="{{ $producto->valor }}">
        <input type="hidden" name="imagen_producto" value="{{ $producto->imagen_producto }}">
        
        
        <div class="descripcion_producto">
            <div class="imagen_descripcion_producto">
                <img src="https://clientes.tiendas.club/storage/{{ $producto->imagen_producto }}" >
            </div>
            <div class="texto_descripcion_producto">
                <div class="div_nombre_producto">
                    <p id="nombre_producto">{{ $producto->name }}</p>
                </div>
                <div class="div_valor_producto">
                    <strong id="valor_producto">${{ $producto->valor }}</strong>
                </div>
                <div class="div_descripcion_producto">
                    <p>{{ $producto->description }}</p>
                </div>
            </div>
        </div>

        <div class="productoCompra">

                <div class="valorProductoCompra">
                    
                </div>
                
                
                <div class="contenedor_adicionales">
                <?php $i=1; ?>
                @foreach ($adicionales as $adicion)
                    <?php $i++; ?>
                    <div class="adicional_unico">
                        <input type="checkbox" name="adicionales[]" value="{{ $adicion->id_adicional }}">
                        {{ $adicion->nombre_adicional }}
                    </div>
                @endforeach
                </div>
                <div class="comentarios_producto">
                    <textarea class="form-control" placeholder="Comentarios del producto..." id="comentarios_producto" name="comentarios_producto"></textarea>
                </div>
            </div>
        </div>
    @endforeach
    <div class="actionsAddDel col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <button type="button"  class="btn iconAddPro" id="iconAddPro"> 
            <l style="color:white;">Agregar producto +</l>
        </button>

    </div>