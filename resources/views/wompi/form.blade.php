<div id="div_resultado_wompi">

	<h4>PAGO EN LINEA</h4>
	<div class="alert alert-danger">
		Todos los campos marcados con asteriscos (*) son obligatorios. 
	</div>
	<form id="wompi_formulario">
		@csrf
		@method('POST')

		<input type="hidden" name="codigo_tienda" value="{{ $codigo_tienda }}">
		<input type="hidden" name="hora_llegada" value="{{ $hora_cliente }}">
		<input type="hidden" name="comentarios" value="{{ $comentarios_cliente }}">
		<input type="hidden" name="opcion" value="local">


		<div class="form-group">
			<strong><code>(*)</code></strong> Nombre:
			<input type="text" name="nombre_wompi" id="nombre_wompi" class="form-control" value="{{ $nombre_cliente }}">
			<code class="nombre_wompi" style="display: none;">Por favor, coloque su nombre.</code>
		</div>

		<div class="form-group">
			<strong><code>(*)</code></strong> Correo:
			<input type="email" name="email_wompi" id="email_wompi" class="form-control" >
			<code class="email_wompi" style="display: none;">Por favor, ingrese un correo valido.</code>
		</div>

		<div class="form-group">
			<strong><code>(*)</code></strong> Tel&eacute;fono:
			<input type="number" name="telefono_wompi" id="telefono_wompi" class="form-control" value="{{ $telefono_cliente }}">
			<code class="telefono_wompi" style="display: none;">Por favor, coloque un número de contacto.</code>
		</div>

		<input type="hidden" name="method" id="method_pay">

		<div class="form-group" id="div_metodo_pago">
			<div class="row">
				<div class="contenedor-tarjetas">
					@foreach($accepted_payment_methods as $method)
				    	@if($method == 'NEQUI')
						    <div id="NEQUI" class="NEQUI">
						    	<div class="select_nequi">
							        <img src="{{ asset('assets/images/marca-de-verificacion.png') }}">
							    </div>
						        <div class="img_nequi">
						            <img src="{{ asset('assets/images/nequi.jpg') }}" width="100%">
						        </div>
						        <div class="descripcion">
						            Nequi
						        </div>
						    </div>
				    	@endif
						@if($method == 'CARD')
						    <div id="CARD" class="CARD">
						    	<div class="select_card">
							        <img src="{{ asset('assets/images/marca-de-verificacion.png') }}">
							    </div>
						        <div class="img_nequi">
						            <img src="{{ asset('assets/images/tarjetas.png') }}" width="100%">
						        </div>
						        <div class="descripcion">
						            Tarjetas de cr&eacute;dito
						        </div>
						    </div>
				    	@endif
					@endforeach
				</div>
				<!--
				<div class="contenedor-tarjetas">
					/* @foreach($accepted_payment_methods as $method)
					@if($method == 'PSE')
						    <div id="PSE" class="PSE">
						    	<div class="select_pse">
							        <img src="{{ asset('assets/images/marca-de-verificacion.png') }}">
							    </div>
						        <div class="img_nequi">
						            <img src="{{ asset('assets/images/PSE.png') }}" width="100%">
						        </div>
						        <div class="descripcion">
						            PSE
						        </div>
						    </div>
				    	@endif
				    	@if($method == 'BANCOLOMBIA_TRANSFER')
						    <div id="BANCOLOMBIA_TRANSFER" class="BANCOLOMBIA_TRANSFER">
						    	<div class="select_transferencia">
							        <img src="{{ asset('assets/images/marca-de-verificacion.png') }}">
							    </div>
						        <div class="img_nequi">
						            <img src="{{ asset('assets/images/bancolombia.png') }}" width="100%">
						        </div>
						        <div class="descripcion">
						            Cuenta Bancolombia
						        </div>
						    </div> 
				    	@endif
				    @endforeach */
				</div>-->
			</div>
		</div>

		<br />

		<div class="form-check">

		  <center>
		  <input class="form-check-input" type="checkbox" name="acceptance_token" id="terminos_condiciones_wompi" value="true" />
			<label class="form-check-label" for="terminos_condiciones_wompi">
		  	
		    	Acepto haber le&iacute;do los <b><a href="{{ $permalink }}" target="_blank">t&eacute;minos y condiciones y la pol&iacute;tica de privacidad</b></a> para hacer esta compra.
		  	
		  </center>
		</div>

		<br><br>
		
		<button type="button" class="btn btn-success envio-wompi envio_formulario_api" data-funcion="capturar-informacion" data-metodo="local" style="margin-top: 0px; margin-right: -12px;">Enviar</button>

		<button type="button" class="btn btn-danger btn-cerrar-modal-carrito" data-dismiss="modal" aria-label="close">
			Cerrar ventana
		</button>
	</form>
</div>