<h4>Formulario para Domicilio</h4>
<div class="alert alert-danger">
	Todos los campos marcados con asteriscos (*) son obligatorios.
</div>
<form method="POST" id="wompi_formulario">
	@csrf
	@method('POST')
		<input type="hidden" name="opcion" class="form-control" value="whatsapp">

		<input type="hidden" name="codigo_tienda" value="{{ $codigo_tienda }}">

	<div class="form-group">
		<strong><code>(*)</code></strong> Nombre:
		<input type="text" name="nombre" id="nombre" class="form-control" value="{{ $nombre }}">
		<code class="nombreDomicilio" style="display: none;">Por favor, coloque su nombre.</code>
	</div>

	<div class="form-group" id="div_correo" style="display: none;">
		<strong><code>(*)</code></strong> Correo electr&oacute;nico:
		<input type="text" name="email_wompi" id="email_wompi" class="form-control" value="">
		<code class="email_wompi" style="display: none;">Por favor, coloque su nombre.</code>
	</div>

	<div class="form-group">
		<strong><code>(*)</code></strong> Celular:
		<input type="number" name="telefono" id="telefono" class="form-control" value="{{ $telefono }}">
		<code class="telefonoDomicilio" style="display: none;">Por favor, coloque un número de contacto.</code>
	</div>
	<div class="form-group" style="display: none;">
		<strong><code>(*)</code></strong> Direccion: 
		<input type="text" name="direccion" id="direccion" class="form-control" value="{{ $direccion }}">
		<code class="direccionDomicilio" style="display: none;">Por favor, coloque su direccion.</code>
	</div>
	<div class="form-group" style="display: none;">
		<strong><code>(*)</code></strong> Barrio: 
		<input type="text" name="barrio" id="barrio" class="form-control" value="{{ $barrio }}">
		<code class="barrioDomicilio" style="display: none;">Por favor, coloque su Barrio.</code>
	</div>
	<div class="form-group" style="display: none;">
		Indicacion adicional: 
		<input type="text" name="indicacion_adicional" class="form-control" value="{{ $indicacion_adicional }}">
	</div>
	<input type="hidden" id="codigo_referencia_wompi" name="codigo_referencia_wompi" value="{{ $codigo_referencia_wompi }}">
	<div class="form-group" style="display: none;">
		Comentarios: 
		<textarea class="form-control" name="comentarios" value="{{ $comentarios }}"></textarea>
	</div>

	<input type="hidden" name="method" id="method_pay">

	<div class="form-group" id="div_metodo_pago">
			<div class="row">
				<div class="contenedor-tarjetas">
					@foreach($accepted_payment_methods as $method)
				    	@if($method == 'NEQUI')
						    <div id="NEQUI" class="NEQUI">
						    	<div class="select_nequi">
							        <img src="{{ asset('assets/images/marca-de-verificacion.png') }}">
							    </div>
						        <div class="img_nequi">
						            <img src="{{ asset('assets/images/nequi.jpg') }}" width="100%">
						        </div>
						        <div class="descripcion">
						            Nequi
						        </div>
						    </div>
				    	@endif
						@if($method == 'CARD')
						    <div id="CARD" class="CARD">
						    	<div class="select_card">
							        <img src="{{ asset('assets/images/marca-de-verificacion.png') }}">
							    </div>
						        <div class="img_nequi">
						            <img src="{{ asset('assets/images/tarjetas.png') }}" width="100%">
						        </div>
						        <div class="descripcion">
						            Tarjetas de cr&eacute;dito
						        </div>
						    </div>
				    	@endif
					@endforeach
				</div>
				<!--
				<div class="contenedor-tarjetas">
					/* @foreach($accepted_payment_methods as $method)
					@if($method == 'PSE')
						    <div id="PSE" class="PSE">
						    	<div class="select_pse">
							        <img src="{{ asset('assets/images/marca-de-verificacion.png') }}">
							    </div>
						        <div class="img_nequi">
						            <img src="{{ asset('assets/images/PSE.png') }}" width="100%">
						        </div>
						        <div class="descripcion">
						            PSE
						        </div>
						    </div>
				    	@endif
				    	@if($method == 'BANCOLOMBIA_TRANSFER')
						    <div id="BANCOLOMBIA_TRANSFER" class="BANCOLOMBIA_TRANSFER">
						    	<div class="select_transferencia">
							        <img src="{{ asset('assets/images/marca-de-verificacion.png') }}">
							    </div>
						        <div class="img_nequi">
						            <img src="{{ asset('assets/images/bancolombia.png') }}" width="100%">
						        </div>
						        <div class="descripcion">
						            Cuenta Bancolombia
						        </div>
						    </div> 
				    	@endif
				    @endforeach */
				</div>-->
			</div>
	</div>

		<div class="form-check">
			

		  <center>
		  <input class="form-check-input" type="checkbox" name="acceptance_token" id="terminos_condiciones_wompi" value="true" />
		  	<label class="form-check-label" for="terminos_condiciones_wompi">
		    	Acepto haber le&iacute;do los <b><a href="{{ $permalink }}" target="_blank">t&eacute;minos y condiciones y la pol&iacute;tica de privacidad</b></a> para hacer esta compra.
		  	
		  </center>
		</div>


<button type="button" class="btn btn-success envio-wompi envio_formulario_api" data-funcion="capturar-informacion" data-metodo="domicilio" style="margin-top: 0px; margin-right: -12px;">Enviar</button>

	<button type="button" class="btn btn-danger btn-vovler-carrito">
		Atrás
	</button>
</form>