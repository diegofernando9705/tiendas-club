<div class="form-group">
	<code>(*)</code><label>Tipo de persona</label>
	<select class="form-control">
		<option value="0">Persona Natural</option>
	</select>
</div>
<div class="form-group">
	<code>(*)</code> <label>Seleccione su banco</label>
	<select class="form-control">
		@foreach($data_pay as $bancos)
			@foreach($bancos as $informacion)
				<option value="{{ $informacion['financial_institution_code'] }}">{{ $informacion['financial_institution_name'] }}</option>
			@endforeach
		@endforeach

	</select>
</div>
<div class="row">
	<div class="valor_pagaras col-12 col-sm-12 col-lg-12 col-xl-12">
		<p>
			$ {{ $total }} COP
		</p>
	</div>
</div>