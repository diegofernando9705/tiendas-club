
<form id="formulario_tarjeta_pago" >
	
	@method('POST')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <input type="hidden" name="nombre_wompi" value="{{ $nombre_wompi }}">
    <input type="hidden" name="email_wompi" value="{{ $email_wompi }}">
    <input type="hidden" name="telefono_wompi" value="{{ $telefono_wompi }}">
    <input type="hidden" name="acceptance_token" value="{{ $acceptance_token }}">
    <input type="hidden" name="method" value="{{ $method }}">
    <input type="hidden" name="nombre_tarjeta" value="{{ $nombre_tarjeta }}">
    <input type="hidden" name="cvv_tarjeta" value="{{ $cvv_tarjeta }}">
    <input type="hidden" name="numero_tarjeta" value="{{ $numero_tarjeta }}">
    <input type="hidden" name="mes_tarjeta" value="{{ $mes_tarjeta }}">
    <input type="hidden" name="ano_tarjeta" value="{{ $ano_tarjeta }}">
    <div class="container py-md-5 py-3">

    </div>

    <button type="button" class="btn btn-success pagar_tarjetas_wompi col-12 col-sm-12 col-lg-12 col-xl-12" style="margin-bottom: 8px;">
        PAGAR
    </button>
        <button type="button" class="btn btn-danger btn-cerrar-modal-carrito" data-dismiss="modal" aria-label="close">
        Cerrar ventana
    </button>
</form>
<script type="text/javascript">
    /*PAGAR CON TARJETA DE CREDITO*/
    $(document).on("click", ".pagar_tarjetas_wompi", function(){
        
        var formData = $("#formulario_tarjeta_pago").serialize();
        
        $.ajax({
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            type: "POST",
            url: "/wompi/pay/success",
            data: formData,
            success: function (data) {
                console.log(data);
                alert(data);

            }
        });

    });

</script>