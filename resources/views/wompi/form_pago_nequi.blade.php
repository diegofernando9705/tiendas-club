<div class="form-group owner">
    <code><b>(*)</b></code>N&uacute;mero celular asociado a Nequi:
    <input type="text" class="form-control" id="celular_nequi" name="celular_nequi" value="{{ $celular }}" />
    <code class="telefono_wompi_nequi" style="display: none;">Por favor, coloque un número de <b>celular</b> válido.</code>
</div>

<input type="hidden" name="codigo_tienda" value="{{ $codigo_tienda }}">