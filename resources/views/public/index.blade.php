  
<!doctype html>
<html lang="zxx">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Tiendas Club | Principal</title>
  <link rel="icon" href="{{ asset('web_principal/images/TIENDAS_CLUB.png') }}">

  <!-- Template CSS -->
  <link href="//fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700&display=swap" rel="stylesheet">
  <link href="//fonts.googleapis.com/css2?family=Limelight&display=swap" rel="stylesheet">
  <!-- Template CSS -->
  <link rel="stylesheet" href="{{ asset('web_principal/css/style-starter.css') }}">
  <!-- Template CSS -->
</head>

<body>

  <!--header-->
  <header id="site-header" class="fixed-top">
    <div class="container">
      <nav class="navbar navbar-expand-lg navbar-light">
        <h1>
        	<img src="{{ asset('web_principal/images/TIENDAS_CLUB.png') }}" width="150px">
        </h1>
        <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarNav"
          aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="fa icon-expand fa-bars"></span>
          <span class="fa icon-close fa-times"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            
          </ul>
          <ul class="navbar-nav search-right mt-lg-0 mt-2">
          	<li class="nav-item">
              <a class="nav-link" href="#como-funciona">¿Como funciona? </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#clientes-tiendas">Ejemplos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#section-planes">Precios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#centro-de-ayuda">Centro de ayuda</a>
            </li>
            <li>
            	<a class="nav-link" href="https://clientes.tiendas.club/login" target="_blank" style="padding: 5px 15px 5px 15px; border: 1px solid white; border-radius: 15px;">Ingresar</a>
            </li>
            <li>
            	<a class="nav-link" href="https://clientes.tiendas.club/register" target="_blank" style="margin-left:10px; padding: 5px 15px 5px 15px; border: 1px solid white; border-radius: 15px;">Registrarme</a>
            </li>
          </ul>

          <!-- //toggle switch for light and dark theme -->
          <!-- search popup -->
          <div id="search" class="pop-overlay">
            <div class="popup">
              <form action="#" method="GET" class="d-sm-flex">
                <input type="search" placeholder="Search.." name="search" required="required" autofocus>
                <button type="submit">Search</button>
                <a class="close" href="#url">&times;</a>
              </form>
            </div>
          </div>
          <!-- /search popup -->
        </div>
        <!-- toggle switch for light and dark theme -->
        <div class="mobile-position">
          <nav class="navigation">
            <div class="theme-switch-wrapper">
              <label class="theme-switch" for="checkbox">
                <input type="checkbox" id="checkbox">
                <div class="mode-container">
                  <!--<i class="gg-sun"></i>
                  <i class="gg-moon"></i>-->
                </div>
              </label>
            </div>
          </nav>
        </div>
      </nav>
    </div>
  </header>
  <!--/header-->

  <!-- main-slider -->
 
  <section class="w3l-main-slider position-relative" id="home">
    <div class="companies20-content">
      <div class="owl-one owl-carousel owl-theme">
        <div class="item">
          <li>
            <div class="slider-info banner-view bg bg2">
              <div class="banner-info">
                <div class="container">
                  <div class="banner-info-bg">
                  	<div class="content-info-in row col-12 col-sm-12 col-md-12 col-lg-12">
			          <div class="col-lg-4">
			            <img src="{{ asset('web_principal/images/celular_banner.png') }}" width="400px">
			          </div>
			          <div class="col-lg-8 mt-lg-0 mt-5 about-right-faq align-self  pl-lg-5">
			            <div class="title-content text-left mb-2">
			              <h1 class="titulo-banner">CONOCEMOS</h1> 
			          	  <h1 class="titulo-banner-2">LO QUE NECESITAS</h1>
			            </div>
			            <p class="mt-3 parrafo-banner">Efectividad y rapidez en una sola herramienta.</p>
			            <!--<a href="about.html" class="btn btn-style btn-primary mt-md-5 mt-4">Read More</a>-->
			          </div>
			        </div>

                    <div class="banner-buttons">
                      <!-- dialog itself, mfp-hide class is required to make dialog hidden -->
                      <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
                        <iframe src="https://player.vimeo.com/video/425349644" allow="autoplay; fullscreen"
                          allowfullscreen=""></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </div>
        <div class="item">
          <li>
            <div class="slider-info  banner-view banner-top1 bg bg2">
              <div class="banner-info">
                <div class="container">
                  <div class="banner-info-bg">
                    <div class="content-info-in row col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-lg-4">
                  <img src="{{ asset('web_principal/images/celular_banner.png') }}" width="400px">
                </div>
                <div class="col-lg-8 mt-lg-0 mt-5 about-right-faq align-self  pl-lg-5">
                  <div class="title-content text-left mb-2">
                    <h1 class="titulo-banner">CONOCEMOS</h1> 
                    <h1 class="titulo-banner-2">LO QUE NECESITAS</h1>
                  </div>
                  <p class="mt-3 parrafo-banner">Efectividad y rapidez en una sola herramienta.</p>
                  <!--<a href="about.html" class="btn btn-style btn-primary mt-md-5 mt-4">Read More</a>-->
                </div>
              </div>

                    <div class="banner-buttons">
                      <!-- dialog itself, mfp-hide class is required to make dialog hidden -->
                      <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
                        <iframe src="https://player.vimeo.com/video/425349644" allow="autoplay; fullscreen"
                          allowfullscreen=""></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </div>
        <div class="item">
          <li>
            <div class="slider-info banner-view banner-top2 bg bg2">
              <div class="banner-info">
                <div class="container">
                  <div class="banner-info-bg">
                    <div class="content-info-in row col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-lg-4">
                  <img src="{{ asset('web_principal/images/celular_banner.png') }}" width="400px">
                </div>
                <div class="col-lg-8 mt-lg-0 mt-5 about-right-faq align-self  pl-lg-5">
                  <div class="title-content text-left mb-2">
                    <h1 class="titulo-banner">CONOCEMOS</h1> 
                    <h1 class="titulo-banner-2">LO QUE NECESITAS</h1>
                  </div>
                  <p class="mt-3 parrafo-banner">Efectividad y rapidez en una sola herramienta.</p>
                  <!--<a href="about.html" class="btn btn-style btn-primary mt-md-5 mt-4">Read More</a>-->
                </div>
              </div>

                    <div class="banner-buttons">
                      <!-- dialog itself, mfp-hide class is required to make dialog hidden -->
                      <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
                        <iframe src="https://player.vimeo.com/video/425349644" allow="autoplay; fullscreen"
                          allowfullscreen=""></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </div>
        <div class="item">
          <li>
            <div class="slider-info banner-view banner-top3 bg bg2">
              <div class="banner-info">
                <div class="container">
                  <div class="banner-info-bg">
                    <div class="content-info-in row col-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-lg-4">
                  <img src="{{ asset('web_principal/images/celular_banner.png') }}" width="400px">
                </div>
                <div class="col-lg-8 mt-lg-0 mt-5 about-right-faq align-self  pl-lg-5">
                  <div class="title-content text-left mb-2">
                    <h1 class="titulo-banner">CONOCEMOS</h1> 
                    <h1 class="titulo-banner-2">LO QUE NECESITAS</h1>
                  </div>
                  <p class="mt-3 parrafo-banner">Efectividad y rapidez en una sola herramienta.</p>
                  <!--<a href="about.html" class="btn btn-style btn-primary mt-md-5 mt-4">Read More</a>-->
                </div>
              </div>

                    <div class="banner-buttons">
                      <!-- dialog itself, mfp-hide class is required to make dialog hidden -->
                      <div id="small-dialog" class="zoom-anim-dialog mfp-hide">
                        <iframe src="https://player.vimeo.com/video/425349644" allow="autoplay; fullscreen"
                          allowfullscreen=""></iframe>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
        </div>
      </div>
    </div>
  </section>

  <!-- //banner-slider-->
 
  <div class="w3l-bottom-grids" style="background-color: #DADADA;">
  	<div class="container texto-informativo">
	  	<p>
	  		Automatizamos el servicio de atención que brindas a tus clientes, para que así aumentes tu número de ventas mientras ahorras tiempo y energía, logrando esto a través de un redireccionamiento de tus usuarios de whatsapp e instagram a Tiendas.Club.
	  	</p>
  	</div>
  	<div class="row texto-porque-debes">
	  	<div class="row">
	  		<div class="porque-debes-comprarlo col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7">
	  			<p >¿POR QU&Eacute; DEBES <br> COMPRARLO?</p>
	  		</div>
	  		<div class="img-porque-debes-comprarlo col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
	  			<img src="{{ asset('web_principal/images/celular.png') }}" width="100%">
	  		</div>
	  	</div>
  	</div>
  </div>

  <!-- TEXTO BENEFICIOS -->
  <div id="como-funciona"></div>
    <section class="w3l-pricing" style="background-color: #DCB04D; padding-bottom: 12%;">
      <div class="py-5" id="pricing">
          <div class="container py-lg-5">
            <div  class="title-content text-center mb-5">
            	<p class="parrafo-beneficios" align="justify">
  		  		En pocas palabras, Tiendas.Club es una plataforma que te permite crear una tienda en línea con un link personalizado, donde podrás exponer tus productos y generar ventas. Esto con un sistema similar al de Rappi, con la gran diferencia de que tú manejas la tienda desde un panel de control, permitiendo que todos los pedidos lleguen directamente a tu whatsapp de forma organizada. 
            <br><br> 
            Con Tiendas.Club ahorrarás mucho tiempo e incluso podrás aumentar tus ventas, todo esto <b>sin comisiones, ni intermediarios.</b>

  		  	</p>
            </div>
          </div>
      </div>
    </section>



  <!-- BENEFICIOS -->

  <section class="section-beneficios">
      <div class="py-5" id="pricing">
          <div class="container py-lg-5">
            <div class="title-content text-center mb-5">
              <div class="row texto-beneficios">
  			  	<div class="container">
  			  		<h1>BENEFICIOS</h1>
  			  	</div>
  		  	</div>
            </div>
              <div class="row t-in">
                  <div class="col-lg-3 col-md-3 price-main-info"> 
                      <div class="price-inner card box-shadow box-1" style="background-image: url('{{ asset('web_principal/images/close-up-of-bitcoins-over-colombian-banknotes.jpg')  }} '); background-size: cover; border-radius: 30px !important;  -webkit-box-shadow: 0 56px 17px -14px rgba(0,0,0,0.53) ; box-shadow: 0 56px 17px -14px rgba(0,0,0,0.53) ;">
                          <div class="card-body" style="background: rgba(18,29,81,0.6); color:white !important;">
                          	<img src="{{ asset('web_principal/images/aumento-de-ventas.svg') }}">
                              <p>Tu propia tienda en l&iacute;nea en pocos minutos</p>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 price-main-info mt-md-0 mt-4"> 
  					<div class="price-inner card box-shadow" style="background-image: url('{{ asset('web_principal/images/close-up-of-bitcoins-over-colombian-banknotes.jpg')  }} '); background-size: cover; border-radius: 30px !important; -webkit-box-shadow: 0 56px 17px -16px rgba(0,0,0,0.53) ; box-shadow: 0 56px 17px -16px rgba(0,0,0,0.53) ;">
                          <div class="card-body" style="background: rgba(18,29,81,0.6); color:white !important;">
                          	<img src="{{ asset('web_principal/images/aumento-de-ventas.svg') }}">
                              <p style="font-size: 25px;">Optimiza la toma de tus pedidos hasta en un 75%</p>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 price-main-info mt-lg-0 mt-4"> 
  					<div class="price-inner card box-shadow" style="background-image: url('{{ asset('web_principal/images/close-up-of-bitcoins-over-colombian-banknotes.jpg')  }} '); background-size: cover; border-radius: 30px !important; -webkit-box-shadow: 0 56px 17px -14px rgba(0,0,0,0.53) ; box-shadow: 0 56px 17px -14px rgba(0,0,0,0.53) ;">
                          <div class="card-body" style="background: rgba(18,29,81,0.6); color:white !important;">
                          	<img src="{{ asset('web_principal/images/aumento-de-ventas.svg') }}">
                              <p style="font-size: 25px;">Genera un link Web para QR o Compartir en redes sociales</p>
                          </div>
                      </div>
                  </div>
                  <div class="col-lg-3 col-md-3 price-main-info mt-lg-0 mt-4"> 
  					<div class="price-inner card box-shadow" style="background-image: url('{{ asset('web_principal/images/close-up-of-bitcoins-over-colombian-banknotes.jpg')  }} '); background-size: cover; border-radius: 30px !important; -webkit-box-shadow: 0 56px 17px -14px rgba(0,0,0,0.53) ; box-shadow: 0 56px 17px -14px rgba(0,0,0,0.53) ;">
                          <div class="card-body" style="background: rgba(18,29,81,0.6); color:white !important;">
                          	<img src="{{ asset('web_principal/images/aumento-de-ventas.svg') }}">
                              <p style="font-size: 25px;">Tu plataforma de pedidos tipo rappi pero sin comisiones</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section>




  <!-- CLIENTES -->

  <section id="clientes-tiendas">
      <div class="py-5" id="pricing">
          <div class="container py-lg-5">
            <div class="title-content text-center mb-5">
              <div class="row texto-clientes">
  			  	<div class="container">
  			  		<h1>CLIENTES</h1>
  			  	</div>
  		  	</div>
            </div>
              <div class="row t-in contenedor-carrusel">
                  <div class="slider">
  					<div class="slide-track">
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/barile-ahumados.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/viu-burguer.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/molcajete.jpeg') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/barile-ahumados.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/viu-burguer.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/kingpapa.jpeg') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/barile-ahumados.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/viu-burguer.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/tribal.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/barile-ahumados.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/viu-burguer.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/tribal.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/barile-ahumados.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  						<div class="slide">
  							<div class="row">
  								<div class="img-cliente col-12 col-md-12 col-sm-12 col-lg-12">
  									<img src="{{ asset('web_principal/images/viu-burguer.png') }}" width="100%" align="right">		
  								</div>
  							</div>
  						</div>
  					</div>
  				</div>
              </div>
          </div>
      </div>
  </section>



  <!-- PLANES -->

  <section class="section-planes" id="section-planes">
      <div class="py-5" id="pricing">
          <div class="container py-lg-5">
            <div class="title-content text-center mb-5">
              <div class="row texto-planes">
            <div class="container">
              <h1>PLANES</h1>
            </div>
          </div>
            </div>
              <div class="row t-in">

                  <div class="col-lg-6 col-md-6 price-main-info"> 
                      <div class="price-inner card box-shadow" style="background-image: url('{{ asset('web_principal/images/close-up-of-bitcoins-over-colombian-banknotes.jpg')  }} '); background-size: cover; border-radius: 30px !important;  -webkit-box-shadow: 0 56px 17px -14px rgba(0,0,0,0.53) ; box-shadow: 0 56px 17px -14px rgba(0,0,0,0.53) ;">
                          <div class="card-body" style="background: rgba(18,29,81,0.6); color:white !important;">
                                <p style="font-size: 70px;" class="precio-plan">
                                  <b>GRATIS</b>
                                </p>
                                <br>
                              <p><b>PLAN B&Aacute;SICO</b></><p style="font-size: 20px;">Hasta 10 productos</p>
                          </div>
                      </div>
                  </div>

                  <div class="col-lg-6 col-md-6 price-main-info div-plan-blue"> 
                      <div class="price-inner card box-shadow" style="background-image: url('{{ asset('web_principal/images/close-up-of-bitcoins-over-colombian-banknotes.jpg')  }} '); background-size: cover; border-radius: 30px !important;  -webkit-box-shadow: 0 56px 17px -14px rgba(0,0,0,0.53) ; box-shadow: 0 56px 17px -14px rgba(0,0,0,0.53) ;">
                          <div class="card-body" style="background: rgba(18,29,81,0.6); color:white !important;">
                                <p style="font-size: 70px;" class="precio-plan">
                                  <b>$ 45000</b>
                                </p>
                                <br>
                              <p><b>PLAN BLUE</b></><p style="font-size: 20px;">Plan Full + Pagos en linea con Wompi + zona de domicilios.</p>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
      </div>
  </section>


  <!-- PREGUNTAS FRECUENTES -->
  <section class="w3l-content-3 contenedor-preguntas-frecuentes" id="centro-de-ayuda">
    <!-- /content-3-main-->
    <div class="row">
    	<div class="img-preguntas-frecuentes col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
    		<img src="{{ asset('web_principal/images/iPhone-12-Mockup.png') }}" alt="" width="100%">
    	</div>
    	<div class="txt-preguntas-frecuentes col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7">
    	
    	<p class="parrafo-preguntas-frecuentes">PREGUNTAS <br>FRECUENTES</p>
    	<br /><br />	
		<button class="accordion">¿C&oacute;mo me registro?</button>
		<div class="panel">
		  <a>Dale REGISTRARME, Solo es necesario que nos facilites un par de datos para empezar. Acá te dejamos el acceso rápido para iniciar <a href="https://clientes.tiendas.club/register"><b>REGISTRARME</b></a>.</a>
		</div>

		<button class="accordion">¿Necesito saber c&oacute;mo programar?</button>
		<div class="panel">
		  <a>No, Tiendas Club está diseñado para que logres crear tu tienda o menú en línea sin necesidad de escribir ni una sola línea de código. Nuestro panel de control es una herramienta muy fácil de usar, somos una herramienta de aprendizaje visual.</a>
		</div>

		<button class="accordion">¿Es necesario colocar tarjeta de cr&eacute;dito?</button>
		<div class="panel">
		  <a>Tenemos diferentes formas de pago, recuerda que nuestro pago es por subscripción, escoge la forma que más se adapte a tu negocio.</a>
		</div>

		<button class="accordion">¿Hay cl&aacute;usulas de permanencia? </button>
		<div class="panel">
		  <a>No, puedes retirarte en cualquier momento. Tiendas Club es una alternativa para que cualquier tipo de empresa tenga a su alcance una tienda en línea, nuestro único interés es contribuir en el crecimiento de tu negocio.</a>
		</div>
    	</div>
    </div>
  </section>
<!-- FIN PREGUNTAS FRECUENTES -->



  <!-- FORMULARIO DE CONTACTO -->

  <section class="w3l-content-3 contenedor-formulario">

    <!-- /content-3-main-->
    <div class="row">
    	<div class="img-formulario col-12 col-sm-5 col-md-5 col-lg-5 col-xl-5">
    		<img src="{{ asset('web_principal/images/mano-telefono.png') }}" alt="" >
    	</div>
    	<div class="txt-formulario col-12 col-sm-7 col-md-7 col-lg-7 col-xl-7">
    			<form id="formulario-contacto">
            
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

    				<div class="form-group">
              @include('conf.error')
              <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                  <div class="input-group-text"></div>
                </div>
                <input type="text" class="form-control" id="nombre" placeholder="Nombre" name="nombre">
              </div>
            </div>

            <div class="form-group">
              <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-briefcase" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M0 12.5A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-6h-1v6a.5.5 0 0 1-.5.5h-13a.5.5 0 0 1-.5-.5v-6H0v6z"/>
                      <path fill-rule="evenodd" d="M0 4.5A1.5 1.5 0 0 1 1.5 3h13A1.5 1.5 0 0 1 16 4.5v2.384l-7.614 2.03a1.5 1.5 0 0 1-.772 0L0 6.884V4.5zM1.5 4a.5.5 0 0 0-.5.5v1.616l6.871 1.832a.5.5 0 0 0 .258 0L15 6.116V4.5a.5.5 0 0 0-.5-.5h-13zM5 2.5A1.5 1.5 0 0 1 6.5 1h3A1.5 1.5 0 0 1 11 2.5V3h-1v-.5a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5V3H5v-.5z"/>
                    </svg>
                  </div>
                </div>
                <input type="text" class="form-control" id="empresa" placeholder="Empresa" name="empresa">
              </div>
            </div>

        <div class="form-group">
              <div class="input-group mb-2 mr-sm-2">
                <div class="input-group-prepend">
                  <div class="input-group-text">
                    @
                  </div>
                </div>
                <input type="text" class="form-control" id="correo" placeholder="Correo" name="correo">
              </div>
            </div>

              <div class="form-group">
                <div class="input-group mb-2 mr-sm-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-telephone-outbound" viewBox="0 0 16 16">
                      <path fill-rule="evenodd" d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511zM11 .5a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-1 0V1.707l-4.146 4.147a.5.5 0 0 1-.708-.708L14.293 1H11.5a.5.5 0 0 1-.5-.5z"/>
                    </svg></div>
                  </div>
                  <input type="text" class="form-control" id="telefono" placeholder="Telefono" name="telefono">
                </div>
              </div>

              <div class="form-group">
                <div class="input-group mb-2 mr-sm-2">
                  <div class="input-group-prepend">
                    <div class="input-group-text">
                      <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-chat-left-dots" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M14 1H2a1 1 0 0 0-1 1v11.586l2-2A2 2 0 0 1 4.414 11H14a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM2 0a2 2 0 0 0-2 2v12.793a.5.5 0 0 0 .854.353l2.853-2.853A1 1 0 0 1 4.414 12H14a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H2z"/>
                        <path d="M5 6a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0zm4 0a1 1 0 1 1-2 0 1 1 0 0 1 2 0z"/>
                      </svg>
                    </div>
                  </div>
                  <textarea class="form-control" placeholder="Mensaje" id="mensaje" name="mensaje"></textarea>
                </div>
              </div>

              <center>
                <button type="button" class="btn-envio" id="btn-formulario-contacto">Enviar</button>
              </center>
            </form>
          </div>
        </div>
  </section>
  <!-- FIN PREGUNTAS FRECUENTES -->

  <div class="contenedor-whatsapp">
    <a href="https://wa.link/vcrnyz" target="_blank">
      <img src="{{ asset('assets/images/Boton-Whatsapp.png') }}">
    </a>
  </div>

  <!-- footer-66 -->
  <footer class="w3l-footer-66">
    <section class="footer-inner-main">

      <!-- copyright -->
      <!-- move top -->
      <button onclick="topFunction()" id="movetop" title="Go to top">
        <span class="fa fa-long-arrow-up" aria-hidden="true"></span>
      </button>

  <script>
  var acc = document.getElementsByClassName("accordion");
  var i;

  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
      this.classList.toggle("active");
      var panel = this.nextElementSibling;
      if (panel.style.display === "block") {
        panel.style.display = "none";
      } else {
        panel.style.display = "block";
      }
    });
  }
  </script>


      <script>
        // When the user scrolls down 20px from the top of the document, show the button
        window.onscroll = function () {
          scrollFunction()
        };

        function scrollFunction() {
          if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
            document.getElementById("movetop").style.display = "block";
          } else {
            document.getElementById("movetop").style.display = "none";
          }
        }

        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
          document.body.scrollTop = 0;
          document.documentElement.scrollTop = 0;
        }
      </script>
      <!-- /move top -->

    </section>
  </footer>
  <!--//footer-66 -->
  <!-- Template JavaScript -->
  <script src="{{ asset('web_principal/js/jquery-3.3.1.min.js') }}"></script>
  <script src="https://code.jquery.com/jquery-3.5.1.js"></script>

  <!-- disable body scroll which navbar is in active -->
<script src="{{ asset('web_principal/js/jquery.magnific-popup.min.js') }}"></script>
<script>
	$(document).ready(function () {
		$('.popup-with-zoom-anim').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,

			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});

		$('.popup-with-move-anim').magnificPopup({
			type: 'inline',

			fixedContentPos: false,
			fixedBgPos: true,

			overflowY: 'auto',

			closeBtnInside: true,
			preloader: false,

			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-slide-bottom'
		});
	});
</script>
<!--//-->
  <script src="{{ asset('web_principal/js/theme-change.js') }}"></script>
  <script src="{{ asset('web_principal/js/owl.carousel.js') }}"></script>
  <!-- script for banner slider-->
  <script>
    $(document).ready(function () {
      $('.owl-one').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        responsiveClass: true,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplaySpeed: 1000,
        autoplayHoverPause: false,
        responsive: {
          0: {
            items: 1,
            nav: false
          },
          480: {
            items: 1,
            nav: false
          },
          667: {
            items: 1,
            nav: true
          },
          1000: {
            items: 1,
            nav: true
          }
        }
      })
    })
  </script>
  <!-- //script -->
  <!-- script for owlcarousel -->
  <script>
    $(document).ready(function () {
      $('.owl-testimonial').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        responsiveClass: true,
        autoplay: false,
        autoplayTimeout: 5000,
        autoplaySpeed: 1000,
        autoplayHoverPause: false,
        responsive: {
          0: {
            items: 1,
            nav: false
          },
          480: {
            items: 1,
            nav: false
          },
          667: {
            items: 1,
            nav: false
          },
          1000: {
            items: 1,
            nav: false
          }
        }
      })
    })
  </script>
  <!-- disable body scroll which navbar is in active -->
  <script>
    $(function () {
      $('.navbar-toggler').click(function () {
        $('body').toggleClass('noscroll');
      })
    });
  </script>
  <!-- disable body scroll which navbar is in active -->

  <!-- stats number counter-->
  <script src="{{ asset('web_principal/js/jquery.waypoints.min.js') }}"></script>
  <script src="{{ asset('web_principal/js/jquery.countup.js') }}"></script>
  <script>
    $('.counter').countUp();
  </script>
  <!-- //stats number counter -->
  <!--/MENU-JS-->
  <script>
    $(window).on("scroll", function () {
      var scroll = $(window).scrollTop();

      if (scroll >= 80) {
        $("#site-header").addClass("nav-fixed");
      } else {
        $("#site-header").removeClass("nav-fixed");
      }
    });

    //Main navigation Active Class Add Remove
    $(".navbar-toggler").on("click", function () {
      $("header").toggleClass("active");
    });
    $(document).on("ready", function () {
      if ($(window).width() > 991) {
        $("header").removeClass("active");
      }
      $(window).on("resize", function () {
        if ($(window).width() > 991) {
          $("header").removeClass("active");
        }
      });
    });
  </script>
  <!--//MENU-JS-->

  <script src="{{ asset('web_principal/js/bootstrap.min.js') }}"></script>
  <script src="{{ asset('web_principal/js/acciones.js') }}"></script>
</body>

</html>