@extends('header.web.app')

@section('content')

    @foreach($tiendas as $tienda)

        @if(isset($tienda->background_tienda))
            <div class="encabezadoTiendas col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="background-image: url('https://clientes.tiendas.club/storage/{{ $tienda->background_tienda }}');">
        @else
            <div class="encabezadoTiendas col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" style="background-image: url('{{ asset('assets/images/banner.jpg') }} ');">
        @endif

        <!-- ENCABEZADO DE LA TIENDA -->

            <div class="container" style="padding-top: 0px; margin-top: 0px;">
                <div class="row">
                    <div class="col-3 col-md-3 col-sm-3 col-lg-3 col-xl-3 btnAtras">
                        <!--<img src="{{ asset('assets/2021/image/btn-atras.gif') }}">-->
                    </div>
                    <div class="col-6 col-md-6 col-sm-6 col-lg-6 col-xl-6 logoEncabezado">
                        <img src="{{ asset('assets/2021/image/logo.gif') }}">
                    </div>
                    <div class="col-3 col-md-3 col-sm-3 col-lg-3 col-xl-3 menuOpcion" data-code="{{ $tienda->id_tienda }}">
                        <img src="{{ asset('assets/2021/image/menu-opcion.gif') }}">
                    </div>
                </div>
                <input type="hidden" id="id_tienda" value="{{ $tienda->id_tienda }}">
                
                <div class="tituloTienda">    
                     <img src="https://clientes.tiendas.club/storage/{{ $tienda->image_tienda }}" width="200px">
                </div>

                <div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <div class="row">
                        <div class="col-2 col-md-2 col-sm-2 col-lg-2 col-xl-2"></div>
                        <div class="col-8 col-md-8 col-sm-8 col-lg-8 col-xl-8 redesEncabezado">
                            <a href="{{ $tienda->facebook_link }}" target="_blank"><img src="{{ asset('assets/2021/image/facebook.gif') }}"></a>
                            <a href="{{ $tienda->instagram_link }}" target="_blank"><img src="{{ asset('assets/2021/image/instagram.gif') }}"></a>
                            <a>{{ $tienda->nombre_tienda }}</a>
                            <br>
                            <p>tiendas.club</p>
                        </div>
                        <div class="col-2 col-md-2 col-sm-2 col-lg-2 col-xl-2 corazonEncabezado">
                            <a href="{{ $tienda->codigo_maps }}" target="_blank">
                                <img src="{{ asset('assets/2021/image/ubicacion.png') }}">
                            </a>
                        </div>
                    </div>
                </div>

            </div> 

        <!-- FIN ENCABEZADO DE LA TIENDA -->

            </div>  
    @endforeach

<input type="hidden" id="validate_nequi_code" value="">
<input type="hidden" id="tiempo_contador" value="3000">


<div class="menu_titulo">
    <p class="titlePrecios">
        MEN&Uacute;
    </p>
</div>
<div class="body_productos">
    <div class="categorias">
            <div class="identificador_categoria" data-valor="todo"><p>Todo</p></div>
         @foreach($categorias as $categoria)
            <div class="identificador_categoria" data-valor="{{ $categoria->id }}"><p>{{ $categoria->nombre_categoria }}</p></div>
         @endforeach
    </div>
</div>
<div class="productos_general">
    @include('conf.error')

    <div id="resultadoSQLProducto">
        <div class="row">
        </div>
    </div>
            
    <div class="productos" id="resultadoSQLProducto">
        <?php $i = 1; ?>
        @foreach($productos as $producto)
            @include('conf.informacion_productos')
        @endforeach
    </div>
</div>

<div class="contenedor">
    <div class="container ">
        
        
        
        <div class="alert alert-success" id="alerta-exito">
            Producto agregado con exito!
        </div>

        <div class="carroCompra col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="barraLateralCarroCompra">
            <h4 class="mb-4">
                <br>
                <img src="{{ asset('assets/images/supermercado.svg') }}" width="24px"> &nbsp; Tu pedido</h4>
            <hr>
            <div id="carroCompra">
                <div class="resumenPedido col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="resumenPedido">
                    <div class="row">
                        @foreach(Cart::content() as $pedido)
                        <div class="productoComprado col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" data-id="{{ $pedido->id }}">
                            <div class="row">
                                <div class="tituloProducto col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">   
                                    <em style="padding-right: 10px;">x{{ $pedido->qty }} </em> {{ $pedido->name }} - <i style="font-size: 12px; text-transform: lowercase !important;">{{ $pedido->options->adicionales }}</i>
                                </div>
                                <div class="valorProducto col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">   
                                    <p>
                                        $ {{ number_format($pedido->qty * $pedido->price)  }} + <br>$ {{ number_format($pedido->options->valores_adicionales) }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>


        <div class="alert alert-success" id="alerta-exito">
            Producto agregado con exito!
        </div>

        <div class="carroCompra col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="barraLateralCarroCompra">
            <h4 class="mb-4">
                <br>
                <img src="{{ asset('assets/images/supermercado.svg') }}" width="24px"> &nbsp; Tu pedido</h4>
            <hr>
            <div id="carroCompra">
                <div class="resumenPedido col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" id="resumenPedido">
                    <div class="row">
                        @foreach(Cart::content() as $pedido)
                        <div class="productoComprado col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" data-id="{{ $pedido->id }}">
                            <div class="row">
                                <div class="tituloProducto col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">   
                                    <em style="padding-right: 10px;">x{{ $pedido->qty }} </em> {{ $pedido->name }} - <i style="font-size: 12px; text-transform: lowercase !important;">{{ $pedido->options->adicionales }}</i>
                                </div>
                                <div class="valorProducto col-12 col-sm-6 col-md-6 col-lg-6 col-xl-6">   
                                    <p>
                                        $ {{ number_format($pedido->qty * $pedido->price)  }} + <br>$ {{ number_format($pedido->options->valores_adicionales) }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


        <!-- MODAL PARA FORMULARIO -->

    <div class="modal fade" id="modalFormulario" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document" data-backdrop="static" data-keyboard="false">

            <div class="modal-content">
                <div class="cerrar-modal btn-abrir-carrito" >
                    <center>
                        <img src="{{ asset('assets/2021/image/cerrar.gif') }}" data-dismiss="modal" aria-label="close" >
                    </center>
                </div>
                <div class="modal-body" id="modalFormularioBody">
                   
                </div>
            </div>
        </div>
    </div>


    <!-- MODAL PARA VER FORMULARIO DE SUGERENCIA -->

    <div class="modal fade" id="modalFormularioSugerencia" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document" data-backdrop="static" data-keyboard="false">
            <div class="modal-content">
                <div class="cerrarModalCarrito" data-dismiss="modal" aria-label="close">
                    <img src="{{ asset('assets/images/close.png') }}" data-dismiss="modal" aria-label="close">
                </div>
                <div class="modal-body" id="modalFormularioBodySugerencia">
                   
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL PARA AGREGAR UN PRODUCTOS CON LAS ADICIONALES -->

    <div class="modal_completa">
        <div class="imagen_modal_completa" >
            <div class="row">
                <div class="col-3 col-md-3 col-sm-3 col-lg-3 col-xl-3 btnAtras_modal">
                    <img src="{{ asset('assets/2021/image/btn-atras.gif') }}">
                </div>
                <div class="col-6 col-md-6 col-sm-6 col-lg-6 col-xl-6 logoEncabezado_modal">
                    <img src="{{ asset('assets/2021/image/logo.gif') }}">
                </div>
                <div class="col-3 col-md-3 col-sm-3 col-lg-3 col-xl-3 menuOpcion_modal">
                    <img src="{{ asset('assets/2021/image/menu-opcion.gif') }}">
                </div>
                    <button type="button" class="btn ver-fotografia" data-img="">
                        <img src="{{ asset('assets/2021/image/camera.png') }}">
                    </button>
            </div>
        </div>
        <div class="informacion_modal_completa">
            
            <form id="formularioPedido" method="POST">
                     @csrf
                    @method('POST')
                    <input type="hidden" id="codigo_producto" name="codigo_producto">
                    <hr class="divisor_descripcion_producto">
                    <div id="categoria_producto">
                        <p></p>
                    </div>
                    <div id="informacion_producto_formulario" data-img="">
                        <p class="nombre"></p>
                        <p class="precio"></p>
                        <p class="descripcion"></p>
                    </div>
                    <div class="titulo_adicionales">
                        <p>ADICIONALES</p>
                    </div>
                    <div class="adicionales col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12">
                        <div class='adicional_unico' id='adicional_unico'>
                            
                        </div>
                    </div>
                        <textarea class="form-control comentarios_producto" id="comentarios_producto" name="comentarios_producto" placeholder="Comenta del producto"></textarea>
                    
            </form>
        </div>
        <div class="boton_agregar col-12 col-md-12 col-sm-12 col-lg-12 col-xl-12">
            <input type="button" class="iconAddPro" id="iconAddPro" value="Agregar al carrito">
        </div>
    </div>

    <!-- MODAL PARA VER EL CARRITO DE COMPRAS -->

    <div class="modal fade" id="modalInformacionCarrito" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document" data-backdrop="static" data-keyboard="false">
            <div class="modal-content">
               <div class="cerrar-modal btn-abrir-carrito" >
                    <center>
                        <img src="{{ asset('assets/2021/image/cerrar.gif') }}" data-dismiss="modal" aria-label="close" >
                    </center>
                </div>
                <div class="modal-body" id="modalCarroCompra">
                    <center><h1 class="titulo">Carrito vacio</h1></center>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalInformacionTienda" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document" data-backdrop="static" data-keyboard="false">
            <div class="modal-content">
               <div class="cerrar-modal" >
                    <center>
                        <img src="{{ asset('assets/2021/image/cerrar.gif') }}" data-dismiss="modal" aria-label="close" >
                    </center>
                </div>
                <div class="modal-body" id="modalCarroTienda">

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalInformacionFotoProducto" data-backdrop="static" data-keyboard="false" style="z-index: 99999999999999 !important;">
        <div class="modal-dialog modal-dialog-centered" role="document" data-backdrop="static" data-keyboard="false">
            <div class="modal-content">
               <div class="cerrar-modal" >
                    <center>
                        <img src="{{ asset('assets/2021/image/cerrar.gif') }}" data-dismiss="modal" aria-label="close" >
                    </center>
                </div>
                <div class="modal-body" id="bodyInformacionFotoproducto">

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modalFormulario" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document" data-backdrop="static" data-keyboard="false">
            <div class="modal-content">
                <div class="cerrar-modal btn-abrir-carrito" >
                    <center>
                        <img src="{{ asset('assets/2021/image/cerrar.gif') }}" data-dismiss="modal" aria-label="close" >
                    </center>
                </div>
                <div class="modal-body" id="modalFormularioBody">
                </div>
            </div>
        </div>
    </div>

    <div id="pantalla_cubierta" style="display:none; ">d</div>
    <div class="modal fade" id="modalInformacionProceso" data-backdrop="static" data-keyboard="false" style="z-index: 999999999 !important;">
        <div class="modal-dialog modal-dialog-centered" role="document" data-backdrop="static" data-keyboard="false">
            <div class="modal-content">
                <div class="modal-body" id="modalInformacionProcesoBody">
                </div>
            </div>
        </div>
    </div>    

    @include('conf.barra_opcion_inferior')

</div>

@endsection



@section('scripts')


<script>

    $(document).on("click", ".share_facebook", function () {
        window.open("http://www.facebook.com/sharer.php?u=http://tiendas.club/<?php echo $url_tienda; ?>", "ventana1" , "width=300,height=300,scrollbars=YES")
    });

    $(document).on("click", ".share_twitter", function () {
        window.open("http://www.twitter.com/share?url=http://tiendas.club/<?php echo $url_tienda; ?>", "ventana1" , "width=300,height=300,scrollbars=YES")
    });

    $(document).on("click", ".envio-formulario-wompi", function () {
        
        $opcion = $(this).attr("data-op");
        
        if($opcion == "whatsapp"){
            
            var formData = $("#formularioWhatsapp").serialize();
            
            
            if($("#nombre").val().trim().length > 0){
                $(".nombreDomicilio").attr("style","display:none;");
                $nombreDomi = 1;
            }else{
                $(".nombreDomicilio").attr("style","display:block;");
            }

            
            if($("#telefono").val().trim().length > 0){
                $(".telefonoDomicilio").attr("style","display:none;");
                
                var celular = $("#telefono").val();

                var expresion = /^3[\d]{9}$/;
                
                if (isNaN(celular) || !expresion.test(celular)){
                    alert("Debe ingresar un número válido.");
                }else{
                    $telefono = 1;
                    var celular_cliente = $("#telefono").val();
                }

            }else{
                $(".telefonoDomicilio").attr("style","display:block;");
            }


            if($("#direccion").val().trim().length > 0){
                $(".direccionDomicilio").attr("style","display:none;");
                $direccionDomi = 1;
            }else{
                $(".direccionDomicilio").attr("style","display:block;");
            }

            
            if($("#barrio").val().trim().length > 0){
                $(".barrioDomicilio").attr("style","display:none;");
                $barrioDomi = 1;
            }else{
                $(".barrioDomicilio").attr("style","display:block;");
            }


            if($nombreDomi == 1 && $direccionDomi == 1 && $barrioDomi == 1 && $telefono == 1){
                
                var formData = $("#formularioWhatsapp").serialize();
                
                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: "POST",
                    url: "/wompi/cliente",
                    data: formData,
                    success: function (data) {
                        $("#modalFormularioBody").html(data);
                    }
                });

            }

        }else{
            
            if($("#nombre").val().trim().length > 0){
                $(".nombreLocal").attr("style","display:none;");
                $nombre = 1;
                var nombre_cliente = $("#nombre").val(); 
            }else{
                $(".nombreLocal").attr("style","display:block;");
            }

            
            if($("#telefono").val().trim().length > 0){
                $(".telefonoLocal").attr("style","display:none;");
                
                var celular = $("#telefono").val();

                var expresion = /^3[\d]{9}$/;
                
                if (isNaN(celular) || !expresion.test(celular)){
                    alert("Debe ingresar un número válido.");
                }else{
                    $telefono = 1;
                    var celular_cliente = $("#telefono").val();
                }

            }else{
                $(".telefonoLocal").attr("style","display:block;");
            }

            
            if($("#hora_llegada").val().trim().length > 0){
                $(".telefonoLocal").attr("style","display:none;");
                $hora = 1;
                var hora_cliente = $("#hora_llegada").val();
            }else{
                $(".horaLocal").attr("style","display:block;");
            }

            if($nombre == 1 && $telefono == 1 && $hora == 1){
                
                var formData = $("#formularioLocal").serialize();

                $.ajax({
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    type: "POST",
                    url: "/wompi/cliente",
                    data: formData,
                    success: function (data) {
                        $("#modalFormularioBody").html(data);
                    }
                });
                /*
                        $("#modalFormularioBody").html(data);     
            */}
        }
    });


    $(document).on("click", "#NEQUI", function(){
        $(".select_nequi").attr("style","display:block;");
        $("#method_pay").val("NEQUI");

        $(".select_card").attr("style","display:none;");
        $(".select_pse").attr("style","display:none;");
        $(".select_transferencia").attr("style","display:none;");
    });

    $(document).on("click", "#CARD", function(){
        $(".select_card").attr("style","display:block;");
        $("#method_pay").val("CARD");


        $(".select_nequi").attr("style","display:none;");
        $(".select_pse").attr("style","display:none;");
        $(".select_transferencia").attr("style","display:none;");
    });

    $(document).on("click", "#PSE", function(){
        $(".select_pse").attr("style","display:block;");
        $("#method_pay").val("PSE");

        $(".select_nequi").attr("style","display:none;");
        $(".select_card").attr("style","display:none;");
        $(".select_transferencia").attr("style","display:none;");
    });

    $(document).on("click", "#BANCOLOMBIA_TRANSFER", function(){
        $(".select_transferencia").attr("style","display:block;");
        $("#method_pay").val("BANCOLOMBIA_TRANSFER");

        $(".select_nequi").attr("style","display:none;");
        $(".select_card").attr("style","display:none;");
        $(".select_pse").attr("style","display:none;");
    });



    $(document).on("click", ".envio_formulario_api", function(){

    });
</script>

@stop

