<hr style="height: 10px !important; margin-top:5px; margin-bottom:5px; ">
<h3>Adicionales</h3>

<?php
$adicional_individual = [];
$array_num = 0;
$data_producto = 0;
foreach (Cart::content() as $pedido) {

    if ($pedido->id == $producto) {

        $adicional_individual = explode(",", $pedido->options->code_adicionales);
        $array_num = count($adicional_individual);

        $data_producto = $pedido->options->codigo_producto;
    } else {
        
    }
}

$id_producto = 0;
//print_r($adicional_individual); 

$i = 0;
?>

@foreach($adicionales as $adicion)

<?php
$id_producto = $adicion->id_producto;

$num = $i++;
?>
<input type="checkbox" value="{{ $adicion->id_adicional }}" class="adicionalInput" data-nombre="{{ $adicion->nombre_adicional }}" data-valor="{{ $adicion->valor_adicional }}" data-producto="<?php echo $data_producto; ?>" 

<?php
for ($i = 0; $i < $array_num; $i++) {
    if ($adicional_individual[$i] == $adicion->id_adicional) {
        echo "checked = 'checked'";
    }
}
?>

       >{{ $adicion->nombre_adicional }} &nbsp;  

@endforeach

<hr style="height: 10px !important; margin-top:5px; margin-bottom:5px; ">
<h3>Comentarios del Producto: </h3>
<textarea id="comentarioProductoModal" class="form-control comentariosProducto col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12" data-producto="{{ $producto }}"><?php
       foreach (Cart::content() as $pedido) {
           if ($pedido->id == $producto) {
               echo $pedido->options->comentarios;
           } else {
               
           }
       }
?></textarea>
<script type="text/javascript">


    // acciones de recargar
    function recargar() {
        var subtotal = document.getElementById('msubtotal').innerHTML;

        if ($("#valor_domicilio").val() != null && msubtotal != null) {


            var adicion = document.getElementById('adicionalesTotalesDiv').innerHTML;

            var valor_adicionales = Math.round(adicion.replace(/,/g, ''));
            var valor_domicilio = Math.round($("#valor_domicilio").val().replace(/,/g, ''));
            var valor_subtotal = Math.round(subtotal.replace(/,/g, ''));

            if ($("#valor_domicilio").val() == "0") {
                var suma = valor_subtotal + valor_domicilio + valor_adicionales;

                $("#valorPedido").html("Total sin Domicilio: $ " + suma);
                $("#totalPedidoWhat").attr("data-suma", suma);


            } else {
                var suma = valor_subtotal + valor_domicilio + valor_adicionales;

                $("#valorPedido").html("Total + Domicilio: $ " + suma);
                $("#totalPedidoWhat").attr("data-suma", suma);
            }

        }


        $("#botonProductosCarrito").load(" #botonProductosCarrito");
        $("#resumenPedido").load(location.href + " #resumenPedido>*", "");
        $("#botonProductoValor").load(" #botonProductoValor");
        $("#cantidadCarrito").load(" #cantidadCarrito");

        $("#valor_subtotal_up").load(location.href + " #valor_subtotal_up>*", "");

        $("#totalPedido").load(location.href + " #totalPedido>*", "");
        $("#tableCarrito").load(location.href + " #tableCarrito>*", "");
    }



    $("#comentarioProductoModal").keyup(function () {
        var value = $(this).val();
        var id_producto = $(this).attr("data-producto");


        var url = '/agregarComentarios/' + id_producto + '/' + value;

        $.ajax({
            url: url,
            type: 'get',
            success: function (data) {
                recargar();
            }
        });

    }).keyup();
</script>