<div class="contenedor_resultado_search">
    <div class="row_resultado_search_categoria">
        @foreach($productos as $producto)
        <div class="productoUnico resultado_consulta_producto" id="producto{{ $producto->id }}" data-id="{{ $producto->id }}" style="background-image: url('https://clientes.tiendas.club/storage/{{ $producto->imagen_producto }}'); ">
            <div class="row descripcion_texto_producto_unico">
                <div class="tituloProducto">
                    <p>{{ $producto->name }}</p>
                </div>
                <div class="valorProductoUnico ">
                    <strong>${{ intval($producto->valor) }}</strong>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
<h2 class="titulo" style="margin-left: 10px;"><strong>M&aacute;s productos:</strong></h3>
<hr>